from django.db import models
# Create your models here.
from django.db.models import SET_NULL, Q
from pytils.translit import slugify

from user_app.models import User


def file_upload_to(instance, filename):
    return "%s" % filename


OUT_PROCESS = 1
PROCESSED = 2
IN_PROCESS = 3
NOT_PARSED = 4
STATUSES = (
    (OUT_PROCESS, 'Необработан'),
    (PROCESSED, 'Обработан'),
    (IN_PROCESS, 'В обработке'),
    (NOT_PARSED, 'Неспарсен'),
)
STEP_ONE = 1
STEP_END = 100


class Project(models.Model):
    class Meta:
        verbose_name_plural = 'Проекты'
        verbose_name = 'проект'

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Language(models.Model):
    class Meta:
        verbose_name_plural = 'языки'
        verbose_name = 'язык'

    code = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    is_translate = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.code

    @property
    def is_related(self):
        count = Country.objects.filter(language=self).count()
        if count > 0:
            return True
        return False


class Currency(models.Model):
    class Meta:
        verbose_name_plural = 'Валюты'
        verbose_name = 'валюту'

    code = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    code_name = models.CharField(max_length=10, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def is_related(self):
        if Country.objects.filter(currency=self).count() > 0:
            return True
        if Brand.objects.filter(currency=self).count() > 0:
            return True
        return False


class ExchangeRate(models.Model):
    class Meta:
        verbose_name = 'обмен валют'
        verbose_name_plural = 'обмен валют'

    value = models.FloatField()
    updated_at = models.DateTimeField(null=True, blank=True, auto_now=True)
    date = models.DateField(null=True, blank=True)
    from_currency = models.ForeignKey(Currency, related_name='from_currency', null=True, on_delete=models.SET_NULL)
    to_currency = models.ForeignKey(Currency, related_name='to_currency', null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.value)


class ExchangeValue(models.Model):
    class Meta:
        ordering = '-updated_at'.split()

    exchange = models.ForeignKey(ExchangeRate, related_name='values', on_delete=models.SET_NULL, null=True, blank=True)
    value = models.FloatField()
    updated_at = models.DateTimeField(auto_now_add=True, null=True)
    date = models.DateField(null=True, blank=True)


# @receiver(post_save, sender=ExchangeRate, dispatch_uid="update_stock_count")
# def update_stock(sender, instance, **kwargs):
#     exchange_value = ExchangeValue.objects.create(value=instance.value, exchange=instance, date=instance.date)
#     exchange_value.save()


class Country(models.Model):
    delivery_price = models.IntegerField(null=True, blank=True, default=2)
    delivery_percent = models.IntegerField(null=True, blank=True, default=10)
    delivery_day = models.IntegerField(null=True, blank=True, default=3)
    day_count_newest = models.IntegerField(null=True, blank=True, default=2)
    code = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    language = models.ForeignKey(Language, null=True, on_delete=SET_NULL)
    currency = models.ForeignKey(Currency, null=True, on_delete=SET_NULL)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def is_related(self):
        if self.currency is None and self.language is None:
            return False
        return True


class VendColour(models.Model):
    class Meta:
        verbose_name = 'Цвет (vend)'
        verbose_name_plural = 'цвет (vend)'

    name = models.CharField(max_length=100)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    name_en = models.CharField(max_length=100, null=True, blank=True)
    izi_colour = models.ForeignKey('IziColour', null=True, blank=True, on_delete=SET_NULL)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name_lower = self.name.lower()
        super(VendColour, self).save()


class IziColour(models.Model):
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    code = models.CharField(max_length=100, null=True, blank=True)
    hex = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(upload_to=file_upload_to, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name_lower = self.name.lower()
        super(IziColour, self).save()

    @property
    def is_related(self):
        if VendColour.objects.filter(izi_colour=self).count() > 0:
            return True
        return False

    @property
    def languages(self):
        data = []
        for i in Language.objects.all():
            context = {
                'lang_id': i.id,
                'lang_name': i.name,
                'lang_code': i.code,
            }
            tr = None
            try:
                tr = TranslationColour.objects.get(colour=self, language=i)
                context['translation'] = tr.name
            except:
                context['translation'] = None
            data.append(context)
        return data


class TranslationColour(models.Model):
    language = models.ForeignKey(Language, null=True, on_delete=models.SET_NULL)
    colour = models.ForeignKey(IziColour, null=True, blank=True, related_name='translations', on_delete=models.SET_NULL)
    name = models.CharField(max_length=100, default='')
    name_lower = models.CharField(max_length=100, default='', null=True)

    def save(self, *args, **kwargs):
        # self.name_lower = self.name.lower()
        super(TranslationColour, self).save()

    def __str__(self):
        return self.name


class City(models.Model):
    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    country = models.ForeignKey(Country,
                                null=True, related_name='cities', on_delete=models.SET_NULL)
    delivery_price = models.IntegerField(null=True, blank=True, default=4)
    delivery_percent = models.IntegerField(null=True, blank=True, default=10)
    delivery_day = models.IntegerField(null=True, blank=True, default=3)
    name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class DeliveryPoint(models.Model):
    class Meta:
        verbose_name = 'Точка самовывоза'
        verbose_name_plural = 'Точка самовывоза'

    city = models.ForeignKey(City,
                             null=True, related_name='points', on_delete=models.SET_NULL)
    delivery_price = models.IntegerField(null=True, blank=True, default=0)
    lat = models.FloatField(null=True, blank=True)
    lng = models.FloatField(null=True, blank=True)
    name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Brand(models.Model):
    class Meta:
        ordering = 'name'.split()
        verbose_name = 'бренд'
        verbose_name_plural = 'бренды'

    delivery_price = models.IntegerField(null=True, blank=True, default=2)
    delivery_percent = models.IntegerField(null=True, blank=True, default=10)
    delivery_day = models.IntegerField(null=True, blank=True, default=3)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100, null=True, blank=True)
    link = models.CharField(max_length=1000)
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_landing = models.BooleanField(default=True)
    is_trend_yol = models.BooleanField(default=True)
    project = models.ForeignKey(Project, null=True, blank=True, on_delete=models.SET_NULL)
    currency = models.ForeignKey(Currency, null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    m_image = models.ImageField(upload_to=file_upload_to, null=True, blank=True)
    db_image = models.ImageField(upload_to=file_upload_to, null=True, blank=True)
    ds_image = models.ImageField(upload_to=file_upload_to, null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def total_count(self):
        count = OriginalProduct.objects.filter(brand=self).count()
        return count

    @property
    def no_price_or_stock(self):
        count = OriginalProduct.objects.filter(Q(original_price=0) | Q(stock=False), brand=self).count()
        return count

    @property
    def document_count(self):
        count = Document.objects.filter(brand=self).count()
        return count

    @property
    def processed_count(self):
        count = OriginalProduct.objects.filter(brand=self, document__step__in=[5, 100]).count()
        return count

    @property
    def in_process_count(self):
        count = OriginalProduct.objects.filter(brand=self, document__step__in=[1, 2, 3, 4]).count()
        return count

    @property
    def out_process_count(self):
        count = (OriginalProduct.objects.filter(document__isnull=True, brand=self).count())
        return count

    @property
    def countries(self):
        countries = []
        for i in Country.objects.all():
            brand_country = None
            try:
                brand_country = BrandCountry.objects.get(country=i, brand=self)
            except:
                pass
            context = {
                'country_id': i.id,
                'country_name': i.name,
                'country_code': i.code,
            }
            if brand_country is not None:
                context['mark_up'] = brand_country.mark_up
                context['round_digit'] = brand_country.round_digit
                context['round_to'] = brand_country.round_to
            else:
                context['mark_up'] = None
                context['round_digit'] = None
                context['round_to'] = None
            countries.append(context)
        return countries


class BrandCountry(models.Model):
    country = models.ForeignKey(Country, null=True, on_delete=models.SET_NULL)
    brand = models.ForeignKey(Brand, null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)
    mark_up = models.FloatField()
    round_digit = models.IntegerField(default=2)
    round_to = models.CharField(max_length=10, default='00', null=True)


class Slider(models.Model):
    title = models.CharField(max_length=100, null=True)

    class Meta:
        verbose_name = 'Слайдер'
        verbose_name_plural = 'сдайдеры'

    def __str__(self):
        return self.title


class ImageSlider(models.Model):
    image = models.ImageField(null=True, blank=True, upload_to=file_upload_to)
    slider = models.ForeignKey(Slider, blank=True, null=True, related_name='images', on_delete=models.SET_NULL)

    def __str__(self):
        return self.image.name


class Department(models.Model):
    class Meta:
        verbose_name = 'отделение (izishop)'
        verbose_name_plural = 'отделения (izishop)'
        ordering = 'position'.split()

    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100, null=True, blank=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    position = models.IntegerField(null=True, blank=True)
    code = models.CharField(max_length=100, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    sizes = models.ManyToManyField('Size', blank=True)
    colours = models.ManyToManyField('IziColour', blank=True)
    price_from = models.FloatField(default=0, null=True, blank=True)
    price_to = models.FloatField(default=1000, null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name_lower = self.name.lower()
        self.slug = slugify('%s-%s' % (self.name, self.code))
        super(Department, self).save()

    @property
    def is_related(self):
        if ParentCategory.objects.filter(department=self).count() > 0:
            return True
        if VendDepartment.objects.filter(department=self).count() > 0:
            return True
        return False

    @property
    def languages(self):
        data = []
        for i in Language.objects.all():
            context = {
                'lang_id': i.id,
                'lang_name': i.name,
                'lang_code': i.code,
            }
            try:
                tr = TranslationDepartment.objects.get(department=self,
                                                       language=i)
                context['translation'] = tr.name
                context['is_active'] = tr.is_active
            except:
                context['translation'] = None
                context['is_active'] = None
            data.append(context)
        return data


class TranslationDepartment(models.Model):
    class Meta:
        verbose_name = 'отделение (перевод)'
        verbose_name_plural = 'отделения (перевод)'

    department = models.ForeignKey(Department, null=True, on_delete=models.SET_NULL)
    slug = models.CharField(max_length=100, null=True, blank=True)
    name = models.CharField(max_length=100, null=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    language = models.ForeignKey(Language, null=True, blank=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        try:
            self.name_lower = self.name.lower()
        except:
            pass
        self.slug = slugify('%s' % self.name)
        super(TranslationDepartment, self).save()


class ParentCategory(models.Model):
    class Meta:
        ordering = 'position'.split()
        verbose_name = 'род. категорию (izishop)'
        verbose_name_plural = 'род. категории (izishop)'

    name = models.CharField(max_length=100, default='')
    slug = models.CharField(max_length=100, null=True, blank=True)
    department = models.ForeignKey(Department, null=True, blank=True, on_delete=SET_NULL)
    code = models.CharField(max_length=100, null=True, blank=True)
    position = models.IntegerField(null=True, blank=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True, default='')
    sizes = models.ManyToManyField('Size', blank=True)
    colours = models.ManyToManyField('IziColour', blank=True)
    price_from = models.FloatField(default=0, null=True, blank=True)
    price_to = models.FloatField(default=1000, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    @property
    def is_related(self):
        if Category.objects.filter(parent=self).count() > 0:
            return True
        return False

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        try:
            self.name_lower = str(self.name).lower()
        except:
            self.name_lower = ''
            pass
        self.slug = slugify('%s-%s' % (self.name, self.code))
        super(ParentCategory, self).save()

    @property
    def languages(self):
        data = []
        languages = Language.objects.all()
        for i in languages:
            context = {
                'lang_id': i.id,
                'lang_code': i.code,
                'lang_name': i.name,
            }
            try:
                tr = TranslationParentCategory.objects.get(language=i,
                                                           parent_category=self)
                context['translation'] = tr.name
                context['is_active'] = tr.is_active
            except:
                context['translation'] = None
                context['is_active'] = None
            data.append(context)
        return data


class TranslationParentCategory(models.Model):
    class Meta:
        verbose_name = 'род. категорию (перевод)'
        verbose_name_plural = 'род. категории (перевод)'

    parent_category = models.ForeignKey(ParentCategory, on_delete=SET_NULL, null=True)
    language = models.ForeignKey(Language, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100, null=True, blank=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name_lower = self.name.lower()
        self.slug = slugify('%s' % self.name)
        super(TranslationParentCategory, self).save()


class Category(models.Model):
    class Meta:
        verbose_name = 'категорию (izishop)'
        verbose_name_plural = 'категории (izishop)'

    parent = models.ForeignKey(ParentCategory, null=True, blank=True, related_name='childs', on_delete=SET_NULL)
    name = models.CharField(max_length=1000, null=True)
    average_weight = models.FloatField(null=True, blank=True, default=0.5)
    slug = models.CharField(max_length=100, null=True, blank=True)
    position = models.IntegerField(null=True, blank=True)
    code = models.CharField(max_length=100, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    has_products = models.BooleanField(default=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(upload_to=file_upload_to, null=True, blank=True)
    size_table = models.ImageField(upload_to=file_upload_to, null=True, blank=True)
    sizes = models.ManyToManyField('Size', blank=True)
    colours = models.ManyToManyField('IziColour', blank=True)
    price_from = models.FloatField(default=0, null=True, blank=True)
    price_to = models.FloatField(default=1000, null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name_lower = self.name.lower()
        self.slug = slugify('%s-%s' % (self.name, self.code))
        super(Category, self).save()

    @property
    def is_related(self):
        if VendCategory.objects.filter(category=self).count() > 0:
            return True
        return False

    @property
    def languages(self):
        data = []
        languages = Language.objects.all()
        for i in languages:
            context = {
                'lang_id': i.id,
                'lang_code': i.code,
                'lang_name': i.name,
            }
            try:
                tr = TranslationCategory.objects.get(language=i, category=self)
                context['translation'] = tr.name
                context['is_active'] = tr.is_active
            except:
                context['translation'] = None
                context['is_active'] = None
            data.append(context)
        return data


class TranslationCategory(models.Model):
    class Meta:
        verbose_name = 'категорию (перевод)'
        verbose_name_plural = 'категории (перевод)'

    category = models.ForeignKey(Category, null=True, blank=True, related_name='translations', on_delete=SET_NULL)
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100, null=True, blank=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    language = models.ForeignKey(Language, null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name_lower = self.name.lower()
        self.slug = slugify('%s' % self.name)
        super(TranslationCategory, self).save()


class Document(models.Model):
    class Meta:
        verbose_name = 'документ'
        verbose_name_plural = 'документы'
        ordering = ['step']

    user = models.ForeignKey(User, null=True, blank=True, on_delete=SET_NULL)
    department = models.ForeignKey('VendDepartment', null=True, blank=True, on_delete=models.SET_NULL)
    brand = models.ForeignKey('Brand', null=True, blank=True, on_delete=models.SET_NULL)
    status = models.IntegerField(default=1)
    is_new = models.BooleanField(default=False)
    step = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.updated_at.__str__()

    def set_to_origin_step(self):
        self.step = STEP_ONE
        self.save()

    def set_to_last_step(self):
        self.step = STEP_END
        self.save()

    def set_to_next_step(self):
        if self.step<5:
            self.step = self.step+1
            self.save()

    @property
    def no_stock_or_price(self):
        count = OriginalProduct.objects.filter(Q(original_price=0) | Q(stock=False), document=self).count()
        return count

    @property
    def products(self):
        return OriginalProduct.objects.filter(document=self).count()


class DocumentProduct(models.Model):
    product = models.ForeignKey('OriginalProduct', null=True, related_name='document_product',
                                on_delete=models.SET_NULL)
    document = models.ForeignKey(Document, null=True, blank=True, on_delete=models.SET_NULL)
    step = models.IntegerField(default=1, null=True, blank=True)


class DocumentComment(models.Model):
    document = models.ForeignKey(Document, null=True, blank=True, related_name='comments', on_delete=models.SET_NULL)
    text = models.TextField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class Tag(models.Model):
    class Meta:
        verbose_name = 'тэг'
        verbose_name_plural = 'тэги'
        ordering = 'name'.split()

    TYPES = (
        (0, ''),
        (1, 'Бренд'),
        (2, 'Категория'),
        (3, 'Цвет'),
    )

    name = models.CharField(max_length=1000, null=False, unique=True)
    type = models.IntegerField(choices=TYPES, default=0)

    def __str__(self):
        return self.name


class Link(models.Model):
    class Meta:
        ordering = ['updated_at']
        verbose_name_plural = 'Ссылка'
        verbose_name = 'Ссылки'

    url = models.URLField()
    tr_category = models.ForeignKey('VendCategory', null=True, related_name='tr_categories', on_delete=SET_NULL)
    status = models.IntegerField(default=4, choices=STATUSES)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.url


class OriginalProduct(models.Model):
    class Meta:
        verbose_name_plural = 'товары (vend)'
        verbose_name = 'товар (vend)'

    title = models.CharField(max_length=100)
    title_lower = models.CharField(max_length=100, null=True, blank=True)
    product_id = models.CharField(max_length=1000)
    product_code = models.CharField(max_length=1000, blank=True, null=True, default='')
    discount_price = models.FloatField(null=True, blank=True)
    selling_price = models.FloatField(null=True, blank=True)
    original_price = models.FloatField(null=True, blank=True)
    difference = models.FloatField(null=True, blank=True, default=0)
    currency = models.CharField(max_length=100, default='TL')
    colour = models.ForeignKey('VendColour', blank=True, null=True, on_delete=SET_NULL)
    colour_code = models.CharField(max_length=100, blank=True, null=True)
    # status = models.IntegerField(default=1)
    link = models.OneToOneField(Link, null=True, related_name='originalproduct', on_delete=SET_NULL)
    brand = models.ForeignKey('Brand', null=True, blank=True, on_delete=SET_NULL)
    department = models.ForeignKey('VendDepartment', null=True, blank=True, on_delete=SET_NULL)
    document = models.ForeignKey('Document', null=True, blank=True, on_delete=SET_NULL)
    category = models.ForeignKey('VendCategory', null=True, blank=True, on_delete=SET_NULL)
    is_rush_delivery = models.BooleanField(default=False)
    stock = models.BooleanField(default=True)
    is_free_argo = models.BooleanField(default=False)
    delivery_date = models.TextField(null=True, blank=True)
    images = models.TextField(null=True, blank=True)
    promotions = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField('Tag', blank=True)

    izi_department = models.ForeignKey('Department', null=True, blank=True, on_delete=SET_NULL)
    izi_parent_category = models.ForeignKey('ParentCategory', null=True, blank=True, on_delete=SET_NULL)
    izi_category = models.ForeignKey('Category', null=True, blank=True, on_delete=SET_NULL)
    izi_colour = models.ForeignKey('IziColour', null=True, blank=True, on_delete=SET_NULL)
    izi_content = models.ForeignKey('Content', null=True, blank=True, on_delete=SET_NULL)
    is_hit = models.BooleanField(default=False)
    is_marketolog = models.BooleanField(default=False)
    is_sellable = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.title_lower = self.title.lower()
        try:
            self.difference = self.original_price - self.discount_price
        except:
            pass
        super(OriginalProduct, self).save()

    @property
    def delivery_day(self):
        try:
            day = self.brand.delivery_day + Country.objects.all().first().delivery_day
        except:
            day = 8
        return day

    @property
    def izi_link(self):
        try:
            link = 'https://izishop.kg/%s/%s/%s/%s' % (
                self.izi_department.slug, self.izi_parent_category.slug,
                self.izi_category.slug,
                self.id)
        except:
            link = 'https://izishop.kg/'
        return link

    @property
    def shipping_price(self):
        try:
            price = self.izi_category.average_weight * self.brand.delivery_price
            percent = (price / 100) * self.brand.delivery_percent
            return round((price + percent) * 80)
        except:
            pass
        return 100


# class ProductImage(models.Model):
#     url = models.CharField(max_length=100)
#     original_product = models.ForeignKey(OriginalProduct, related_name='images')


class Variant(models.Model):
    class Meta:
        verbose_name_plural = 'Варианты'
        verbose_name = 'вариант'

    tr_size = models.ForeignKey('VendSize', null=True, on_delete=models.SET_NULL)
    original_product = models.ForeignKey(OriginalProduct, related_name='variants', on_delete=SET_NULL, null=True)
    stock = models.BooleanField(default=False)

    def __str__(self):
        return self.tr_size.name


class VendSize(models.Model):
    class Meta:
        verbose_name_plural = 'Размер (vend)'
        verbose_name = 'Размеры (vend)'

    name = models.CharField(max_length=100)
    izi_size = models.ForeignKey('Size', null=True, on_delete=SET_NULL)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(VendSize, self).save()


class Size(models.Model):
    class Meta:
        verbose_name_plural = 'Размер (izishop)'
        verbose_name = 'Размеры (izishop)'

    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100, null=True, blank=True, default='')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def is_related(self):
        if VendSize.objects.filter(izi_size=self).count() > 0:
            return True
        return False

    @property
    def languages(self):
        data = []
        for i in Language.objects.all():
            context = {
                'lang_id': i.id,
                'lang_name': i.name,
                'lang_code': i.code,
            }
            try:
                tr = TranslationSize.objects.get(size=self, language=i)
                context['translation'] = tr.name
            except:
                context['translation'] = None
            data.append(context)
        return data


class TranslationSize(models.Model):
    class Meta:
        verbose_name = 'размер (перевод)'
        verbose_name_plural = 'размер (перевод)'

    size = models.ForeignKey(Size, null=True, blank=True, related_name='translations', on_delete=SET_NULL)
    name = models.CharField(max_length=100, null=True, blank=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    language = models.ForeignKey(Language, null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        # self.name_lower = self.name.lower()
        super(TranslationSize, self).save()


class Content(models.Model):
    class Meta:
        verbose_name_plural = 'Состав (izishop)'
        verbose_name = 'Состав (izishop)'

    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100, null=True, blank=True, default='')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def is_related(self):
        return False

    @property
    def languages(self):
        data = []
        for i in Language.objects.all():
            tr = None
            try:
                tr = TranslationContent.objects.filter(content=self,
                                                       language=i)[0]
            except:
                pass
            context = {
                'lang_id': i.id,
                'lang_name': i.name,
                'lang_code': i.code,
            }
            if tr is not None:
                context['translation'] = tr.name
            else:
                context['translation'] = None
            data.append(context)
        return data


class TranslationContent(models.Model):
    class Meta:
        verbose_name = 'Состав (перевод)'
        verbose_name_plural = 'Состав (перевод)'

    content = models.ForeignKey(Content, null=True, blank=True, related_name='translations', on_delete=SET_NULL)
    name = models.CharField(max_length=100, null=True, blank=True)
    name_lower = models.CharField(max_length=100, null=True, blank=True)
    language = models.ForeignKey(Language, null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        # self.name_lower = self.name.lower()
        super(TranslationContent, self).save()


class Product(models.Model):
    class Meta:
        verbose_name_plural = 'товары (izishop)'
        verbose_name = 'товар (izishop)'

    title = models.CharField(max_length=100)
    title_lower = models.CharField(max_length=100, null=True, blank=True)
    link = models.OneToOneField(Link, null=True, on_delete=SET_NULL)
    description = models.TextField()
    description_lower = models.TextField(null=True, blank=True)
    is_sellable = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    department = models.ForeignKey('Department', null=True, blank=True, on_delete=SET_NULL)
    category = models.ForeignKey('Category', null=True, blank=True, on_delete=SET_NULL)
    colour = models.ForeignKey('IziColour', null=True, blank=True, on_delete=SET_NULL)
    content = models.ForeignKey('Content', null=True, blank=True, on_delete=SET_NULL)
    is_hit = models.BooleanField(default=False)
    is_marketolog = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.title_lower = self.title.lower()
        self.description_lower = self.description.lower()
        super(Product, self).save()


class VendDepartment(models.Model):
    class Meta:
        verbose_name = 'отделение (vend)'
        verbose_name_plural = 'отделения (vend)'

    name = models.CharField(max_length=100)
    link = models.CharField(max_length=1000, null=True)
    brand = models.ForeignKey(Brand, related_name='brand', on_delete=SET_NULL, null=True)
    is_active = models.BooleanField(default=True)
    department = models.ForeignKey(Department, null=True, related_name='departments', on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class VendCategory(models.Model):
    class Meta:
        verbose_name = 'категория (vend)'
        verbose_name_plural = 'категории (vend)'

    name = models.CharField(max_length=100)
    link = models.CharField(max_length=1000, null=True)
    department = models.ForeignKey(VendDepartment, null=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField(default=True)
    category = models.ForeignKey(Category, null=True, blank=True, on_delete=SET_NULL, related_name='categories')

    def __str__(self):
        return self.name
