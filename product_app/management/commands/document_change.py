from django.core.management.base import BaseCommand
from django.db import transaction

from product_app.models import OriginalProduct, DocumentProduct


class Command(BaseCommand):
    help = 'The Zen of Python'

    def handle(self, *args, **options):
        with transaction.atomic():
            count = 0
            for original_product in OriginalProduct.objects.all():
                count += 1
                print(count)
                try:
                    document_product = DocumentProduct.objects.filter(product=original_product)[0]
                    original_product.document = document_product.document
                    original_product.save()
                except:
                    continue
