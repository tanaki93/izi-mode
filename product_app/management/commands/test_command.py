from django.core.management.base import BaseCommand
from django.db import transaction

from product_app.models import OriginalProduct, DocumentProduct


class Command(BaseCommand):
    help = 'The Zen of Python'

    def handle(self, *args, **options):
        count = 0
        for original_product in OriginalProduct.objects.all():
            count += 1
            print(count)
            try:
                product = original_product.link.product
            except:
                continue
            try:
                original_product.izi_department_id = product.department_id

            except:
                pass
            try:
                original_product.izi_parent_category_id = product.category.parent_id
            except:
                pass
            try:
                original_product.izi_category_id = product.category_id
            except:
                pass

            try:
                original_product.izi_colour_id = product.colour_id
            except:
                pass
            try:
                original_product.izi_content_id = product.content_id
            except:
                pass
            original_product.save(update_fields=['izi_department',
                                                 'izi_parent_category_id',
                                                 'izi_category_id',
                                                 'izi_colour_id',
                                                 'izi_content_id'])