import datetime
import itertools
from ctypes import Union
from pprint import pprint

from django.contrib.auth import get_user_model
from django.db.models import QuerySet, Q
from rest_framework import status
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.response import Response

from product_app.models import Brand, Document, VendDepartment, OriginalProduct, IziColour, Content, DocumentComment, \
    Country, Currency, City, DeliveryPoint, Language, ExchangeRate, ExchangeValue, BrandCountry, Department, \
    TranslationDepartment, Category, TranslationCategory, TranslationColour, VendColour, VendSize, ParentCategory, \
    TranslationParentCategory, Size, TranslationSize, TranslationContent
from user_app.admin.serializers import BrandProcessSerializer, IziColourSerializer, ContentSerializer, CommentSerializer
from user_app.main.serializers import MainProductSerializer

User = get_user_model()


class BrandService:
    model = Brand

    @classmethod
    def get(cls, **filters):
        try:
            return Brand.objects.get(**filters)
        except Brand.DoesNotExist:
            raise NotFound('Brand not found')

    @classmethod
    def create(cls, delivery_price: int,
               delivery_day: int,
               delivery_percent: int,
               is_active: bool,
               name: str, link: str, code: str, currency_id, countries) -> model:
        try:
            brand = Brand.objects.create(
                currency_id=currency_id,
                delivery_price=delivery_price,
                delivery_day=delivery_day,
                delivery_percent=delivery_percent,
                name=name,
                code=code,
                link=link,
                is_active=is_active,
            )
            for i in countries:
                brand_country = BrandCountry.objects.create(
                    brand=brand,
                    country_id=i['country_id'],
                    mark_up=i['mark_up'],
                    round_digit=i['round_digit'],
                    round_to=i['round_to'],
                )
                brand_country.save()
            return brand
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def create_document(cls, brand: Brand):
        try:
            documents = Document.objects.filter(brand=brand)
            if documents.count() == 0:
                for i in VendDepartment.objects.filter(brand=brand):
                    document = Document.objects.create(department=i, brand=brand)
                    document.save()
                    OriginalProduct.objects.filter(department=i).update(document=document)
            else:
                document = Document.objects.create(brand=brand)
                OriginalProduct.objects.filter(document__isnull=True, brand=brand).update(document=document)
            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            raise ValidationError('Error while creating distributions: {e}'.format(e=str(e)))

    @classmethod
    def update(cls, brand_id, delivery_price: int,
               delivery_day: int,
               delivery_percent: int,
               is_active: bool,
               name: str, link: str, code: str, currency_id, countries) -> model:

        try:
            brand = Brand.objects.get(id=brand_id)
            if currency_id != 0:
                brand.currency_id = currency_id
            brand.delivery_price = delivery_price
            brand.delivery_day = delivery_day
            brand.delivery_percent = delivery_percent
            brand.name = name
            brand.code = code
            brand.link = link
            brand.is_active = is_active
            brand.save()
            for country in countries:
                try:
                    brand_country = \
                        BrandCountry.objects.get(brand_id=brand.id,
                                                 country_id=int(country['country_id']))
                except:
                    brand_country = BrandCountry()
                    brand_country.brand = brand
                    brand_country.country_id = int(country['country_id'])
                brand_country.mark_up = country['mark_up']
                brand_country.round_digit = country['round_digit']
                brand_country.round_to = country['round_to']
                brand_country.save()
            return brand
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


def get_from_price(price):
    brand_country = None
    try:
        brand_country = BrandCountry.objects.all()[0]
    except:
        pass
    new_price = 0
    try:
        exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                            to_currency=brand_country.country.currency)
        new_price = (price / 1.15) / exchange.value
    except:
        pass
    return new_price


def get_to_price(price):
    brand_country = None
    try:
        brand_country = BrandCountry.objects.all()[0]
    except:
        pass
    new_price = 10000000
    try:
        exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                            to_currency=brand_country.country.currency)
        new_price = (price / 1.15) / exchange.value
    except:
        pass
    return new_price


class ProductService:
    model = OriginalProduct

    @classmethod
    def get(cls, **filters):
        try:
            return OriginalProduct.objects.get(**filters)
        except OriginalProduct.DoesNotExist:
            raise NotFound('Product not found')

    @classmethod
    def get_admin_products(self, filters, document):
        query = filters.get('query', '')
        query_filters = {
            'stock': True,
            'original_price__gt': 0,
        }
        if document is not None:
            query_filters['document'] = document
        exclude_filters = {}
        if query != "":
            if query[0] == '-':
                exclude_filters['title_lower__contains'] = query[1:]
            else:
                query_filters['title_lower__contains'] = query
        try:
            department_id = int(filters.get('department_id', ''))
            query_filters['izi_department_id'] = department_id
        except:
            pass
        try:
            category_id = int(filters.get('category_id', ''))
            query_filters['izi_category_id'] = category_id
        except:
            pass
        try:
            colour_id = int(filters.get('colour_id', ''))
            query_filters['izi_colour_id'] = colour_id

        except:
            pass
        try:
            content_id = int(filters.get('content_id', ''))
            if content_id != 0:
                query_filters['izi_content_id'] = content_id
            else:
                query_filters['izi_content__isnull'] = True
        except:
            pass
        return OriginalProduct.objects.filter(**query_filters).exclude(**exclude_filters)

    @classmethod
    def get_client_random_products(self):
        query_filters = {
            'stock': True,
            'selling_price__gt': 0,
            'is_active': True,
            'document__step': 100,
            'brand__is_active': True,
            'izi_department__is_active': True,
            'izi_category__is_active': True,
            'izi_parent_category__is_active': True,
            'is_sellable': True,
            'is_hit': True,
        }
        return OriginalProduct.objects.filter(**query_filters) \
                   .order_by('?')[0:6]

    @classmethod
    def get_client_izi_products(self, filters, request):
        query_filters = {
            'stock': True,
            'selling_price__gt': 0,
            'is_active': True,
            'document__step': 100,
            'brand__is_active': True,
            'izi_department__is_active': True,
            'izi_category__is_active': True,
            'izi_parent_category__is_active': True,
            'is_sellable': True,
        }
        js = []
        try:
            js = list(request.data.get('brands'))
        except:
            pass
        if len(js) > 0:
            query_filters['brand_id__in'] = js

        colors = []
        try:
            colors = list(request.data.get('colours'))
        except:
            pass

        if len(colors) > 0:
            query_filters['izi_colour_id__in'] = colors
        sizes = []
        try:
            sizes = list(request.data.get('sizes'))
        except:
            pass
        if len(sizes) > 0:
            query_filters['variants__stock'] = True
            query_filters['variants__tr_size__izi_size__name__in'] = sizes
        try:
            department_id = int(filters.get('department_id', ''))
            if department_id != 0:
                query_filters['izi_department_id'] = department_id
            else:
                query_filters['izi_department__isnull'] = True
        except:
            pass
        try:
            parent_category_id = int(filters.get('parent_category_id', ''))
            if parent_category_id != 0:
                query_filters['izi_parent_category_id'] = parent_category_id
            else:
                query_filters['izi_parent_category__isnull'] = True
        except:
            pass
        try:
            category_id = int(filters.get('category_id', ''))
            if category_id != 0:
                query_filters['izi_category_id'] = category_id
            else:
                query_filters['izi_category__isnull'] = True
        except:
            pass
        price_from = None
        try:
            price_from = int(request.data.get('price_from'))
        except:
            pass
        if price_from is not None:
            query_filters['selling_price__gte'] = get_from_price(price_from)
        price_to = None
        try:
            price_to = int(request.data.get('price_to'))
        except:
            pass
        if price_to is not None:
            query_filters['selling_price__lte'] = get_to_price(price_to)
        order_by = ''
        try:
            order_by = str(request.data.get('order_by', ''))
        except:
            pass
        if order_by != '':
            result = OriginalProduct.objects.filter(**query_filters).order_by(order_by)
        else:
            products = OriginalProduct.objects.filter(**query_filters).order_by('-document__is_new',
                                                                                'izi_parent_category__position',
                                                                                'izi_category__position',
                                                                                '-difference')
            q = itertools.chain(
                products.filter(link__product__is_hit=True),
                products.filter(link__product__is_hit=False))
            result = [x for x in q]
        count_products = len(result)
        pages = count_products // 15
        if count_products % 15 != 0:
            pages += 1
        page = 1
        try:
            page = int(filters.get('page', '1'))
        except:
            pass
        data = {
            'data': {
                'pages': pages,
                'page': page,
                'count': count_products,
            },
            'objects': MainProductSerializer(result[(page - 1) * 15: page * 15], many=True,
                                             context={'request': request}).data
        }
        return data

    @classmethod
    def get_parsing_links(self, filters):
        query_filters = {
            'stock': True,
            'selling_price__gt': 0,
            'is_active': True,
            'document__step': 100,
            'brand__is_active': True,
            'izi_department__is_active': True,
            'izi_category__is_active': True,
            'izi_parent_category__is_active': True,
            'is_sellable': True,
        }

        try:
            category_id = int(filters.get('category_id', ''))
            if category_id != 0:
                query_filters['izi_category_id'] = category_id
            else:
                query_filters['izi_category__isnull'] = True
        except:
            pass
        products = OriginalProduct.objects.filter(**query_filters).order_by('-document__is_new',
                                                                            'izi_parent_category__position',
                                                                            'izi_category__position',
                                                                            '-difference', '-updated_at')
        q = itertools.chain(
            products.filter(link__product__is_hit=True),
            products.filter(link__product__is_hit=False))
        result = []
        count = 0
        for i in q:
            if i.link is not None:
                count += 1
                result.append(i.link)
            if count == 15:
                break

        return result

    @classmethod
    def get_admin_izi_products(self, filters):
        query = filters.get('query', '')
        query_filters = {
            'stock': True,
            'selling_price__gt': 0,
            'document__step': 100,
        }
        exclude_filters = {}
        if query != "":
            if query[0] == '-':
                exclude_filters['title_lower__contains'] = query[1:]
            else:
                # try:
                #     id = int(query)
                #     query_filters['id'] = id
                # except:
                #     pass
                query_filters['title_lower__contains'] = query
        try:
            brand_id = int(filters.get('brand_id', ''))
            if brand_id != 0:
                query_filters['brand_id'] = brand_id
            else:
                query_filters['brand__isnull'] = True
        except:
            pass
        try:
            department_id = int(filters.get('department_id', ''))
            if department_id != 0:
                query_filters['izi_department_id'] = department_id
            else:
                query_filters['izi_department__isnull'] = True
        except:
            pass
        try:
            category_id = int(filters.get('category_id', ''))
            if category_id != 0:
                query_filters['izi_category_id'] = category_id
            else:
                query_filters['izi_category__isnull'] = True
        except:
            pass
        try:
            colour_id = int(filters.get('colour_id', ''))
            if colour_id != 0:
                query_filters['izi_colour_id'] = colour_id
            else:
                query_filters['izi_colour__isnull'] = True

        except:
            pass
        try:
            content_id = int(filters.get('content_id', ''))
            if content_id != 0:
                query_filters['izi_content_id'] = content_id
            else:
                query_filters['izi_content__isnull'] = True
        except:
            pass
        return OriginalProduct.objects.filter(**query_filters).exclude(**exclude_filters).order_by('-is_hit',
                                                                                                   '-created_at')


class DocumentService:
    model = Document

    @classmethod
    def get(cls, **filters):
        try:
            return Document.objects.get(**filters)
        except Document.DoesNotExist:
            raise NotFound('Document not found')

    @classmethod
    def get_operator_documents(self, user):
        return Document.objects.filter(user=user)

    @classmethod
    def get_admin_documents(cls, filters) -> QuerySet:
        context = {}
        try:
            brand_id = int(filters.get('brand_id'))
            context['brand_id'] = brand_id
        except:
            pass
        try:
            user_id = int(filters.get('user_id'))
            context['user_id'] = user_id
        except:
            pass
        date_from = datetime.datetime(year=2018, month=1, day=1)
        try:
            date_from = datetime.datetime.strptime(filters.get('date_from'), "%Y-%m-%d")
        except:
            pass
        context['updated_at__gte'] = date_from
        date_to = datetime.datetime(year=2030, month=1, day=1)
        try:
            date_to = datetime.datetime.strptime(filters.get('date_to'), "%Y-%m-%d")
        except:
            pass
        context['updated_at__lte'] = date_to
        return Document.objects.select_related('brand', 'user').filter(**context)

    @classmethod
    def get_document_data(cls, pk):
        document = Document.objects.get(id=pk)
        department = document.department
        data = {
            'step': document.step,
            'id': document.id,
            'brand': BrandProcessSerializer(document.brand).data,
            'colours': IziColourSerializer(IziColour.objects.all(), many=True).data,
            'comments': CommentSerializer(DocumentComment.objects.filter(document=document), many=True).data,
            'contents': ContentSerializer(Content.objects.all(), many=True).data,
        }
        if department is None:
            data['department_id'] = None
            data['department_name'] = None
        else:
            data['department_id'] = department.id
            data['department_name'] = department.name
        return data

    @classmethod
    def set_comment(cls, pk, comment):
        document = Document.objects.get(id=pk)
        document_comment = DocumentComment.objects.create(document=document, text=comment)
        document_comment.save()
        document.set_to_origin_step()

    @classmethod
    def set_to_last_step(cls, pk):
        document = Document.objects.get(id=pk)
        document.set_to_last_step()

    @classmethod
    def get_all_documents(self):
        return Document.objects.all()

    @classmethod
    def set_user(cls, pk, user):
        document = Document.objects.get(id=pk)
        document.user = user
        document.save()


class CurrencyService:
    model = Currency

    @classmethod
    def get(cls, **filters):
        try:
            return Currency.objects.get(**filters)
        except Currency.DoesNotExist:
            raise NotFound('Currency not found')

    @classmethod
    def get_all_currencies(cls):
        return Currency.objects.all()

    @classmethod
    def create(cls, code: str, name: str, code_name: str, is_active: bool) -> model:
        try:

            return cls.model.objects.create(
                code=code.upper(),
                name=name,
                code_name=code_name,
                is_active=is_active,
            )
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def update(cls, code: str, name: str, code_name: str, is_active: bool, currency_id) -> model:
        try:
            currency = Currency.objects.get(id=currency_id)
            currency.code = code.upper()
            currency.name = name
            currency.code_name = code_name
            currency.is_active = is_active
            currency.save()
            return currency
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


class CountryService:
    model = Country

    @classmethod
    def get(cls, **filters):
        try:
            return Country.objects.get(**filters)
        except Country.DoesNotExist:
            raise NotFound('Country not found')

    @classmethod
    def get_all_countries(cls):
        return Country.objects.all()

    @classmethod
    def create(cls, language_id, currency_id, delivery_price: int,
               day_count_newest: int, delivery_percent: int,
               delivery_day: int, code: str, name: str, is_active: bool) -> model:
        try:

            return cls.model.objects.create(
                language_id=language_id,
                currency_id=currency_id,
                delivery_price=delivery_price,
                day_count_newest=day_count_newest,
                delivery_percent=delivery_percent,
                delivery_day=delivery_day,
                code=code.upper(),
                name=name,
                is_active=is_active,
            )
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def update(cls, language_id, currency_id, delivery_price: int, is_active: bool, country_id,
               day_count_newest: int, delivery_percent: int, delivery_day: int, code: str, name: str) -> model:
        if language_id == 0:
            language_id = None
        if currency_id == 0:
            currency_id = None
        try:
            country = Country.objects.get(id=country_id)
            country.language_id = language_id
            country.currency_id = currency_id
            if delivery_price != 0:
                country.delivery_price = delivery_price
            if day_count_newest != 0:
                country.day_count_newest = day_count_newest
            if delivery_percent != 0:
                country.delivery_percent = delivery_percent
            if delivery_day != 0:
                country.delivery_day = delivery_day
            country.code = code.upper()
            country.name = name
            country.is_active = is_active
            country.save()
            return country
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


class CityService:
    model = City

    @classmethod
    def get(cls, **filters):
        try:
            return City.objects.get(**filters)
        except City.DoesNotExist:
            raise NotFound('City not found')

    @classmethod
    def get_all_cities(cls):
        return City.objects.all()

    @classmethod
    def create(cls, country_id, delivery_price: int, is_active: bool,
               delivery_percent: int, delivery_day: int, name: str) -> model:
        try:

            return cls.model.objects.create(
                country_id=country_id,
                delivery_price=delivery_price,
                delivery_percent=delivery_percent,
                delivery_day=delivery_day,
                name=name,
                is_active=is_active,
            )
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def update(cls, country_id, delivery_price: int, city_id, is_active: bool,
               delivery_percent: int, delivery_day: int, name: str) -> model:
        if country_id == 0:
            country_id = None

        try:
            city = City.objects.get(id=city_id)
            city.country_id = country_id
            city.delivery_price = delivery_price
            city.delivery_percent = delivery_percent
            city.delivery_day = delivery_day
            city.name = name
            city.is_active = is_active
            city.save()
            return city
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


class PointService:
    model = DeliveryPoint

    @classmethod
    def get(cls, **filters):
        try:
            return DeliveryPoint.objects.get(**filters)
        except DeliveryPoint.DoesNotExist:
            raise NotFound('DeliveryPoint not found')

    @classmethod
    def get_all_points(cls):
        return DeliveryPoint.objects.all()

    @classmethod
    def create(cls, city_id, delivery_price: int, is_active: bool,
               lat: float, lng: float, name: str) -> model:
        try:

            return cls.model.objects.create(
                city_id=city_id,
                delivery_price=delivery_price,
                lat=lat,
                lng=lng,
                name=name,
                is_active=is_active,
            )
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def update(cls, city_id, delivery_price: int, is_active: bool,
               lat: float, lng: float, name: str, point_id) -> model:

        try:
            point = DeliveryPoint.objects.get(id=point_id)
            if city_id != 0:
                point.city_id = city_id
            point.delivery_price = delivery_price
            point.lat = lat
            point.lng = lng
            point.name = name
            point.is_active = is_active
            point.save()
            return point
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


class LanguageService:
    model = Language

    @classmethod
    def get(cls, **filters):
        try:
            return Language.objects.get(**filters)
        except Language.DoesNotExist:
            raise NotFound('Language not found')

    @classmethod
    def get_all_languages(cls):
        return Language.objects.all()

    @classmethod
    def create(cls, code: str, name: str, is_active: bool, is_translate) -> model:
        try:

            return cls.model.objects.create(
                code=code.upper(),
                name=name,
                is_translate=is_translate,
                is_active=is_active,
            )
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def update(cls, code: str, name: str, is_active: bool, is_translate, language_id) -> model:
        try:
            language = Language.objects.get(id=language_id)
            language.code = code.upper()
            language.name = name
            language.is_translate = is_translate
            language.is_active = is_active
            language.save()
            return language
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


class ExchangeService:
    model = ExchangeRate

    @classmethod
    def get(cls, **filters):
        try:
            return ExchangeRate.objects.get(**filters)
        except ExchangeRate.DoesNotExist:
            raise NotFound('ExchangeRate not found')

    @classmethod
    def get_all_exchanges(cls):
        return ExchangeRate.objects.all()

    @classmethod
    def create(cls, from_currency_id, to_currency_id, value, is_active, date) -> model:
        try:
            from_currency = from_currency_id
            to_currency = to_currency_id
            if from_currency is not None and to_currency is not None:
                exchange = ExchangeRate.objects.create(from_currency_id=from_currency, to_currency_id=to_currency,
                                                       value=value, is_active=is_active)
                try:
                    date = datetime.datetime.strptime(date, "%Y-%m-%d")
                    exchange.date = date
                except:
                    pass
                exchange.save()
                exchange_value = ExchangeValue.objects.create(value=value, exchange=exchange,
                                                              date=exchange.date)
                exchange_value.save()

                return exchange
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def update(cls, from_currency_id, to_currency_id, value, is_active, date, exchange_id) -> model:
        try:
            from_currency = from_currency_id
            to_currency = to_currency_id
            exchange = ExchangeRate.objects.get(id=exchange_id)
            if from_currency is not None and to_currency is not None:
                exchange.to_currency_id = to_currency
                exchange.from_currency_id = from_currency
                exchange.value = value
                try:
                    date = datetime.datetime.strptime(date, "%Y-%m-%d")
                    exchange.date = date
                except:
                    pass
                exchange.is_active = is_active
                exchange_value = ExchangeValue.objects.create(value=value, exchange=exchange,
                                                              date=exchange.date)
                exchange_value.save()
                exchange.save()
            return exchange
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


class DepartmentService:
    model = Department

    @classmethod
    def update(cls, code: str, name: str, is_active: bool,
               languages: list, position: int, department_id: int) -> model:
        department = Department.objects.get(id=department_id)
        department.code = code
        department.name = name
        department.position = position
        department.is_active = is_active
        department.save()
        for i in languages:
            tr = None
            try:
                tr = TranslationDepartment.objects.get(department=department, language_id=int(i['lang_id']))
            except:
                pass
            if tr is None:
                tr = TranslationDepartment.objects.create(department=department, language_id=int(i['lang_id']),
                                                          name=i['translation'], is_active=i['is_active'])
            else:
                tr.name = i['translation']
            tr.save()
        return department

    @classmethod
    def assign(cls, code: str, name: str, is_active: bool,
               department_id,
               position,
               department,
               languages) -> model:
        try:
            if department_id == 0:
                depart = cls.create(code, name, is_active, languages, position, department_id)
            else:
                depart = Department.objects.get(id=department_id)
            department.department = depart
            department.save()
            return department
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def set_active(cls, is_active: bool, department) -> model:
        try:
            department.is_active = is_active
            department.save()
            return department
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))

    @classmethod
    def create(cls, code: str, name: str, is_active: bool,
               languages: list, position: int, department_id) -> model:
        depart = cls.model.objects.create(
            code=code.upper(),
            name=name,
            position=position,
            is_active=is_active,
        )
        for i in languages:
            TranslationDepartment.objects.create(department=depart, language_id=int(i['lang_id']),
                                                 name=i['translation'], is_active=i.get('is_active', True))
        return depart


class CategoryService:
    model = Category

    @classmethod
    def create(cls, code: str, name: str, is_active: bool,
               languages: list, position: int, category_id: int, parent_id,
               average_weight) -> model:
        depart = cls.model.objects.create(
            code=code.upper(),
            name=name,
            position=position,
            is_active=is_active,
            parent_id=parent_id,
            average_weight=average_weight,
        )
        for i in languages:
            TranslationCategory.objects.create(category=depart, language_id=int(i['lang_id']),
                                               name=i['translation'], is_active=i.get('is_active', True))
        return depart

    @classmethod
    def assign(cls, code: str, name: str, is_active: bool,
               category_id,
               position,
               category,
               languages,
               parent_id,
               average_weight) -> model:
        try:
            if category_id == 0:
                depart = cls.create(code, name, is_active, languages,
                                    position, category_id, parent_id, average_weight)
            else:
                depart = Category.objects.get(id=category_id)
            category.category = depart
            category.save()
            return category
        except Exception as e:
            raise ValidationError('Error while creating hospital needs: {e}'.format(e=str(e)))

    @classmethod
    def set_active(cls, is_active: bool, category) -> model:
        try:
            category.is_active = is_active
            category.save()
            return category
        except Exception as e:
            raise ValidationError('Error while updating country: {e}'.format(e=str(e)))


class ColourService:
    model = IziColour

    @classmethod
    def create(cls, code: str, name: str, hex: str,
               languages: list, is_active) -> model:
        depart = cls.model.objects.create(
            code=code.upper(),
            name=name,
            hex=hex,
            is_active=True,
        )
        for i in languages:
            TranslationColour.objects.create(colour=depart, language_id=int(i['lang_id']),
                                             name=i['translation'])
        return depart

    @classmethod
    def update(cls, code: str, name: str, is_active: bool,
               languages: list, hex: str, colour) -> model:
        colour.code = code.upper()
        colour.name = name
        colour.hex = hex
        colour.is_active = is_active
        colour.save()
        for i in languages:
            tr = None
            try:
                tr = TranslationColour.objects.get(colour=colour,
                                                   language_id=int(i['lang_id']))
            except:
                pass
            if tr is None:
                tr = TranslationColour.objects.create(colour=colour,
                                                      language_id=
                                                      int(i['lang_id']),
                                                      name=i['translation'], )
            else:
                tr.name = i['translation']
            tr.save()
        return colour


class VendColourService:
    model = VendColour

    @classmethod
    def assign(cls, vend_colour, izi_colour_id) -> model:
        if izi_colour_id > 0:
            vend_colour.izi_colour_id = izi_colour_id
        else:
            vend_colour.izi_colour = None
        vend_colour.save()
        return vend_colour


class VendSizeService:
    model = VendSize

    @classmethod
    def assign(cls, vend_size, izi_size_id) -> model:
        if izi_size_id > 0:
            vend_size.izi_colour_id = izi_size_id
        else:
            vend_size.izi_colour = None
        vend_size.save()
        return vend_size


class ParentCategoryService:
    model = ParentCategory

    @classmethod
    def create(cls, code: str, name: str, is_active: bool, position,
               languages: list, department_id: int) -> model:
        depart = cls.model.objects.create(
            code=code.upper(),
            name=name,
            position=position,
            is_active=is_active,
            department_id=department_id,
        )
        for i in languages:
            TranslationParentCategory.objects.create(parent_category=depart,
                                                     language_id=int(i['lang_id']),
                                                     name=i['translation'], is_active=i.get('is_active', True))
        return depart

    @classmethod
    def update(cls, code: str, name: str, is_active: bool, position,
               languages: list, department_id: int, parent) -> model:
        parent.code = code.upper()
        parent.name = name
        parent.department_id = department_id
        parent.position = position
        parent.is_active = is_active
        parent.save()
        for i in languages:
            tr = None
            try:
                tr = TranslationParentCategory.objects.get(parent_category=parent, language_id=int(i['lang_id']))
            except:
                pass
            if tr is None:
                tr = TranslationParentCategory.objects.create(parent_category=parent, language_id=int(i['lang_id']),
                                                              name=i['translation'], is_active=i['is_active'])
            else:
                tr.name = i['translation']
            tr.save()
        return parent


class SizeService:
    model = Size

    @classmethod
    def create(cls, code: str, name: str, is_active: bool,
               languages: list, size_id: int) -> model:
        size = cls.model.objects.create(
            code=code.upper(),
            name=name,
            is_active=is_active,
        )
        for i in languages:
            TranslationSize.objects.create(size=size,
                                           language_id=int(i['lang_id']),
                                           name=i['translation'],
                                           is_active=i.get('is_active', True))
        return size

    @classmethod
    def update(cls, code: str, name: str, is_active: bool,
               languages: list, size_id: int) -> model:
        size = Size.objects.get(id=size_id)
        size.code = code.upper()
        size.name = name
        size.is_active = is_active
        size.save()
        for i in languages:
            tr = None
            try:
                tr = TranslationSize.objects.get(size=size, language_id=int(i['lang_id']))
            except:
                pass
            if tr is None:
                tr = TranslationSize.objects.create(size=size,
                                                    language_id=
                                                    int(i['lang_id']),
                                                    name=i['translation'],
                                                    is_active=i['is_active'])
            else:
                tr.name = i['translation']
            tr.save()
        return size


class ContentService:
    model = Content

    @classmethod
    def create(cls, code: str, name: str, is_active: bool,
               languages: list, ) -> model:
        content = cls.model.objects.create(
            code=code.upper(),
            name=name,
            is_active=is_active,
        )
        for i in languages:
            TranslationContent.objects.create(content=content,
                                              language_id=int(i['lang_id']),
                                              name=i['translation'],
                                              is_active=i.get('is_active', True))
        return content

    @classmethod
    def update(cls, code: str, name: str, is_active: bool,
               languages: list, content) -> model:
        content.code = code.upper()
        content.name = name
        content.is_active = is_active
        content.save()
        for i in languages:
            tr = None
            try:
                tr = TranslationContent.objects.filter(content=content,
                                                       language_id=int(i['lang_id']))[0]
            except:
                pass
            if tr is None:
                tr = TranslationContent.objects.create(content=content,
                                                       language_id=
                                                       int(i['lang_id']),
                                                       name=i['translation'],
                                                       is_active=i['is_active'])
            else:
                tr.name = i['translation']
            tr.save()
        return content
