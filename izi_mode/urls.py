"""izi_mode URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework.documentation import include_docs_urls

v1 = ([
          path('', include('user_app.urls')),
          url(r'^marketolog/', include('user_app.marketing.urls')),
          url(r'^operator/', include('user_app.operator.urls')),
          url(r'^manager/', include('user_app.manager.urls')),
          url(r'^profile/', include('user_app.profile.urls')),
          url(r'^parsing/', include('projects_app.parsing.urls')),
          url(r'^landing/', include('user_app.landing.urls')),
          url(r'^admin/', include('user_app.admin.urls')),
          url(r'^main/', include('user_app.main.urls')),
      ], 'v1')
v2 = ([
          url(r'^main/', include('user_app.main.urls')),
      ], 'v2')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(v1)),
    path('api/v2/', include(v2)),
    url(r'^docs/', include_docs_urls(title='API Docs', public=True)),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
