from django.db.models import QuerySet
from rest_framework.exceptions import NotFound, ValidationError

from user_app.models import User


class UserService:
    model = User

    @classmethod
    def get(cls, **filters):
        try:
            return User.objects.get(**filters)
        except User.DoesNotExist:
            raise NotFound('User not found')

    @classmethod
    def get_users(cls, **filters) -> QuerySet:
        return User.objects.filter(**filters)

    @classmethod
    def get_all_users(cls) -> QuerySet:
        return User.objects.all()

    @classmethod
    def create(cls, **user_data):
        try:
            return cls.model.objects.create_user(**user_data)
        except Exception as e:
            raise ValidationError('Error while creating distributions: {e}'.format(e=str(e)))
