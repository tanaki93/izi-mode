from django.conf.urls import url

from user_app.marketing import views

urlpatterns = [
    url(r'^products/$', views.marketolog_products_views),
    url(r'^products/([0-9]+)/$', views.marketolog_products_item_view),

]
