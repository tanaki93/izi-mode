from django.db.models import Q
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from product_app.models import OriginalProduct
from user_app.admin.serializers import IziShopProductSerializer
from user_app.permissions import IsMarketing


@api_view(['GET'])
@permission_classes([IsMarketing])
def marketolog_products_views(request):
    if request.method == 'GET':
        query = request.GET.get('query', '')
        page = int(request.GET.get('page', 1))

        data = {}
        products = OriginalProduct.objects.filter(document__step=100,
                                                  selling_price__gt=0)
        if query != "":
            if query[0] == '-':
                products = products.exclude(title_lower__contains=query[1:])
            else:
                id = None
                try:
                    id = int(query)
                except:
                    pass
                if id is not None:
                    products = products.filter(Q(title_lower__contains=str(query)) | Q(
                        description__contains=str(query)) |
                                               Q(product_code__contains=str(query)) | Q(
                        product_id__contains=str(query)) | Q(pk=id))
                else:
                    products = products.filter(Q(title_lower__contains=query) | Q(
                        description__contains=query) |
                                               Q(product_code__contains=query) | Q(
                        product_id__contains=query))
        department_id = None
        try:
            department_id = int(request.GET.get('department_id', ''))
        except:
            pass
        if department_id is not None and department_id != 0:
            products = products.filter(izi_department_id=department_id)
        elif department_id is not None and department_id == 0:
            products = products.filter(izi_department__isnull=True)
        brand_id = None
        try:
            brand_id = int(request.GET.get('brand_id', ''))
        except:
            pass
        if brand_id is not None and brand_id != 0:
            products = products.filter(brand_id=brand_id)
        elif brand_id is not None and brand_id == 0:
            products = products.filter(brand__isnull=True)
        category_id = None
        try:
            category_id = int(request.GET.get('category_id', ''))
        except:
            pass
        if category_id is not None and category_id != 0:
            products = products.filter(izi_category_id=category_id)
        elif category_id is not None and category_id == 0:
            products = products.filter(izi_category__isnull=True)
        colour_id = None
        try:
            colour_id = int(request.GET.get('colour_id', ''))
        except:
            pass
        if colour_id is not None and colour_id != 0:
            products = products.filter(izi_colour_id=colour_id)
        elif colour_id is not None and colour_id == 0:
            products = products.filter(izi_colour__isnull=True)

        content_id = None
        try:
            content_id = int(request.GET.get('content_id', ''))
        except:
            pass
        if content_id is not None and content_id != 0:
            products = products.filter(izi_content_id=content_id)
        elif content_id is not None and content_id == 0:
            products = products.filter(izi_content__isnull=True)
        is_marketolog = None
        try:
            is_marketolog = int(request.GET.get('is_marketolog', ''))
        except:
            pass
        if is_marketolog is not None and is_marketolog == 1:
            products = products.filter(is_marketolog=True)
        elif is_marketolog is not None and is_marketolog == 0:
            products = products.filter(is_marketolog=False)
        length = products.count()
        pages = length // 50
        if pages == 0:
            pages = 1
        elif length % 50 != 0:
            pages += 1
        data['count'] = length
        data['pages'] = pages
        data['products'] = IziShopProductSerializer(products[(page - 1) * 50:page * 50], many=True).data
        return Response(status=status.HTTP_200_OK, data=data)


@api_view(['PUT'])
@permission_classes([IsMarketing])
def marketolog_products_item_view(request, product_id):
    if request.method == 'PUT':
        izi = OriginalProduct.objects.get(id=product_id)
        izi.is_marketolog = request.data.get('is_marketolog', False)
        izi.save()
        return Response(status=status.HTTP_200_OK, data=IziShopProductSerializer(izi).data)
