from django.db import transaction
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from izi_mode.pagination import CustomPagination
from product_app.models import Brand, Department, Category, ParentCategory, VendDepartment, TranslationDepartment, \
    VendCategory, IziColour, VendColour, VendSize, Size, Content, OriginalProduct, Document
from product_app.services import BrandService, DepartmentService, CategoryService, ColourService, VendColourService, \
    VendSizeService, ParentCategoryService, SizeService, ContentService, ProductService, DocumentService
from user_app.admin.serializers import BrandCreateSerializer, VendDepartmentDetailedSerializer, \
    DepartmentCreateSerializer, CategoryCreateSerializer, IziColourCreateSerializer, ParentCategoryCreateSerializer, \
    IziSizeCreateSerializer, IziContentCreateSerializer, ProductSerializer, IziShopProductSerializer, \
    DocumentsSerializer
from user_app.operator.serializers import VendorBrandSerializer, BrandDetailedSerializer, IziShopDepartmentSerializer, \
    IziShopCategorySerializer, ParentCategorySerializer, DepartmentListSerializer, IziColorSerializer, \
    VendColourSerializer, VendSizeSerializer, SizeSerializer, CategorySerializer, ContentSerializer, \
    NewVendBrandSerializer, DocumentSerializer
from user_app.permissions import IsAdminOperator, IsOperator


class VendorBrandListAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = VendorBrandSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(Brand.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = BrandCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        brand = BrandService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK, data=self.serializer_class(brand).data)


class TrendYolBrandListAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = NewVendBrandSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(Brand.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = BrandCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        brand = BrandService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK, data=self.serializer_class(brand).data)


class VendorBrandUpdateAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = VendorBrandSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=BrandDetailedSerializer(
            BrandService.get(id=kwargs['brand_id'])).data,
                        status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer = BrandCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['brand_id'] = kwargs['brand_id']
        brand = BrandService.update(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK, data=self.serializer_class(brand).data)


class IziDepartmentListAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = IziShopDepartmentSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(Department.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)


class IziCategoryListAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = IziShopCategorySerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(Category.objects.filter(parent__isnull=False),
                                                   many=True).data,
                        status=status.HTTP_200_OK)


class IziDepartmentDetailedListAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ParentCategorySerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            ParentCategory.objects.filter(
                department_id=kwargs['department_id']), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = ParentCategoryCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['department_id'] = kwargs['department_id']
        ParentCategoryService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


class VendorDepartmentRetrieveAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = VendDepartmentDetailedSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            VendDepartment.objects.get(
                id=kwargs['department_id']), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        department = VendDepartment.objects.get(
            id=kwargs['department_id'])
        serializer = DepartmentCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['department'] = department
        DepartmentService.assign(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        department = VendDepartment.objects.get(
            id=kwargs['department_id'])
        DepartmentService.set_active(is_active=request.data.get('is_active', True),
                                     department=department)
        return Response(status=status.HTTP_200_OK)


class IziShopDepartmentAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = DepartmentListSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            Department.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = DepartmentCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        DepartmentService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        department = Department.objects.get(id=kwargs['department_id'])
        department.delete()
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer = DepartmentCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['department_id'] = kwargs['department_id']
        DepartmentService.update(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


class VendorCategoryRetrieveAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)

    def post(self, request, *args, **kwargs):
        category = VendCategory.objects.get(
            id=kwargs['category_id'])
        serializer = CategoryCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['category'] = category
        CategoryService.assign(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        category = VendCategory.objects.get(
            id=kwargs['category_id'])
        CategoryService.set_active(is_active=request.data.get('is_active', True),
                                   category=category)
        return Response(status=status.HTTP_200_OK)


class IziCategoryRetrieveAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = CategorySerializer

    def get(self, request, *args, **kwargs):
        categories = Category.objects.filter(parent_id=kwargs['parent_id'])
        return Response(data=self.serializer_class(categories, many=True
                                                   ).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = CategoryCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        CategoryService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


class IziShopColoursAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = IziColorSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            IziColour.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = IziColourCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        ColourService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        colour = IziColour.objects.get(id=kwargs['colour_id'])
        colour.delete()
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer = IziColourCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['colour'] = IziColour.objects.get(
            id=kwargs['colour_id'])
        ColourService.update(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


class VendColoursAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = VendColourSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            VendColour.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        vend_colour = VendColour.objects.get(id=kwargs['colour_id'])
        izi_colour_id = int(request.data.get('colour_id', 0))
        return Response(status=status.HTTP_200_OK,
                        data=self.serializer_class(
                            VendColourService.assign(
                                vend_colour,
                                izi_colour_id)).data)


class VendSizesAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = VendSizeSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            VendSize.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        vend_size = VendSize.objects.get(id=kwargs['size_id'])
        izi_size_id = int(request.data.get('size_id', 0))
        return Response(status=status.HTTP_200_OK,
                        data=self.serializer_class(
                            VendSizeService.assign(
                                vend_size,
                                izi_size_id)).data)


class IziShopSizesAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = SizeSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            Size.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        size = Size.objects.get(id=kwargs['size_id'])
        size.delete()
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer = IziSizeCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['size_id'] = kwargs['size_id']
        SizeService.update(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = IziSizeCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        SizeService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


class IziShopParentCategoryAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)

    def delete(self, request, *args, **kwargs):
        parent = ParentCategory.objects.get(id=kwargs['parent_id'])
        parent.delete()
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer = ParentCategoryCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['parent'] = ParentCategory.objects.get(id=kwargs['parent_id'])
        ParentCategoryService.update(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


class IziShopContentsAPIView(APIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ContentSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            Content.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        content = Content.objects.get(id=kwargs['content_id'])
        content.delete()
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        serializer = IziContentCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['content'] = Content.objects.get(
            id=kwargs['content_id'])
        ContentService.update(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = IziContentCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        ContentService.create(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


PAGE_SIZE = 50


class AdminVendorProductsAPIView(GenericAPIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        page = int(request.query_params.get('page', 1))
        products = ProductService.get_admin_products(request.query_params, None)
        length = products.count()
        pages = length // PAGE_SIZE
        if pages == 0:
            pages = 1
        elif length % PAGE_SIZE != 0:
            pages += 1
        data = {'count': length, 'pages': pages,
                'products': ProductSerializer(products[(page - 1) * PAGE_SIZE:page * PAGE_SIZE], many=True).data}
        return Response(status=status.HTTP_200_OK, data=data)


class IziShopProductsAPIView(GenericAPIView):
    permission_classes = (IsAdminOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = IziShopProductSerializer

    def get(self, request, *args, **kwargs):
        page = int(request.query_params.get('page', 1))
        products = ProductService.get_admin_izi_products(request.query_params)
        length = products.count()
        pages = length // PAGE_SIZE
        if pages == 0:
            pages = 1
        elif length % PAGE_SIZE != 0:
            pages += 1
        data = {'count': length, 'pages': pages,
                'products': self.serializer_class(products[(page - 1) * PAGE_SIZE:page * PAGE_SIZE], many=True).data}
        return Response(status=status.HTTP_200_OK, data=data)

    def put(self, request, *args, **kwargs):
        with transaction.atomic():
            option = request.data.get('option', '')
            id = None
            try:
                id = int(request.data.get('id', ''))
            except:
                pass
            izi = OriginalProduct.objects.get(id=kwargs['product_id'])
            if id is not None:
                if option == 'department':
                    izi.department_id = id
                elif option == 'category':
                    izi.category_id = id
                elif option == 'colour':
                    izi.colour_id = id
                elif option == 'content':
                    izi.content_id = id
            izi.save()
            return Response(status=status.HTTP_200_OK,
                            data=self.serializer_class(izi).data)

    def post(self, request, *args, **kwargs):
        izi = OriginalProduct.objects.get(id=kwargs['product_id'])
        izi.is_sellable = request.data.get('is_sellable', True)
        izi.save()
        return Response(status=status.HTTP_200_OK,
                        data=self.serializer_class(izi).data)

    def delete(self, request, *args, **kwargs):
        izi = OriginalProduct.objects.get(id=kwargs['product_id'])
        izi.is_hit = request.data.get('is_hit', False)
        izi.save()
        return Response(status=status.HTTP_200_OK,
                        data=self.serializer_class(izi).data)


class OperatorDocumentsAPIView(APIView):
    """
    List API to show documents with filters
    """
    permission_classes = (IsOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = DocumentSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            DocumentService.get_operator_documents(user=request.user),
            many=True).data, status=status.HTTP_200_OK)


class OperatorDocumentsAllAPIView(APIView):
    """
    List API to show documents with filters
    """
    permission_classes = (IsOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = DocumentSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            DocumentService.get_all_documents(),
            many=True).data, status=status.HTTP_200_OK)


class OperatorDocumentAPIView(APIView):
    """
    List API to show documents with filters
    """
    permission_classes = (IsOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = DocumentSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(
            DocumentService.get(id=kwargs['document_id']),
            many=False).data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        DocumentService.set_user(pk=kwargs['document_id'], user=request.user)
        return Response(status=status.HTTP_200_OK)


class OperatorDocumentGetProcessAPIView(APIView):
    """
    GET, PUT, POST API -  processes with document
    """
    permission_classes = (IsOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = DocumentsSerializer

    def get(self, request, *args, **kwargs):
        data = DocumentService.get_document_data(pk=kwargs['document_id'])
        return Response(data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = Document.objects.get(pk=kwargs['document_id'])
        data.set_to_next_step()
        return Response(self.serializer_class(data).data, status=status.HTTP_200_OK)


class OperatorDocumentProductsAPIView(GenericAPIView):
    """
    GET, PUT, POST API -  processes with document
    """
    permission_classes = (IsOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ProductSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        document = DocumentService.get(id=kwargs['document_id'])
        queryset = self.filter_queryset(ProductService.get_admin_products(request.query_params, document))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            result = self.get_paginated_response(serializer.data)
            data = result.data  # pagination data
        else:
            serializer = self.get_serializer(queryset, many=True)
            data = serializer.data
        cont = {
            'step': document.step,
            'count': data['count'],
            'pages': data['pages'],
            'products': data['results'], }
        return Response(data=cont, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        with transaction.atomic():
            option = request.data.get('option', '')
            id = None
            try:
                id = int(request.data.get('id', ''))
            except:
                pass
            for i in request.data.get('products'):
                try:
                    izi = OriginalProduct.objects.get(id=int(i))
                    if option == 'department':
                        department = Department.objects.get(id=int(id))
                        izi.izi_department = department
                        category = izi.izi_category
                        if category is not None:
                            try:
                                if department != category.parent.department:
                                    izi.izi_category = None
                            except:
                                pass
                    elif option == 'category':
                        izi.izi_category_id = id
                    elif option == 'colour':
                        izi.izi_colour_id = id
                    elif option == 'content':
                        izi.izi_content_id = id
                    izi.save()
                except:
                    pass
        return Response(status=status.HTTP_200_OK)


class OperatorDocumentProductProcessAPIView(GenericAPIView):
    """
    GET, PUT, POST API -  processes with document
    """
    permission_classes = (IsOperator,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ProductSerializer
    pagination_class = CustomPagination

    def put(self, request, *args, **kwargs):
        with transaction.atomic():
            option = request.data.get('option', '')
            id = None
            try:
                id = int(request.data.get('id', ''))
            except:
                pass
            izi = OriginalProduct.objects.get(id=kwargs['product_id'],
                                              document_id=kwargs['document_id'])
            if option == 'department':
                department = Department.objects.get(id=int(id))
                izi.izi_department = department
                category = izi.izi_category
                if category is not None:
                    try:
                        if department != category.parent.department:
                            izi.izi_category = None
                    except:
                        pass
            elif option == 'category':
                izi.izi_category_id = id
            elif option == 'colour':
                izi.izi_colour_id = id
            elif option == 'content':
                izi.izi_content_id = id
            izi.save()
            return Response(status=status.HTTP_200_OK, data=ProductSerializer(izi).data)
