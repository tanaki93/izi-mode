from rest_framework import serializers

from product_app.models import Brand, Country, Currency, BrandCountry, VendDepartment, VendCategory, Department, \
    ParentCategory, Category, Language, TranslationCategory, IziColour, VendColour, VendSize, Size, Content, Document
from user_app.serializers import UserSerializer


class ParentDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'


class ParentSerializer(serializers.ModelSerializer):
    department = ParentDepartmentSerializer()

    class Meta:
        model = ParentCategory
        fields = 'name id department is_active'.split()


class VendCategorySerializer(serializers.ModelSerializer):
    brand = serializers.SerializerMethodField()
    department = serializers.SerializerMethodField()

    class Meta:
        model = VendCategory
        fields = ('id', 'name', 'department', 'brand')

    def get_brand(self, obj):
        return obj.department.brand.name

    def get_department(self, obj):
        return obj.department.name


class CategorySerializer(serializers.ModelSerializer):
    parent = ParentSerializer()
    categories = VendCategorySerializer(many=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'parent', 'languages',
                  'categories', 'code', 'position', 'is_active', 'is_related',
                  'average_weight', 'size_table')


class VendCategoryDetailedSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = VendCategory
        fields = ('id', 'name', 'link', 'is_active', 'category')


class VendDepartmentSerializer(serializers.ModelSerializer):
    brand = serializers.SerializerMethodField()

    class Meta:
        model = VendDepartment
        fields = ('id', 'name', 'brand')

    def get_brand(self, obj):
        return obj.brand.name


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ('id', 'name')


class VendDepartmentDetailedSerializer(serializers.ModelSerializer):
    department = DepartmentSerializer()
    categories = serializers.SerializerMethodField()

    class Meta:
        model = VendDepartment
        fields = ('id', 'name', 'department', 'is_active', 'categories')

    def get_categories(self, obj):
        categories = VendCategory.objects.filter(department=obj)
        return VendCategoryDetailedSerializer(categories, many=True).data


class BrandDetailedSerializer(serializers.ModelSerializer):
    departments = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        fields = ('id', 'departments')

    def get_departments(self, obj):
        departments = VendDepartment.objects.filter(brand=obj)
        return VendDepartmentDetailedSerializer(departments, many=True).data


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = 'id code name code_name is_active is_related'.split()


class NewVendCategoryDetailedSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendCategory
        fields = ('id', 'name',)


class NewVendDepartmentDetailedSerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField()

    class Meta:
        model = VendDepartment
        fields = ('id', 'name', 'categories')

    def get_categories(self, obj):
        categories = VendCategory.objects.filter(department=obj)
        return NewVendCategoryDetailedSerializer(categories, many=True).data


class NewVendBrandSerializer(serializers.ModelSerializer):
    departments = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        fields = ('id', 'name', 'departments')

    def get_departments(self, obj):
        departments = VendDepartment.objects.filter(brand=obj)
        return NewVendDepartmentDetailedSerializer(departments, many=True).data


class VendorBrandSerializer(serializers.ModelSerializer):
    currency = CurrencySerializer()

    class Meta:
        model = Brand
        fields = (
            'id', 'name', 'is_active',
            'link', 'code', 'created_at',
            'updated_at', 'countries',
            'currency', 'delivery_price', 'delivery_percent',
            'delivery_day')


class IziShopDepartmentSerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField()

    class Meta:
        model = Department
        fields = ('id', 'name',
                  'languages', 'position', 'code', 'is_active',
                  'is_related', 'categories')

    def get_categories(self, obj):
        return CategorySerializer([], many=True).data


class IziShopCategorySerializer(serializers.ModelSerializer):
    parent = ParentSerializer()

    class Meta:
        model = Category
        fields = ('id', 'name', 'parent')


class IziDepSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = 'id name slug'.split()


class ChildCategorySerializer(serializers.ModelSerializer):
    name_ru = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('id', 'name', 'name_ru', 'slug', 'languages', 'average_weight', 'size_table')

    def get_name_ru(self, obj):
        try:
            language = Language.objects.all().first()
            translation = TranslationCategory.objects.get(category=obj, language=language)
            return translation.name.capitalize()
        except:
            pass
        return ''


class ParentCategorySerializer(serializers.ModelSerializer):
    childs = serializers.SerializerMethodField()
    department = IziDepSerializer()

    class Meta:
        model = ParentCategory
        fields = (
            'id', 'name', 'childs',
            'code', 'position',
            'is_active', 'languages', 'department',
            'is_related', 'slug')

    def get_childs(self, obj):
        n = Category.objects.filter(parent=obj, is_active=True, has_products=True).order_by('position')
        return ChildCategorySerializer(n, many=True).data


class DepartmentListSerializer(serializers.ModelSerializer):
    departments = VendDepartmentSerializer(many=True)
    categories = serializers.SerializerMethodField()

    class Meta:
        model = Department
        fields = ('id', 'name', 'languages', 'departments', 'position', 'code', 'is_active', 'is_related', 'categories')

    def get_categories(self, obj):
        categories = Category.objects.filter(parent__department=obj)
        return CategorySerializer(categories, many=True).data


class IziColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = IziColour
        fields = ('id', 'name', 'languages', 'code', 'is_active', 'is_related', 'hex', 'image')


class VendColourSerializer(serializers.ModelSerializer):
    izi_colour = IziColorSerializer()

    class Meta:
        model = VendColour
        fields = ('id', 'name', 'izi_colour')


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ('id', 'name', 'languages', 'code', 'is_related')


class VendSizeSerializer(serializers.ModelSerializer):
    izi_size = SizeSerializer()

    class Meta:
        model = VendSize
        fields = ('id', 'name', 'izi_size')


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = ('id', 'name', 'languages', 'code', 'is_related')


class DocumentSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    department = serializers.SerializerMethodField()
    brand = serializers.SerializerMethodField()

    class Meta:
        model = Document
        fields = ('id', 'updated_at', 'user', 'step', 'status', 'department', 'brand', 'products', 'no_stock_or_price')

    def get_department(self, obj):
        try:
            return obj.department.name
        except:
            return 'Нет отделения'


    def get_brand(self, obj):
        try:
            return obj.brand.name
        except:
            return 'Нет бренда'

