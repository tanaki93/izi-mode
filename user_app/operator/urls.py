from django.urls import path
from user_app.operator import views

urlpatterns = [
    path('trendyol/brands/<int:brand_id>/',
         views.VendorBrandUpdateAPIView.as_view()),
    path('trendyol/brands/',
         views.TrendYolBrandListAPIView.as_view()),
    path('trendyol/departments/<int:department_id>/',
         views.VendorDepartmentRetrieveAPIView.as_view()),
    path('trendyol/categories/<int:category_id>/',
         views.VendorCategoryRetrieveAPIView.as_view()),
    path('trendyol/colours/',
         views.VendColoursAPIView.as_view()),
    path('trendyol/colours/<int:colour_id>/',
         views.VendColoursAPIView.as_view()),
    path('trendyol/sizes/',
         views.VendSizesAPIView.as_view()),
    path('trendyol/sizes/<int:size_id>/',
         views.VendSizesAPIView.as_view()),

    path('izishop/departments/<int:department_id>/',
         views.IziShopDepartmentAPIView.as_view()),
    path('izishop/departments/<int:department_id>/parents/',
         views.IziDepartmentDetailedListAPIView.as_view()),
    path('izishop/parents/<int:parent_id>/categories/',
         views.IziCategoryRetrieveAPIView.as_view()),
    path('izishop/parents/<int:parent_id>/',
        views.IziShopParentCategoryAPIView.as_view()),

    path('izishop/departments/',
         views.IziShopDepartmentAPIView.as_view()),
    path('izishop/colours/',
         views.IziShopColoursAPIView.as_view()),
    path('izishop/colours/<int:colour_id>/',
         views.IziShopColoursAPIView.as_view()),
    path('izishop/sizes/',
         views.IziShopSizesAPIView.as_view()),
    path('izishop/sizes/<int:size_id>/',
         views.IziShopSizesAPIView.as_view()),
    path('izishop/content/',
         views.IziShopContentsAPIView.as_view()),
    path('izishop/content/<int:content_id>/',
         views.IziShopContentsAPIView.as_view()),
    path('izishop/products/',
         views.IziShopProductsAPIView.as_view()),
    path('izishop/products/<int:product_id>/',
         views.IziShopProductsAPIView.as_view()),
    path('izishop/categories/',
         views.IziCategoryListAPIView.as_view()),

    path('izi/departments/',
         views.IziDepartmentListAPIView.as_view()),
    path('izi/categories/',
         views.IziCategoryListAPIView.as_view()),

    path('vendor/brands/',
         views.VendorBrandListAPIView.as_view()),
    path('vendor/products/',
         views.AdminVendorProductsAPIView.as_view()),

    path('documents/', views.OperatorDocumentsAPIView.as_view()),
    path('documents/all/', views.OperatorDocumentsAllAPIView.as_view()),
    path('documents/<int:document_id>/', views.OperatorDocumentAPIView.as_view()),
    path('documents/<int:document_id>/process/', views.OperatorDocumentGetProcessAPIView.as_view()),
    path('documents/<int:document_id>/process/products/', views.OperatorDocumentProductsAPIView.as_view()),
    path('documents/<int:document_id>/process/products/<int:product_id>/',
         views.OperatorDocumentProductProcessAPIView.as_view()),

]
