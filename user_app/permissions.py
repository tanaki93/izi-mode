from rest_framework import permissions

from .models import ADMIN, OPERATOR, CLIENT, MARKETING, Q_CONTROLLER


class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.user_type == ADMIN


class IsOperator(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.user_type == OPERATOR


class IsClient(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.user_type == CLIENT


class IsMarketing(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.user_type == MARKETING


class IsAdminOperator(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        elif request.user.user_type == ADMIN or request.user.user_type == OPERATOR:
            return True
        else:
            return False


class IsAdminManager(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        elif request.user.user_type == ADMIN or request.user.user_type == Q_CONTROLLER:
            return True
        else:
            return False
