from django.conf.urls import url
from django.urls import path

from user_app.landing import views

urlpatterns = [
    path('brands/',
         views.LandingBrandsAPIView.as_view()),
    path('brands/<int:brand_id>/',
         views.LandingBrandsAPIView.as_view()),
]
