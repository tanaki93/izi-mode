from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView

from product_app.models import Brand
from user_app.landing.serializers import BrandListSerializer
from user_app.permissions import IsAdmin


class LandingBrandsAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)

    def get(self, request, *args, **kwargs):
        data = BrandListSerializer(Brand.objects.all(), many=True).data
        return Response(status=status.HTTP_200_OK, data=data)

    def put(self, request, *args, **kwargs):
        brand = Brand.objects.get(id=kwargs['brand_id'])
        is_landing = request.data.get('is_landing', True)
        try:
            brand.m_image = request.data.get('m_image', '').split('media/')[-1]
        except:
            pass
        try:
            brand.db_image = request.data.get('db_image', '').split('media/')[-1]
        except:
            pass
        try:
            brand.ds_image = request.data.get('ds_image', '').split('media/')[-1]
        except:
            pass
        brand.is_landing = is_landing
        brand.save()
        data = BrandListSerializer(brand).data
        return Response(status=status.HTTP_200_OK, data=data)