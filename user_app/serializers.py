from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from product_app.models import Document


class UserSerializer(serializers.ModelSerializer):
    is_related = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = 'id first_name last_name gender birthday fio username user_type phone email is_related'.split()

    def get_is_related(self, obj):
        count = Document.objects.filter(user=obj).count()
        if count > 0:
            return True
        return False


class UserSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = 'id first_name last_name username user_type'.split()


class TokenSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField(read_only=True)
    user = UserSimpleSerializer()

    class Meta:
        model = Token
        fields = 'token user'.split()

    def get_token(self, obj):
        return obj.key
