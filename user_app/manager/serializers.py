from rest_framework import serializers

from product_app.models import VendSize, OriginalProduct, Country
from projects_app.models import Order, OrderItem, OrderPackage, OrderItemComment, CommentImage, OrderPacket, \
    PacketProduct, Flight, RefundOrder, Transaction, RefundOrderItem
from user_app.admin.serializers import IziSizeSerializer, LinkSerializer, TrendYolDepartmentSerializer, \
    VendBrandSerializer, TrendYolCategorySerializer, ColourSerializer, IziColourSerializer, CategoryItemSerializer, \
    ParentDepartmentSerializer
from user_app.operator.serializers import ContentSerializer
from user_app.serializers import UserSerializer


class TransactionSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = 'pan pg_payment_system amount status trans_id auth_code card_brand'.split()

    def get_status(self, obj):
        if obj.status == 0:
            return "Ошибка в транзакиции"
        return "Успешная транзакция"


class OrderListSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    count = serializers.SerializerMethodField()
    statuses = serializers.SerializerMethodField()
    user = UserSerializer()
    logistic_statuses = serializers.SerializerMethodField()
    process_status = serializers.SerializerMethodField()
    transactions = TransactionSerializer(many=True)

    class Meta:
        model = Order
        fields = 'id date transactions name payment_status payment_type products_price shipping_price process_status ' \
                 'total_price email user phone address price logistic_statuses statuses count'.split()

    def get_price(self, obj):
        items = OrderItem.objects.filter(order=obj)
        price = 0
        for i in items:
            price += i.price
        return price

    def get_count(self, obj):
        return OrderItem.objects.filter(order=obj).count()

    def get_statuses(self, obj):
        data = {
            'waiting': OrderItem.objects.filter(order=obj, product_status=1).count(),
            'in_process': OrderItem.objects.filter(order=obj, product_status=2).count(),
            'done': OrderItem.objects.filter(order=obj, product_status=3).count(),
            'cancelled': OrderItem.objects.filter(order=obj, product_status=4).count(),
        }
        return data

    def get_logistic_statuses(self, obj):
        data = {
            'logistic_receive_status': OrderItem.objects.filter(order=obj, logistic_receive_status=1).count(),
            'logistic_deliver_status': OrderItem.objects.filter(order=obj, logistic_deliver_status=3).count(),
        }
        return data

    def get_process_status(self, obj):
        # refund = None
        # try:
        #     refund = RefundOrder.objects.get(order=obj)
        # except:
        #     pass
        # if refund is not None and refund.status == 1:
        #     return 100
        return obj.process_status


class OrderLogisticSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    count = serializers.SerializerMethodField()
    statuses = serializers.SerializerMethodField()
    logistic_statuses = serializers.SerializerMethodField()
    user = UserSerializer()
    transactions = TransactionSerializer(many=True)

    class Meta:
        model = Order
        fields = 'id date name transactions payment_status user process_status email phone address price ' \
                 'logistic_statuses statuses count'.split()

    def get_price(self, obj):
        items = OrderItem.objects.filter(order=obj)
        price = 0
        for i in items:
            price += i.price
        return price

    def get_count(self, obj):
        return OrderItem.objects.filter(order=obj).count()

    def get_statuses(self, obj):
        data = {
            'waiting': OrderItem.objects.filter(order=obj, product_status=1).count(),
            'in_process': OrderItem.objects.filter(order=obj, product_status=2).count(),
            'done': OrderItem.objects.filter(order=obj, product_status=3).count(),
            'cancelled': OrderItem.objects.filter(order=obj, product_status=4).count(),
        }
        return data

    def get_logistic_statuses(self, obj):
        data = {
            'logistic_receive_status': OrderItem.objects.filter(order=obj, logistic_receive_status=2).count(),
            'logistic_deliver_status': OrderItem.objects.filter(order=obj, logistic_deliver_status=3).count(),
        }
        return data


class PackageItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderPackage
        fields = '__all__'


class ImageItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentImage
        fields = '__all__'


class CommentItemSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()

    class Meta:
        model = OrderItemComment
        fields = 'comment id images'.split()

    def get_images(self, obj):
        return ImageItemSerializer(CommentImage.objects.filter(comment=obj), many=True).data


class ProductSerializer(serializers.ModelSerializer):
    link = LinkSerializer()
    images = serializers.SerializerMethodField()
    department = TrendYolDepartmentSerializer()
    izi_department = serializers.SerializerMethodField()
    brand = VendBrandSerializer()
    vend_colour = serializers.SerializerMethodField()
    category = TrendYolCategorySerializer()
    izi_category = serializers.SerializerMethodField()
    colour = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()
    shipping_price = serializers.SerializerMethodField()
    izi_link = serializers.SerializerMethodField()
    delivery_day = serializers.SerializerMethodField()

    class Meta:
        model = OriginalProduct
        # fields = '__all__'
        fields = ['selling_price', 'discount_price', 'is_free_argo', 'images', 'delivery_date', 'product_code', 'id',
                  'colour', 'promotions', 'created_at', 'is_active', 'brand', 'product_id', 'link', 'is_rush_delivery',
                  'title', 'izi_link', 'delivery_day',
                  'original_price', 'updated_at', 'description', 'department', 'category', 'izi_category',
                  'izi_department', 'colour', 'content', 'vend_colour', 'shipping_price']

    def get_delivery_day(self, obj):
        day = 8
        try:
            country = Country.objects.first()
            delivery_day = obj.brand.delivery_day
            country_delivery_day = country.delivery_day
            day = delivery_day + country_delivery_day
        except:
            pass
        return day

    def get_images(self, obj):
        return obj.images.split()

    def get_izi_link(self, product):
        link = 'https://izishop.kg/'
        try:
            link = 'https://izishop.kg/%s/%s/%s/%s' % (
                product.izi_department.slug, product.izi_parent_category.slug,
                product.izi_category.slug,
                product.id)
        except:
            pass
        return link

    def get_shipping_price(self, obj):
        try:
            average_weight = obj.izi_category.average_weight
            delivery_price = obj.brand.delivery_price
            delivery_percent = obj.brand.delivery_percent
            price = average_weight * delivery_price
            percent = (price / 100) * delivery_percent
            return round((price + percent) * 70)
        except:
            pass
        return 100

    def get_vend_colour(self, obj):
        colour = None
        try:
            colour = obj.colour
        except:
            pass
        return ColourSerializer(colour).data

    def get_colour(self, obj):
        return IziColourSerializer(obj.izi_colour).data

    def get_content(self, obj):
        return ContentSerializer(obj.izi_content).data

    def get_izi_category(self, obj):
        return CategoryItemSerializer(obj.izi_category).data

    def get_izi_department(self, obj):
        return ParentDepartmentSerializer(obj.izi_department).data


class OrderProductItemSerializer(serializers.ModelSerializer):
    size = IziSizeSerializer(read_only=True)
    product = serializers.SerializerMethodField()
    package = PackageItemSerializer(read_only=True)
    package_status = serializers.SerializerMethodField()
    product_status = serializers.SerializerMethodField()
    delivery_status = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()
    izi_link = serializers.SerializerMethodField()
    weight = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = 'id size product logistic_deliver_status receive_date send_date delivery_date ' \
                 'logistic_receive_status price amount product_status checking_status delivery_status ' \
                 'package_status updated stage shipping_service package package_status shipping_price comments ' \
                 'izi_link weight'.split()

    def get_product(self, obj):
        return ProductSerializer(obj.product).data

    def get_izi_link(self, obj):
        product = obj.product
        link = 'https://izishop.kg/'
        try:
            link = 'https://izishop.kg/%s/%s/%s/%s' % (
                product.izi_department.slug, product.izi_parent_category.slug,
                product.izi.category.slug,
                product.id)
        except:
            pass
        return link

    def get_weight(self, obj):
        weight = 0
        try:
            product = obj.product
            weight = product.izi_category.average_weight
        except:
            pass
        return weight

    def get_product_status(self, obj):
        refund = RefundOrderItem.objects.filter(order_item=obj)
        if refund.count() > 0:
            return 4
        else:
            return obj.product_status

    def get_package_status(self, obj):
        package = None
        try:
            status = obj.package.status
            package = obj.package
        except:
            pass
        if package is None:
            return 0
        else:
            return package.status

    def get_delivery_status(self, obj):
        packet = None
        try:
            packet = PacketProduct.objects.get(order_item=obj)
        except:
            pass
        if packet is None:
            return obj.delivery_status
        elif packet.order_packet.flight is not None:
            return 5
        elif packet.order_packet.received_status == 1:
            return 4
        else:
            return 3

    def get_comments(self, obj):
        data = OrderItemComment.objects.filter(order_item=obj)
        return CommentItemSerializer(data, many=True).data


class OrderItemSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    products = serializers.SerializerMethodField()
    statuses = serializers.SerializerMethodField()
    logistic_statuses = serializers.SerializerMethodField()
    count = serializers.SerializerMethodField()
    process_status = serializers.SerializerMethodField()
    transactions = TransactionSerializer(many=True)
    user = UserSerializer()

    def get_logistic_statuses(self, obj):
        data = {
            'logistic_receive_status': OrderItem.objects.filter(order=obj, logistic_receive_status=2).count(),
            'logistic_deliver_status': OrderItem.objects.filter(order=obj, logistic_deliver_status=3).count(),
        }
        return data

    class Meta:
        model = Order
        fields = 'id date name payment_status user payment_type process_status shipping_price email phone address ' \
                 'logistic_statuses price products ' \
                 'statuses count transactions'.split()

    def get_count(self, obj):
        return OrderItem.objects.filter(order=obj).count()

    def get_statuses(self, obj):
        data = {
            'waiting': OrderItem.objects.filter(order=obj, product_status=1).count(),
            'in_process': OrderItem.objects.filter(order=obj, product_status=2).count(),
            'done': OrderItem.objects.filter(order=obj, product_status=3).count(),
            'cancelled': OrderItem.objects.filter(order=obj, product_status=4).count(),
        }
        return data

    def get_price(self, obj):
        items = OrderItem.objects.filter(order=obj)
        price = 0
        for i in items:
            price += i.price
        return price

    def get_products(self, obj):
        data = OrderProductItemSerializer(OrderItem.objects.filter(order=obj), many=True).data
        return data

    def get_process_status(self, obj):
        refund = None
        try:
            refund = RefundOrder.objects.get(order=obj)
        except:
            pass
        if refund is not None and refund.status == 1:
            return 100
        return obj.process_status


class FlightListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flight
        fields = 'id number'.split()


class PackageListSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()

    class Meta:
        model = OrderPackage
        fields = 'id number updated created status products'.split()

    def get_products(self, obj):
        return OrderProductItemSerializer(OrderItem.objects.filter(package=obj), many=True).data


class PacketListSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()
    total_weight = serializers.SerializerMethodField()
    total_price = serializers.SerializerMethodField()
    flight = FlightListSerializer(read_only=True)

    class Meta:
        model = OrderPacket
        fields = 'id updated weight created money received_status flight status products total_weight total_price'.split()

    def get_products(self, obj):
        data = [i.order_item for i in PacketProduct.objects.filter(order_packet=obj)]
        return OrderProductItemSerializer(data, many=True).data

    def get_total_weight(self, obj):
        weight = 0
        data = [i.order_item for i in PacketProduct.objects.filter(order_packet=obj)]
        for packet in data:
            try:
                product = packet.product
                weight += product.link.product.category.average_weight
            except:
                pass
        return weight

    def get_total_price(self, obj):
        price = 0
        data = [i.order_item for i in PacketProduct.objects.filter(order_packet=obj)]
        for packet in data:
            try:
                price += packet.shipping_price
            except:
                pass
        return price


class OrderProductLogisticItemSerializer(serializers.ModelSerializer):
    size = IziSizeSerializer(read_only=True)
    product = ProductSerializer(read_only=True)
    package = PackageItemSerializer(read_only=True)
    package_status = serializers.SerializerMethodField()
    delivery_status = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()
    flight = serializers.SerializerMethodField()
    izi_link = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = 'id size product logistic_deliver_status receive_date send_date delivery_date logistic_receive_status price amount product_status checking_status delivery_status ' \
                 ' package_status updated stage shipping_service package package_status comments flight izi_link'.split()

    def get_izi_link(self, obj):
        product = obj.product
        link = 'https://izishop.kg/'
        try:
            link = 'https://izishop.kg/%s/%s/%s/%s' % (
                product.izi_department.slug, product.izi_parent_category.slug,
                product.izi.category.slug,
                product.id)
        except:
            pass
        return link

    def get_flight(self, obj):
        try:
            flight = Flight.objects.get(orderpacket__packetproduct__order_item=obj)
            return FlightListSerializer(flight).data
        except:
            pass
        return None

    def get_package_status(self, obj):
        package = None
        try:
            status = obj.package.status
            package = obj.package
        except:
            pass
        if package is None:
            return 0
        else:
            return package.status

    def get_delivery_status(self, obj):
        packet = None
        try:
            packet = PacketProduct.objects.get(order_item=obj)
        except:
            pass
        if packet is None:
            return obj.delivery_status
        elif packet.order_packet.flight is not None:
            return 5
        elif packet.order_packet.received_status == 1:
            return 4
        else:
            return 3

    def get_comments(self, obj):
        data = OrderItemComment.objects.filter(order_item=obj)
        return CommentItemSerializer(data, many=True).data


class OrderItemLogisticSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    products = serializers.SerializerMethodField()
    statuses = serializers.SerializerMethodField()
    logistic_statuses = serializers.SerializerMethodField()
    count = serializers.SerializerMethodField()
    user = UserSerializer()

    def get_logistic_statuses(self, obj):
        data = {
            'logistic_deliver_status': OrderItem.objects.filter(order=obj, logistic_deliver_status=3).count(),
            'logistic_receive_status': OrderItem.objects.filter(order=obj, logistic_receive_status=2).count(),
        }
        return data

    class Meta:
        model = Order
        fields = 'id date name payment_status process_status user email phone address logistic_statuses price products ' \
                 'statuses count'.split()

    def get_count(self, obj):
        return OrderItem.objects.filter(order=obj).count()

    def get_statuses(self, obj):
        data = {
            'waiting': OrderItem.objects.filter(order=obj, product_status=1).count(),
            'in_process': OrderItem.objects.filter(order=obj, product_status=2).count(),
            'done': OrderItem.objects.filter(order=obj, product_status=3).count(),
            'cancelled': OrderItem.objects.filter(order=obj, product_status=4).count(),
        }
        return data

    def get_price(self, obj):
        items = OrderItem.objects.filter(order=obj)
        price = 0
        for i in items:
            price += i.price
        return price

    def get_products(self, obj):
        data = OrderProductLogisticItemSerializer(OrderItem.objects.filter(order=obj), many=True).data
        return data
