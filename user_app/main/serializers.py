from rest_framework import serializers

from product_app.models import Brand, Department, ParentCategory, Language, TranslationDepartment, Category, \
    DeliveryPoint, City, ExchangeRate, Tag, VendCategory, TranslationCategory, ImageSlider, Slider, Variant, \
    TranslationColour, IziColour, TranslationSize, OriginalProduct, Country, DocumentProduct, BrandCountry, \
    TranslationContent
from projects_app.models import Transaction, Order, OrderItem, PacketProduct, RefundOrderItem, Favourite
from user_app.admin.serializers import IziSizeSerializer, ProductSerializer, VendBrandSerializer
from user_app.operator.serializers import ChildCategorySerializer, IziDepSerializer


class DeliveryPointSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryPoint
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    delivery_price = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()

    class Meta:
        model = City
        fields = 'id name is_active delivery_percent delivery_day delivery_price points'.split()

    def get_delivery_price(self, obj):
        country_delivery_price = obj.delivery_price
        country_delivery_percent = obj.delivery_percent
        country_price = country_delivery_price
        country_percent = (country_price / 100) * country_delivery_percent
        from_country_price = country_price + country_percent
        exchange = ExchangeRate.objects.get(from_currency__code='USD', to_currency__code='KGS')
        x = round(from_country_price * exchange.value)
        if x % 100 < 51:
            x = (x // 100) * 100 + 50
        else:
            x = (x // 100) * 100 + 100
        return x

    def get_points(self, obj):
        points = DeliveryPoint.objects.filter(city=obj, is_active=True)
        return DeliveryPointSerializer(points, many=True).data


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class ParentCategorySerializer(serializers.ModelSerializer):
    childs = serializers.SerializerMethodField()
    department = IziDepSerializer()

    class Meta:
        model = ParentCategory
        fields = (
            'id', 'name', 'childs', 'code', 'position', 'is_active', 'languages', 'department',
            'is_related', 'slug')

    def get_childs(self, obj):
        n = Category.objects.filter(parent=obj, is_active=True, has_products=True).order_by('position')
        return ChildCategorySerializer(n, many=True).data


class DepartmentSerializer(serializers.ModelSerializer):
    parent_categories = serializers.SerializerMethodField()
    name_ru = serializers.SerializerMethodField()

    class Meta:
        model = Department
        fields = ('id', 'name', 'name_ru', 'parent_categories', 'slug')

    def get_parent_categories(self, obj):
        parents = ParentCategory.objects.filter(department=obj, is_active=True)
        return ParentCategorySerializer(parents, many=True).data

    def get_name_ru(self, obj):
        try:
            language = Language.objects.all().first()
            translation = TranslationDepartment.objects.get(department=obj, language=language)
            return translation.name.capitalize()
        except:
            pass
        return ''


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageSlider
        fields = '__all__'


class SliderSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Slider
        fields = ('id', 'title', 'images')


class TransactionSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = 'pan pg_payment_system amount status trans_id auth_code card_brand'.split()

    def get_status(self, obj):
        if obj.status == 0:
            return "Ошибка в транзакиции"
        return "Успешная транзакция"


class OrderProductLogisticItemSerializer(serializers.ModelSerializer):
    size = IziSizeSerializer(read_only=True)
    product = ProductSerializer(read_only=True)
    delivery_status = serializers.SerializerMethodField()
    stock = serializers.SerializerMethodField()

    class Meta:
        model = OrderItem
        fields = 'id size product price shipping_price amount delivery_status stock'.split()

    def get_stock(self, obj):
        if not obj.product.stock:
            return False
        size_id = obj.size_id
        variants = Variant.objects.filter(tr_size__izi_size_id=size_id, original_product=obj.product,
                                          stock=True).count()
        if variants > 0:
            return True
        else:
            return False

    def get_delivery_status(self, obj):
        status = 'В обработке'
        if obj.product_status == 1:
            status = 'Обработан'
        elif obj.product_status == 2:
            status = 'Заказан'
        package = obj.package
        if package is not None:
            if package.status == 2:
                status = 'На складе'
        packet = None
        try:
            packet = PacketProduct.objects.get(order_item=obj)
        except:
            pass
        if packet is not None and packet.order_packet.flight is not None and packet.order_packet.received_status == 1:
            status = "В пути"
        if obj.logistic_receive_status == 2:
            status = "Товар прибыл"
        if obj.logistic_deliver_status == 2:
            status = 'На пути к вам'
        if obj.logistic_deliver_status == 3:
            status = 'Доставлен'
        refund = None
        try:
            refund = RefundOrderItem.objects.get(order_item=obj)
        except:
            pass
        if refund is not None and refund.status == 1:
            status = 'Оформлен возврат'
        if refund is not None and refund.status == 2:
            status = 'Возвращен'
        return status


class OrdersSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()
    transactions = TransactionSerializer(many=True)

    class Meta:
        model = Order
        fields = 'id date name products payment_status order_hash shipping_price email phone address total_price transactions'.split()

    def get_products(self, obj):
        data = OrderProductLogisticItemSerializer(OrderItem.objects.filter(order=obj), many=True).data
        return data


class ClientOrdersSerializer(serializers.ModelSerializer):
    products = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    payment_status = serializers.SerializerMethodField()
    transactions = TransactionSerializer(many=True)

    class Meta:
        model = Order
        fields = 'id date name products payment_status status order_hash shipping_price email phone address total_price transactions'.split()

    def get_products(self, obj):
        data = OrderProductLogisticItemSerializer(OrderItem.objects.filter(order=obj), many=True).data
        return data

    def get_status(self, obj):
        status = 'В ожидании'
        if obj.process_status == 2:
            status = 'В обработке'
        elif obj.process_status == 3:
            status = 'Завершен'
        elif obj.process_status == -1:
            status = 'Отменен'
        elif obj.process_status == 4:
            status = 'Оформлен возврат'
        return status

    def get_payment_status(self, obj):
        status = 'В ожидании'
        if obj.payment_status == 1:
            status = 'Оплачен'
        elif obj.payment_status == 0:
            status = 'Неоплачен'
        return status


class MainColourSerializer(serializers.ModelSerializer):
    name_ru = serializers.SerializerMethodField()

    class Meta:
        model = IziColour
        fields = ('id', 'name', 'name_ru', 'languages', 'hex', 'image')

    def get_name_ru(self, obj):
        try:
            language = Language.objects.all().first()
            translation = TranslationColour.objects.get(colour=obj, language=language)
            return translation.name.capitalize()
        except:
            pass
        return ''


class ParentSearchCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ParentCategory
        fields = (
            'id', 'name', 'code', 'position', 'is_active', 'languages', 'slug')


class DepartmentSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ('id', 'name', 'languages', 'slug')


class VariantsSerializer(serializers.ModelSerializer):
    size = serializers.SerializerMethodField()

    class Meta:
        model = Variant
        fields = 'stock size'.split()

    def get_size(self, obj):
        try:
            izi_size = obj.tr_size.izi_size
            data = {'id': izi_size.id}

        except:
            return None
        tr_name = None
        try:
            tr_name = TranslationSize.objects.filter(size=izi_size)[0].name
        except:
            pass
        if tr_name is None:
            data['name'] = izi_size.name
        else:
            data['name'] = tr_name
        return data


class MainProductSerializer(serializers.ModelSerializer):
    shipping_price = serializers.SerializerMethodField()
    shipping_prices = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    original_price = serializers.SerializerMethodField()
    discount_price = serializers.SerializerMethodField()
    selling_price = serializers.SerializerMethodField()
    parent_category = serializers.SerializerMethodField()
    brand = VendBrandSerializer()
    department = serializers.SerializerMethodField()
    category = serializers.SerializerMethodField()
    colour = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()
    is_favourite = serializers.SerializerMethodField()
    is_hit = serializers.SerializerMethodField()
    is_new = serializers.SerializerMethodField()
    delivery_day = serializers.SerializerMethodField()
    variants = VariantsSerializer(many=True)

    class Meta:
        model = OriginalProduct
        fields = ['discount_price', 'images', 'id', 'parent_category', 'is_hit', 'is_new',
                  'colour', 'created_at', 'title', 'content', 'is_favourite',
                  'original_price', 'updated_at', 'description', 'delivery_day',
                  'brand', 'department', 'category', 'colour', 'variants', 'selling_price', 'shipping_prices',
                  'shipping_price', 'stock']

    def get_shipping_prices(self, obj):
        try:
            country = Country.objects.all()[0]
            average_weight = obj.link.product.category.average_weight
            delivery_price = obj.brand.delivery_price
            delivery_percent = obj.brand.delivery_percent
            price = average_weight * delivery_price
            percent = (price / 100) * delivery_percent
            from_point_price = (price + percent)
            country_delivery_price = country.delivery_price
            country_delivery_percent = country.delivery_percent
            country_price = average_weight * country_delivery_price
            country_percent = (country_price / 100) * country_delivery_percent
            from_country_price = country_price + country_percent
            exchange = ExchangeRate.objects.get(from_currency__code='USD', to_currency__code='KGS')
            x = round((from_country_price + from_point_price) * exchange.value)
            if x % 100 < 51:
                x = (x // 100) * 100 + 50
            else:
                x = (x // 100) * 100 + 100
            return x
        except:
            pass
        return 100

    def get_shipping_price(self, obj):
        return 0

    def get_delivery_day(self, obj):
        day = 8
        try:
            country = Country.objects.all()[0]
            delivery_day = obj.brand.delivery_day
            country_delivery_day = country.delivery_day
            day = delivery_day + country_delivery_day
        except:
            pass
        return day

    def get_is_hit(self, obj):
        return obj.link.product.is_hit

    def get_is_new(self, obj):
        documents = DocumentProduct.objects.filter(product=obj)
        if documents.count() > 0:
            return documents.first().document.is_new
        else:
            return False

    def get_original_price(self, obj):
        brand_country = None
        try:
            brand_country = BrandCountry.objects.filter(brand=obj.brand)[0]
        except:
            pass
        price = 0
        if brand_country is not None:
            try:
                exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                                    to_currency=brand_country.country.currency)
                price = ((round(obj.original_price) * exchange.value * brand_country.mark_up) // 100) * 100 + 100
            except:
                pass
        return price + self.get_shipping_prices(obj)

    def get_content(self, obj):
        name = ''
        try:
            content = obj.izi_content
            name = TranslationContent.objects.filter(content=content).first().name
        except:
            pass
        return name

    def get_is_favourite(self, obj):
        if self.context['request'].user.is_anonymous:
            return False
        elif Favourite.objects.filter(user=self.context['request'].user, product=obj).count() > 0:
            return True
        else:
            return False

    def get_discount_price(self, obj):
        brand_country = None
        try:
            brand_country = BrandCountry.objects.filter(brand=obj.brand)[0]
        except:
            pass
        price = 0
        if brand_country is not None:
            try:
                exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                                    to_currency=brand_country.country.currency)
                price = ((round(obj.discount_price) * exchange.value * brand_country.mark_up) // 100) * 100 + 100
            except:
                pass
        return price + self.get_shipping_prices(obj)

    def get_selling_price(self, obj):
        brand_country = None
        try:
            brand_country = BrandCountry.objects.filter(brand=obj.brand)[0]
        except:
            pass
        price = 0
        if brand_country is not None:
            try:
                exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                                    to_currency=brand_country.country.currency)
                price = ((round(obj.selling_price) * exchange.value * brand_country.mark_up) // 100) * 100 + 100
            except:
                pass
        return price + self.get_shipping_prices(obj)

    def get_images(self, original):
        return original.images.split()

    def get_colour(self, obj):
        try:
            return MainColourSerializer(obj.link.product.colour).data
        except:
            pass
        return None

    def get_department(self, obj):
        try:
            return DepartmentSearchSerializer(obj.izi_department).data
        except:
            pass
        return None

    def get_category(self, obj):
        try:
            return ChildCategorySerializer(obj.izi_category).data
        except:
            pass
        return None

    def get_parent_category(self, obj):
        try:
            return ParentSearchCategorySerializer(obj.izi_parent_category).data
        except:
            return None
