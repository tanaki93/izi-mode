import random
from urllib.parse import unquote

import requests
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from unidecode import unidecode

from product_app.models import Brand, Department, ParentCategory, Category, Size, IziColour, BrandCountry, ExchangeRate, \
    City, Tag, Slider, OriginalProduct, Variant, Country
from product_app.services import ProductService
from projects_app.models import ImageUploader, Order, OrderItem, Transaction, TelegramChat
from projects_app.update_parser.all_brands import parse_products
from user_app.admin.serializers import IziSizeSerializer
from user_app.main.serializers import BrandSerializer, DepartmentSerializer, ParentCategorySerializer, CitySerializer, \
    TagSerializer, SliderSerializer, OrdersSerializer, ClientOrdersSerializer, MainColourSerializer, \
    ParentSearchCategorySerializer, DepartmentSearchSerializer, MainProductSerializer
from user_app.models import ClientAddress
from user_app.operator.serializers import SizeSerializer, IziColorSerializer, ChildCategorySerializer


class ImageUploaderAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        try:
            image = request.FILES['image']
            fs = FileSystemStorage()
            image_name = unidecode(image.name)
            if image_name[-1] == '\"':
                image_name = image_name[:-1]
            filename = fs.save(image_name, image)
            comment_image = ImageUploader.objects.create(image=filename)
            comment_image.save()
            return Response(status=status.HTTP_200_OK,
                            data={'url': comment_image.image.url})
        except:
            pass
        return Response(status=status.HTTP_200_OK)


class MainBrandListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        brands = Brand.objects.filter(is_active=True)
        return Response(status=status.HTTP_200_OK,
                        data=BrandSerializer(brands, many=True).data)


class MainDepartmentListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        departments = Department.objects.filter(is_active=True)
        return Response(status=status.HTTP_200_OK,
                        data=DepartmentSerializer(departments, many=True).data)


def get_price_from(obj):
    brand_country = None
    try:
        brand_country = BrandCountry.objects.all()[0]
    except:
        pass
    price = 0
    if brand_country is not None:
        try:
            exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                                to_currency=brand_country.country.currency)
            price = ((round(obj) * exchange.value * brand_country.mark_up) // 100) * 100 + 100
        except:
            pass
    return price


def get_price_to(obj):
    brand_country = None
    try:
        brand_country = BrandCountry.objects.all()[0]
    except:
        pass
    price = 0
    if brand_country is not None:
        try:
            exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                                to_currency=brand_country.country.currency)
            price = ((round(obj) * exchange.value * brand_country.mark_up) // 100) * 100 + 100
        except:
            pass
    return price


class MainFiltersAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        option = request.data.get('option', '')
        data = {}
        try:
            id = int(request.data.get('id', ''))
            option_obj = None
            if option == 'department':
                option_obj = Department.objects.get(id=id)
            elif option == 'parent_category':
                option_obj = ParentCategory.objects.get(id=id)
            elif option == 'category':
                option_obj = Category.objects.get(id=id)
            data = {'sizes': SizeSerializer(option_obj.sizes.all(), many=True).data,
                    'colours': IziColorSerializer(option_obj.colours.all(), many=True).data,
                    'price_from': get_price_from(option_obj.price_from), 'price_to': get_price_to(option_obj.price_to)}
        except:
            if option == 'all':
                data = {'sizes': SizeSerializer(Size.objects.filter(is_active=True), many=True).data,
                        'colours': IziColorSerializer(IziColour.objects.filter(is_active=True), many=True).data,
                        'price_from': 100, 'price_to': 20000}
        return Response(status=status.HTTP_200_OK, data=data)


class MainCategoryListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        categories = ParentCategory.objects.all()
        return Response(status=status.HTTP_200_OK,
                        data=ParentCategorySerializer(categories, many=True).data)


class MainCityListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        cities = City.objects.filter(is_active=True)
        return Response(status=status.HTTP_200_OK,
                        data=CitySerializer(cities, many=True).data)


class MainTagListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        tags = Tag.objects.filter(name__icontains=request.GET.get('query', '').lower())[0:15]
        return Response(status=status.HTTP_200_OK,
                        data=TagSerializer(tags, many=True).data)


class MainCategoryTopListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        categories = Category.objects.filter(is_active=True)
        return Response(status=status.HTTP_200_OK,
                        data=ChildCategorySerializer(categories, many=True).data)


class MainSliderListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        sliders = Slider.objects.all()[0:6]
        return Response(status=status.HTTP_200_OK,
                        data=SliderSerializer(sliders, many=True).data)


class MainCheckoutAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        products = request.data.get('products')
        products_price = float(request.data.get('products_price', 0))
        total_shipping_price = float(request.data.get('shipping_price', 0))
        total_price = float(request.data.get('total_price', 0))
        order = Order.objects.create(name='', phone='', email=None, delivery_type=1, address='',
                                     property_type=1, products_price=products_price,
                                     shipping_price=total_shipping_price, total_price=total_price, payment_type=0)
        order.save()
        hash_order = str(random.randint(100, 999)) + str(order.id)
        order.order_hash = hash_order
        order.save()
        total_shipping_price = 0
        for i in products:
            try:
                amount = int(i.get('amount', 1))
                price = float(i.get('price', 1))
                shipping_price = float(i.get('shipping_price', 0))
                size_id = int(i.get('size_id', 1))
                product_id = int(i.get('product_id', 1))
                order_item = OrderItem.objects.create(amount=amount, size_id=size_id, product_id=product_id,
                                                      order=order, price=price, shipping_price=shipping_price)
                order_item.save()
            except:
                pass
        order.total_price = order.products_price + total_shipping_price
        order.save()
        return Response(data=OrdersSerializer(order).data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        order_hash = request.data.get('hash', '')
        order = Order.objects.get(order_hash=order_hash)
        order.name = request.data.get('name', '')
        order.phone = request.data.get('phone', '')
        order.email = request.data.get('email', '')
        try:
            order.delivery_type = int(request.data.get('delivery_type', 1))
        except:
            pass
        order.address = request.data.get('address', '')
        try:
            order.address_name = request.data.get('address_name', '')
        except:
            pass
        saved_address = False
        try:
            saved_address = request.data.get('saved_address', False)
        except:
            pass
        if saved_address and not request.user.is_anonymous():
            client_address = ClientAddress.objects.create(user=request.user, address=request.data.get('address', ''),
                                                          name=request.data.get('address_name', ''))
            client_address.save()
        try:
            order.deliver_city_id = int(request.data.get('city_id'))
        except:
            pass
        try:
            order.point_id = int(request.data.get('point_id'))
        except:
            pass
        order.property_type = int(request.data.get('property_type', 1))
        order.payment_type = int(request.data.get('payment_type', 2))
        if not request.user.is_anonymous():
            order.client = request.user
        order.save()
        return Response(data=OrdersSerializer(order).data, status=status.HTTP_200_OK)


class MainTransactionAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        hash_order = request.GET.get('hash', '')
        try:
            order = Order.objects.filter(order_hash=hash_order).first()
            if order.payment_status == 0:
                return Response(data=OrdersSerializer(order).data, status=status.HTTP_200_OK)
            else:
                return Response(data=OrdersSerializer(order).data, status=status.HTTP_200_OK)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)


class MainFindOrderAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        hash_order = request.GET.get('hash', '')
        order = None
        try:
            order = Order.objects.filter(order_hash=hash_order)[0]
        except:
            pass
        if order is not None:
            return Response(data=ClientOrdersSerializer(order).data, status=status.HTTP_200_OK)
        elif order is None:
            return Response(status=status.HTTP_404_NOT_FOUND)


def telegram_bot_sendtext(bot_message):
    for j in TelegramChat.objects.all():
        bot_chatID = '%s' % j.chat
        bot_token = '1097337743:AAHQ4aOXQrTsDO3ZlzHVeUcjA77Ys_4VlMg'
        send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=HTML&text=' + bot_message
        response = requests.get(send_text)
        print(response.json())
    return ''


def send_html_to_email(to, hash):
    sender_email = settings.EMAIL_HOST_USER
    password = settings.EMAIL_HOST_PASSWORD
    receiver_email = to

    import smtplib

    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    # me == my email address
    # you == recipient's email address
    me = sender_email
    you = receiver_email

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "IziShop - Заказ оплачен"
    msg['From'] = me
    msg['To'] = you

    # Create the body of the message (a plain-text and an HTML version).
    parts1 = """\
    <!doctype html>
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

    <head>
      <title> </title>
      <!--[if !mso]> -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--<![endif]-->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
        #outlook a {
          padding: 0;
        }

        body {
          margin: 0;
          padding: 0;
          -webkit-text-size-adjust: 100%;
          -ms-text-size-adjust: 100%;
        }

        table,
        td {
          border-collapse: collapse;
          mso-table-lspace: 0pt;
          mso-table-rspace: 0pt;
        }

        img {
          border: 0;
          height: auto;
          line-height: 100%;
          outline: none;
          text-decoration: none;
          -ms-interpolation-mode: bicubic;
        }

        p {
          display: block;
          margin: 13px 0;
        }
      </style>
      <!--[if mso]>
            <xml>
            <o:OfficeDocumentSettings>
              <o:AllowPNG/>
              <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
      <!--[if lte mso 11]>
            <style type="text/css">
              .mj-outlook-group-fix { width:100% !important; }
            </style>
            <![endif]-->
      <!--[if !mso]><!-->
      <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
      <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
      </style>
      <!--<![endif]-->
      <style type="text/css">
        @media only screen and (min-width:480px) {
          .mj-column-per-100 {
            width: 100% !important;
            max-width: 100%;
          }
        }
      </style>
      <style type="text/css">
        @media only screen and (max-width:480px) {
          table.mj-full-width-mobile {
            width: 100% !important;
          }
          td.mj-full-width-mobile {
            width: auto !important;
          }
        }
      </style>
      <style type="text/css">
        .code {
          background-color: white;
          box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.08);
          width: 100%;
          max-width: 250px;
          display: block;
          margin: 0 auto;
        }
      </style>
    </head>

    <body>
      <div style="">
        <!--[if mso | IE]>
          <table
             align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
          >
            <tr>
              <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
          <![endif]-->
        <div style="margin:0px auto;max-width:600px;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                  <!--[if mso | IE]>
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">

            <tr>

                <td
                   class="" style="vertical-align:top;width:600px;"
                >
              <![endif]-->
                  <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                      <tr>
                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                            <tbody>
                              <tr>
                                <td style="width:158px;"> <img height="auto" src="https://izishop.kg/img/logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="158" /> </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td style="font-size:0px;word-break:break-word;">
                          <!--[if mso | IE]>

            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="70" style="vertical-align:top;height:70px;">

        <![endif]-->
                          <div style="height:70px;"> &nbsp; </div>
                          <!--[if mso | IE]>

            </td></tr></table>

        <![endif]-->
                        </td>
                      </tr>
                      <tr>
                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <div style="font-family:helvetica;font-size:18px;font-weight:bold;line-height:23px;text-align:center;color:#000000;">Узнайте как там ваш заказ )</div>
                        </td>
                      </tr>
                      <tr>
                        <td style="font-size:0px;word-break:break-word;">
                          <!--[if mso | IE]>

            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="20" style="vertical-align:top;height:20px;">

        <![endif]-->
                          <div style="height:20px;"> &nbsp; </div>
                          <!--[if mso | IE]>

            </td></tr></table>

        <![endif]-->
                        </td>
                      </tr>
                      <tr>
                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <div style="font-family:helvetica;font-size:12px;font-weight:bold;line-height:15px;text-align:center;color:#000000;">Код заказа</div>
                        </td>
                      </tr>
                      <tr>
                        <td align="center" class="code" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <div style="font-family:helvetica;font-size:24px;font-weight:bold;line-height:30px;text-align:center;color:#000000;">
                          """

    parts2 = """
    </div>
                        </td>
                      </tr>
                      <tr>
                        <td style="font-size:0px;word-break:break-word;">
                          <!--[if mso | IE]>

            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="35" style="vertical-align:top;height:35px;">

        <![endif]-->
                          <div style="height:35px;"> &nbsp; </div>
                          <!--[if mso | IE]>

            </td></tr></table>

        <![endif]-->
                        </td>
                      </tr>
                      <tr>
                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <div style="font-family:helvetica;font-size:12px;line-height:15px;text-align:center;color:#000000;">Вы можете узнать на какой стадии обработки сейчас находится Ваш заказ введя номер заказа в поле поиска заказа (нажмите на компас на шапке сайта), или вы можете просто нажать на кнопку ниже</div>
                        </td>
                      </tr>
                      <tr>
                        <td align="center" vertical-align="middle" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:178px;line-height:100%;">
                            <tr>
                              <td align="center" bgcolor="#ED1B23" role="presentation" style="border:none;border-radius:3px;cursor:auto;height:40px;mso-padding-alt:10px 25px;background:#ED1B23;" valign="middle"> <a href="https://izishop.kg/find/"""
    parts3 = """
    " style="display:inline-block;width:128px;background:#ED1B23;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:14px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"
                                  target="_blank">
                  Где мой заказ?
                </a> </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td style="font-size:0px;word-break:break-word;">
                          <!--[if mso | IE]>

            <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="100" style="vertical-align:top;height:100px;">

        <![endif]-->
                          <div style="height:100px;"> &nbsp; </div>
                          <!--[if mso | IE]>

            </td></tr></table>

        <![endif]-->
                        </td>
                      </tr>
                      <tr>
                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:12px;line-height:15px;text-align:center;color:#000000;"><a href="" style="color: #ED1B23">
              www.izishop.kg
              </a></div>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <!--[if mso | IE]>
                </td>

            </tr>

                      </table>
                    <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!--[if mso | IE]>
              </td>
            </tr>
          </table>
          <![endif]-->
      </div>
    </body>

    </html>
    """

    # Record the MIME types of both parts - text/plain and text/html.
    part2 = MIMEText(parts1 + hash + parts2 + hash + parts3, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part2)
    # Send the message via local SMTP server.
    mail = smtplib.SMTP('smtp.yandex.ru', 587)

    mail.ehlo()

    mail.starttls()

    mail.login(sender_email, password)
    mail.sendmail(me, you, msg.as_string())
    mail.quit()


class MainPayboxAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        hash_order = request.data.get('pg_order_id', '')
        trans_id = request.data.get('pg_payment_id', '')
        auth_code = request.data.get('pg_auth_code', '')
        pg_payment_system = ''
        try:
            pg_payment_system = request.data.get('pg_payment_system', '')
        except:
            pass
        amount = float(request.data.get('pg_amount', 0))
        statuses = 0
        try:
            statuses = int(request.data.get('pg_result', 0))
        except:
            pass
        pan = request.data.get('pg_card_pan', '')
        card_brand = request.data.get('pg_card_brand', '')
        order = None
        try:
            order = Order.objects.get(order_hash=hash_order)
        except:
            pass
        if order is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        transaction = Transaction.objects.create(order=order, pan=pan, trans_id=trans_id, amount=amount,
                                                 status=statuses, auth_code=auth_code, card_brand=card_brand,
                                                 pg_payment_system=pg_payment_system)
        transaction.save()
        text = '<b>Заказ № %s оплачен</b>\n<a href=\"https://admin.izishop.kg/orders/%s\">Ссылка на заказ </a>\n <b>Сумма заказа: %s</b>' % (
            order.order_hash, order.id, order.total_price)
        if statuses == 1:
            telegram_bot_sendtext(text)
        order.payment_status = statuses
        order.payment_type = 2
        order.save()
        if order.payment_status == 1:
            try:
                send_html_to_email(order.email, order.order_hash)
            except:
                pass
        return Response(status=status.HTTP_200_OK)


class MainSizeAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        sizes = Size.objects.filter(is_active=True).distinct('name').order_by('-name')
        return Response(data=IziSizeSerializer(sizes, many=True).data,
                        status=status.HTTP_200_OK)


class MainColourListAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        return Response(data=MainColourSerializer(IziColour.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)


class MainSearchAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        query = None
        try:
            query = (request.GET.get('query', ''))
        except:
            pass
        query = unquote(query).encode('utf8')
        categories = Category.objects.filter(parent__isnull=False) \
            .filter(Q(name_lower__icontains=query.lower()) | Q(
            translations__name_lower__icontains=query.lower()))
        data = []
        for category in categories:
            context = {
                'option': 'category',
                'id': category.id,
                'slug': category.slug,
                'name': category.name,
                'parent': ParentSearchCategorySerializer(category.parent).data,
                'department': DepartmentSearchSerializer(category.parent.department).data,
                'translations': category.languages
            }
            data.append(context)
        return Response(data=data, status=status.HTTP_200_OK)


class MainProductItemAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        try:
            product = OriginalProduct.objects.get(id=kwargs['product_id'],
                                                  document__step=100)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(data=MainProductSerializer(product, context={'request': request}).data,
                        status=status.HTTP_200_OK)


def get_shipping_prices(obj):
    try:
        country = Country.objects.all()[0]
        average_weight = obj.link.product.category.average_weight
        delivery_price = obj.brand.delivery_price
        delivery_percent = obj.brand.delivery_percent
        price = average_weight * delivery_price
        percent = (price / 100) * delivery_percent
        from_point_price = (price + percent)
        country_delivery_price = country.delivery_price
        country_delivery_percent = country.delivery_percent
        country_price = average_weight * country_delivery_price
        country_percent = (country_price / 100) * country_delivery_percent
        from_country_price = country_price + country_percent
        exchange = ExchangeRate.objects.get(from_currency__code='USD', to_currency__code='KGS')
        x = round((from_country_price + from_point_price) * exchange.value)
        if x % 100 < 51:
            x = (x // 100) * 100 + 50
        else:
            x = (x // 100) * 100 + 100
        return x
    except:
        pass
    return 100


def get_selling_price(obj):
    brand_country = None
    try:
        brand_country = BrandCountry.objects.filter(brand=obj.brand)[0]
    except:
        pass
    price = 0
    if brand_country is not None:
        try:
            exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                                to_currency=brand_country.country.currency)
            price = ((round(obj.selling_price) * exchange.value * brand_country.mark_up) // 100) * 100 + 100
        except:
            pass
    return price + get_shipping_prices(obj)


class MainCheckProductsAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        data = []
        for i in request.data:
            product = OriginalProduct.objects.get(id=int(i['product_id']))
            parse_products(product)
            variants = Variant.objects.filter(original_product_id=int(i['product_id']), stock=True,
                                              tr_size__izi_size_id=int(i['size_id'])).count()
            available = False
            if variants > 0 and product.stock:
                available = True
            checked_product = {
                'product_id': i['product_id'],
                'available': available,
                'price': get_selling_price(OriginalProduct.objects.get(id=i['product_id']))
            }
            data.append(checked_product)
        return Response(status=status.HTTP_200_OK, data=data)


class MainRandomHitProductsAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        data = MainProductSerializer(ProductService.get_client_random_products(),
                                     many=True,
                                     context={'request': request}).data
        return Response(status=status.HTTP_200_OK, data=data)


class MainProductsAPIView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        data = ProductService.get_client_izi_products(request.query_params, request)
        return Response(status=status.HTTP_200_OK, data=data)

    def post(self, request, *args, **kwargs):
        data = ProductService.get_client_izi_products(request.data, request)
        return Response(status=status.HTTP_200_OK, data=data)