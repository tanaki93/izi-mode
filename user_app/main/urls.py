from django.urls import path

from user_app.main import views

urlpatterns = [
    path('upload/',
         views.ImageUploaderAPIView.as_view()),

    path('brands/',
         views.MainBrandListAPIView.as_view()),

    path('departments/',
         views.MainDepartmentListAPIView.as_view()),

    path('filters/',
         views.MainFiltersAPIView.as_view()),

    path('categories/',
         views.MainCategoryListAPIView.as_view()),

    path('cities/',
         views.MainCityListAPIView.as_view()),

    path('tags/',
         views.MainTagListAPIView.as_view()),

    path('categories/top/',
         views.MainCategoryTopListAPIView.as_view()),

    path('sliders/',
         views.MainSliderListAPIView.as_view()),

    path('checkout/',
         views.MainCheckoutAPIView.as_view()),

    path('transaction/',
         views.MainTransactionAPIView.as_view()),

    path('find/order/',
         views.MainFindOrderAPIView.as_view()),

    path('paybox/result/',
         views.MainFindOrderAPIView.as_view()),

    path('sizes/',
         views.MainSizeAPIView.as_view()),

    path('colours/',
         views.MainColourListAPIView.as_view()),

    path('search/',
         views.MainSearchAPIView.as_view()),

    path('products/<int:product_id>/',
         views.MainProductItemAPIView.as_view()),

    path('check/products/',
         views.MainCheckProductsAPIView.as_view()),

    path('random/hits/',
         views.MainRandomHitProductsAPIView.as_view()),

    path('products/',
         views.MainProductsAPIView.as_view()),
]
