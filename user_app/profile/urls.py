from django.conf.urls import url
from django.urls import path

from user_app.profile import views

urlpatterns = [
    url(r'^google/$', views.GoogleLogin.as_view(), name='google_login'),
    url(r'^facebook/$', views.FacebookLogin.as_view(), name='facebook_login'),
    path('favourites/', views.ClientFavouriteAPIView.as_view()),
    # url(r'^orders/$', views.client_orders_view),
    # url(r'^address/$', views.client_address_view),
    # url(r'^address/([0-9]+)/$', views.client_address_item_view),
    # url(r'^data/$', views.client_data_view),
    # url(r'^refunds/$', views.client_refund_list_view),
]
