import os
from typing import Union

import requests
from django.contrib.auth import get_user_model
from django.db.models import QuerySet, Q
from rest_framework.exceptions import NotFound, ValidationError

from product_app.models import Product
from projects_app.models import Favourite

User = get_user_model()


class FavouriteService:
    model = Favourite

    @classmethod
    def get(cls, **filters):
        try:
            return Favourite.objects.get(**filters)
        except Favourite.DoesNotExist:
            raise NotFound('Favoutite not found')

    @classmethod
    def get_client_favourites(cls, filters) -> QuerySet:
        return Favourite.objects.filter(**filters)

    @classmethod
    def create(cls, product: Product, user: User):
        try:
            return cls.model.objects.create(
                product=product,
                user=user
            )
        except Exception as e:
            raise ValidationError('Error while creating distributions: {e}'.format(e=str(e)))
