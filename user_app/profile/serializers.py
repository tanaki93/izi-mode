from rest_framework import serializers

from projects_app.models import Favourite


class FavouriteSerializer(serializers.ModelSerializer):
    # product = FavouriteProductSerializer(read_only=True)

    class Meta:
        model = Favourite
        fields = ('id', 'created', 'updated', 'product')


class FavouriteCreateSerializer(serializers.Serializer):
    product_id = serializers.IntegerField()
