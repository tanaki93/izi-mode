# Create your views here.

from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from product_app.services import ProductService
from user_app.profile.serializers import FavouriteSerializer, FavouriteCreateSerializer
from user_app.profile.services import FavouriteService


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


from rest_framework.views import APIView


class ClientFavouriteAPIView(APIView):
    """
    API favourite list
    """

    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = FavouriteSerializer

    def get(self, request, *args, **kwargs):
        return FavouriteService.get_client_favourites(
            {
                'user': self.request.user
            }
        )

    def post(self, request, *args, **kwargs):
        serializer = FavouriteCreateSerializer(data=request.data, many=False)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        product = ProductService.get(id=serializer.validated_data.get('product_id'))
        favourite = FavouriteService.create(
            product=product,
            user=request.user
        )
        return Response(
            self.serializer_class(favourite, many=False).data, status=status.HTTP_201_CREATED
        )

    def put(self, request, *args, **kwargs):
        serializer = FavouriteCreateSerializer(data=request.data, many=False)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        product = ProductService.get(id=serializer.validated_data.get('product_id'))
        favourites = FavouriteService.get_client_favourites(
            {
                'user': self.request.user,
                'product': product
            }
        )
        favourites.delete()
        return Response(status=status.HTTP_201_CREATED
                        )
