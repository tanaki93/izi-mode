import datetime
import random
import string

from django.conf import settings
from django.contrib.auth import authenticate

# Create your views here.
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from user_app.models import ConfirmationCode, User, ResetPasswordCode
from user_app.permissions import IsAdmin
from user_app.serializers import TokenSerializer

import smtplib

from user_app.services import UserService


def send_email(request, email, subject, text):
    sender_email = settings.EMAIL_HOST_USER
    password = settings.EMAIL_HOST_PASSWORD
    receiver_email = email
    email = sender_email

    server = smtplib.SMTP('smtp.yandex.ru', 587)
    server.ehlo()  # Кстати, зачем это?
    server.starttls()
    server.login(email, password)

    dest_email = receiver_email
    email_text = text
    message = 'From: %s\nTo: %s\nSubject: %s\n\n%s' % (email, dest_email, subject, email_text)

    server.set_debuglevel(1)  # Необязательно; так будут отображаться данные с сервера в консоли
    server.sendmail(email, dest_email, message)
    server.quit()


def token_generator():
    return ''.join([random.choice(string.ascii_letters) for x in range(10)])


class ConfirmView(APIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = TokenSerializer

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            code = request.data.get('code', '')
            try:
                confirmation_code = ConfirmationCode.objects.get(token=code,
                                                                 valid_until__gte=datetime.datetime.now())
            except ConfirmationCode.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
            user = confirmation_code.user
            user.is_active = True
            user.save()
            confirmation_code.delete()
            try:
                token = Token.objects.get(user=user)
            except:
                token = Token.objects.create(user=user)
                token.save()
            if user.is_active:
                return Response(data=self.serializer_class(token).data, status=status.HTTP_200_OK)
            else:
                return Response(data={'data': 'Профиль не активирован'}, status=status.HTTP_406_NOT_ACCEPTABLE)


class RegisterView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        username = request.data['username']
        password = request.data['password']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        user = User.objects.create(username=username, last_name='', first_name='',
                                   user_type=request.data.get('user_type', 3), is_active=False)
        user.set_password(password)
        user.save()
        token = ConfirmationCode.objects.create(user=user, token=token_generator(),
                                                valid_until=datetime.datetime.now() + datetime.timedelta(minutes=120))
        token.save()
        send_email(request, user.username, 'IziShop - Confirmation code',
                   'https://izishop.kg/confirmation/%s' % token.token)
        return Response(status=status.HTTP_201_CREATED, data={'data': 'Пользователь создан'})


class LoginView(APIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = TokenSerializer

    def post(self, request, *args, **kwargs):
        user = authenticate(username=request.data['username'], password=request.data['password'])

        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'data': 'Неправильный логин или пароль'})
        else:
            try:
                token = Token.objects.get(user=user)
            except Token.DoesNotExist:
                token = Token.objects.create(user=user)
                token.save()
            if user.is_active:
                return Response(data=self.serializer_class(token).data, status=status.HTTP_200_OK)
            else:
                return Response(data={'data': 'Профиль не активирован'}, status=status.HTTP_406_NOT_ACCEPTABLE)


class ReAuthView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = TokenSerializer

    def get(self, request, *args, **kwargs):
        user = request.user
        if not user:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'data': 'Пользователь не найден'})
        else:
            token = Token.objects.get(user=user)
            return Response(data=self.serializer_class(token).data, status=status.HTTP_200_OK)


class ResendCodeView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        user = User.objects.get(username=request.data.get('email'))
        try:
            ConfirmationCode.objects.get(user=user,
                                         valid_until__gte=datetime.datetime.now()).delete()
        except:
            pass
        until = datetime.datetime.now() + datetime.timedelta(minutes=30)
        token = ConfirmationCode.objects.create(user=user, token=token_generator(),
                                                valid_until=until)
        send_email(request, [user.username], 'IziShop - Confirmation code',
                   'https://izishop.kg/confirmation/%s' % token.token,
                   )

        return Response(status=status.HTTP_200_OK, data={'data': 'Код отправлен на почту'})


class ResetPasswordView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        user = None
        try:
            user = User.objects.get(username=request.data.get('email'), is_active=True)
        except:
            pass
        if user is not None:
            ResetPasswordCode.objects.filter(user=user,
                                             valid_until__gte=datetime.datetime.now()).delete()

            token = ResetPasswordCode.objects.create(user=user, token=token_generator(),
                                                     valid_until=datetime.datetime.now() + datetime.timedelta(
                                                         minutes=20))
            email_body = """
            To reset password click here: https://izishop.kg/user/reset/%s
            thanks, 
            IziShop Team.
            """ % token.token
            send_email(request, user.username, 'IziShop - reset password link', email_body, )
            return Response(status=status.HTTP_200_OK, data={'data': 'Код отправлен на почту'})
        else:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'data': 'Пользватель не найден'})


class ChangePasswordView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        user = None
        try:
            code = request.data.get('code', '')
            codes = ResetPasswordCode.objects.filter(token=code).first()
            user = codes.user
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'data': 'Время истекло'})
        if user is not None:
            password = request.data.get('password')
            user.set_password(raw_password=password)
            user.save()
            return Response(status=status.HTTP_200_OK, data={'data': 'Пароль изменен'})


class AddUserView(APIView):
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)

    def post(self, request, *args, **kwargs):
        user = UserService.create(username=request.data['username'],
                                  email=request.data.get('email', ''),
                                  password=request.data['password'],
                                  phone=request.data.get('phone'),
                                  user_type=int(request.data.get('user_type', 2))
                                  )
        user.save()
        return Response(status=status.HTTP_201_CREATED)
