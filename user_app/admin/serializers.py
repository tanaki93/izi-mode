from django.db.models import Q
from rest_framework import serializers

from product_app.models import Brand, Document, VendDepartment, Department, Category, VendCategory, IziColour, Content, \
    Language, TranslationContent, DocumentComment, OriginalProduct, Link, Currency, Country, City, DeliveryPoint, \
    ExchangeRate, ExchangeValue, Size, VendColour, BrandCountry
from projects_app.models import RefundOrder, RefundOrderItem, OrderItem, Order, RefundOrderItemImage
from user_app.serializers import UserSerializer


class BrandAdminDetailedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id', 'name', 'is_active', 'total_count', 'document_count', 'processed_count', 'in_process_count',
                  'out_process_count', 'no_price_or_stock')


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendDepartment
        fields = "__all__"


class BrandAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id', 'name', 'is_active')


class DocumentsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    department = DepartmentSerializer()
    brand = BrandAdminSerializer()

    class Meta:
        model = Document
        fields = (
            'id', 'updated_at', 'user', 'step', 'status', 'department', 'brand', 'products', 'no_stock_or_price',
            'is_new')


class VendCategoryDetailedSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendCategory
        fields = ('id', 'name', 'link', 'is_active',)


class CategoryDetailedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'is_active', 'size_table')


class VendDepartmentDetailedSerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField()

    class Meta:
        model = VendDepartment
        fields = ('id', 'name', 'is_active', 'categories')

    def get_categories(self, obj):
        categories = VendCategory.objects.filter(department=obj)
        return VendCategoryDetailedSerializer(categories, many=True).data


class DepartmentDetailedSerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField()

    class Meta:
        model = Department
        fields = ('id', 'name', 'is_active', 'categories')

    def get_categories(self, obj):
        categories = Category.objects.filter(parent__department=obj)
        return CategoryDetailedSerializer(categories, many=True).data


class BrandProcessSerializer(serializers.ModelSerializer):
    departments = serializers.SerializerMethodField()

    class Meta:
        model = Brand
        fields = ('id', 'name', 'is_active', 'departments')

    def get_departments(self, obj):
        departments = Department.objects.filter(departments__brand=obj).order_by('id').distinct('id')
        return DepartmentDetailedSerializer(departments, many=True).data


class IziColourSerializer(serializers.ModelSerializer):
    class Meta:
        model = IziColour
        fields = '__all__'


class ContentSerializer(serializers.ModelSerializer):
    is_related = serializers.SerializerMethodField()

    class Meta:
        model = Content
        fields = ('id', 'name', 'languages', 'code', 'is_related')

    def get_is_related(self, obj):
        return False


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentComment
        fields = ('id', 'text', 'updated_at')


class ProductDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendDepartment
        fields = "id name".split()


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = VendCategory
        fields = "id name".split()


class ProductBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = "id name".split()


class ProductIziDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'


class ProductIziCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "id name languages".split()


class ProductIziColourSerializer(serializers.ModelSerializer):
    class Meta:
        model = IziColour
        fields = "id name".split()


class ProductIziContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = "id name".split()


class ProductVendLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = "__all__"


class ColourSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendColour
        fields = ('id', 'name', 'name_en')


class ProductSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()
    colour = serializers.SerializerMethodField()
    vend_colour = serializers.SerializerMethodField()
    department = ProductDepartmentSerializer()
    category = ProductCategorySerializer()
    brand = ProductBrandSerializer()
    izi_department = ProductIziDepartmentSerializer()
    izi_category = ProductIziCategorySerializer()
    link = ProductVendLinkSerializer()

    class Meta:
        model = OriginalProduct
        fields = ('id', 'title', 'vend_colour', 'images', 'department', 'category', 'brand', 'izi_department',
                  'izi_category', 'colour', 'content', 'link', 'discount_price', 'selling_price',
                  'original_price', 'is_rush_delivery', 'stock', 'is_free_argo', 'delivery_date',
                  'description', 'shipping_price', 'updated_at', 'promotions', 'delivery_day')

    def get_colour(self, obj):
        return ProductIziColourSerializer(obj.izi_colour).data

    def get_content(self, obj):
        return ProductIziContentSerializer(obj.izi_content).data

    def get_vend_colour(self, obj):
        colour = None
        try:
            colour = obj.colour
        except:
            pass
        return ColourSerializer(colour).data

    def get_images(self, obj):
        return obj.images.split()


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = 'id code name code_name is_active is_related'.split()


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = 'id code name is_translate is_active is_related'.split()


class CountrySerializer(serializers.ModelSerializer):
    currency = CurrencySerializer()
    language = LanguageSerializer()

    class Meta:
        model = Country
        fields = 'id code name currency day_count_newest language is_active is_related delivery_percent delivery_day delivery_price'.split()


class CitySerializer(serializers.ModelSerializer):
    country = CountrySerializer()
    is_related = serializers.SerializerMethodField()

    class Meta:
        model = City
        fields = 'id name is_active country is_related delivery_percent delivery_day delivery_price'.split()

    def get_is_related(self, obj):
        return False


class DeliveryPointSerializer(serializers.ModelSerializer):
    city = CitySerializer()

    class Meta:
        model = DeliveryPoint
        fields = 'id name lat lng delivery_price is_active city'.split()


class ValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExchangeValue
        fields = '__all__'


class ExchangeRateSerializer(serializers.ModelSerializer):
    from_currency = CurrencySerializer()
    to_currency = CurrencySerializer()
    values = ValueSerializer(many=True)
    is_related = serializers.SerializerMethodField()

    class Meta:
        model = ExchangeRate
        fields = 'id from_currency to_currency date value values is_related'.split()

    def get_is_related(self, obj):
        return True


class OrderSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    count = serializers.SerializerMethodField()
    # user = UserSerializer()
    client = UserSerializer()

    class Meta:
        model = Order
        fields = 'id date name payment_status payment_type process_status email client phone address price count'.split()

    def get_price(self, obj):
        items = OrderItem.objects.filter(order=obj)
        price = 0
        for i in items:
            price += i.price
        return price

    def get_count(self, obj):
        return OrderItem.objects.filter(order=obj).count()


class IziSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ('id', 'name', 'code')


class OrderProductItemSerializer(serializers.ModelSerializer):
    size = IziSizeSerializer(read_only=True)
    product = ProductSerializer(read_only=True)

    class Meta:
        model = OrderItem
        fields = 'id size product price amount ' \
                 ' updated package shipping_price'.split()


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = RefundOrderItemImage
        fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):
    order_item = OrderProductItemSerializer()
    item_images = ImageSerializer(many=True)

    class Meta:
        model = RefundOrderItem
        fields = 'id order_item status details comment item_images'.split()


class RefundOrderListSerializer(serializers.ModelSerializer):
    order = OrderSerializer()
    items = ItemSerializer(many=True)

    class Meta:
        model = RefundOrder
        fields = 'id order items updated created price status refund_type'.split()


class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = '__all__'


class LinkWithBrandSerializer(serializers.ModelSerializer):
    code = serializers.SerializerMethodField()

    class Meta:
        model = Link
        fields = 'id url code'.split()

    def get_code(self, obj):
        return obj.originalproduct.brand.code


class ParentDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'


class CategoryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'size_table', 'languages')


class TrendYolDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendDepartment
        fields = ('id', 'name')


class VendBrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id', 'name')


class TrendYolCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = VendCategory
        fields = ('id', 'name', 'is_active')


class VendProductSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    department = TrendYolDepartmentSerializer()
    brand = VendBrandSerializer()
    vend_colour = serializers.SerializerMethodField()
    category = TrendYolCategorySerializer()

    class Meta:
        model = OriginalProduct
        fields = ['selling_price', 'discount_price', 'is_free_argo', 'images', 'delivery_date', 'product_code', 'id',
                  'colour', 'promotions', 'created_at', 'is_active', 'brand', 'product_id', 'is_rush_delivery',
                  'title',
                  'original_price', 'updated_at', 'description', 'department', 'category', 'vend_colour']

    def get_images(self, obj):
        return obj.images.split()

    def get_vend_colour(self, obj):
        return ColourSerializer(obj.colour).data


class IziShopProductSerializer(serializers.ModelSerializer):
    link = LinkSerializer()
    department = serializers.SerializerMethodField()
    category = serializers.SerializerMethodField()
    colour = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()
    vend_product = serializers.SerializerMethodField()
    selling_price = serializers.SerializerMethodField()
    izi_link = serializers.SerializerMethodField()

    class Meta:
        model = OriginalProduct
        fields = ['id', 'created_at',
                  'link', 'updated_at',
                  'department', 'category',
                  'colour',
                  'content', 'vend_product',
                  'is_sellable', 'izi_link',
                  'is_hit', 'is_marketolog',
                  'selling_price']

    def get_department(self, obj):
        return ParentDepartmentSerializer(obj.izi_department).data

    def get_category(self, obj):
        return CategoryItemSerializer(obj.izi_category).data

    def get_vend_product(self, obj):
        return VendProductSerializer(obj).data

    def get_colour(self, obj):
        return IziColourSerializer(obj.izi_colour).data

    def get_content(self, obj):
        return ContentSerializer(obj.izi_content).data

    def get_izi_link(self, product):
        link = 'https://izishop.kg/'
        try:
            link = 'https://izishop.kg/%s/%s/%s/%s' % (
                product.izi_department.slug, product.izi_parent_category.slug, product.izi_category.slug,
                product.id)
        except:
            pass
        return link

    def get_selling_price(self, obj):
        brand_country = None
        try:
            brand_country = BrandCountry.objects.filter(brand=obj.brand)[0]
        except:
            pass
        selling_price = 0
        if brand_country is not None:
            try:
                exchange = ExchangeRate.objects.get(from_currency=brand_country.brand.currency,
                                                    to_currency=brand_country.country.currency)
                selling_price = ((round(
                    obj.selling_price) * exchange.value * brand_country.mark_up) // 100) * 100 + 100
            except:
                pass
        shipping_price = 100
        try:
            country = Country.objects.first()
            average_weight = obj.category.average_weight
            delivery_price = obj.brand.delivery_price
            delivery_percent = obj.brand.delivery_percent
            price = average_weight * delivery_price
            percent = (price / 100) * delivery_percent
            from_point_price = (price + percent)
            country_delivery_price = country.delivery_price
            country_delivery_percent = country.delivery_percent
            country_price = average_weight * country_delivery_price
            country_percent = (country_price / 100) * country_delivery_percent
            from_country_price = country_price + country_percent
            exchange = ExchangeRate.objects.get(from_currency__code='USD', to_currency__code='KGS')
            shipping_price = round((from_country_price + from_point_price) * exchange.value)
            if shipping_price % 100 < 51:
                shipping_price = (shipping_price // 100) * 100 + 50
            else:
                shipping_price = (shipping_price // 100) * 100 + 100
        except:
            pass
        return selling_price + shipping_price


class CountryCreateSerializer(serializers.Serializer):
    language_id = serializers.IntegerField(allow_null=True, default=0)
    currency_id = serializers.IntegerField(allow_null=True, default=0)
    delivery_price = serializers.IntegerField(allow_null=True, default=0)
    day_count_newest = serializers.IntegerField(allow_null=True, default=0)
    delivery_percent = serializers.IntegerField(allow_null=True, default=0)
    delivery_day = serializers.IntegerField(allow_null=True, default=0)
    code = serializers.CharField(default='')
    name = serializers.CharField(default='')
    is_active = serializers.BooleanField(default=True)


class CurrencyCreateSerializer(serializers.Serializer):
    code = serializers.CharField(default='')
    code_name = serializers.CharField(default='')
    name = serializers.CharField(default='')
    is_active = serializers.BooleanField(default=True)


class CityCreateSerializer(serializers.Serializer):
    country_id = serializers.IntegerField(allow_null=True, default=0)
    delivery_price = serializers.IntegerField(allow_null=True, default=0)
    delivery_percent = serializers.IntegerField(allow_null=True, default=0)
    delivery_day = serializers.IntegerField(allow_null=True, default=0)
    name = serializers.CharField(default='')
    is_active = serializers.BooleanField(default=True)


class PointCreateSerializer(serializers.Serializer):
    city_id = serializers.IntegerField(allow_null=True, default=0)
    delivery_price = serializers.IntegerField(allow_null=True, default=0)
    lat = serializers.FloatField(allow_null=True, default=0)
    lng = serializers.FloatField(allow_null=True, default=0)
    name = serializers.CharField(default='')
    is_active = serializers.BooleanField(default=True)


class LanguageCreateSerializer(serializers.Serializer):
    code = serializers.CharField(default='')
    name = serializers.CharField(default='')
    is_active = serializers.BooleanField(default=True)
    is_translate = serializers.BooleanField(default=True)


class ExchangeCreateSerializer(serializers.Serializer):
    from_currency_id = serializers.IntegerField(allow_null=False)
    to_currency_id = serializers.IntegerField(allow_null=False)
    date = serializers.CharField(default='')
    value = serializers.FloatField(default=0)
    is_active = serializers.BooleanField(default=True)


class BrandCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=False)
    link = serializers.CharField(allow_null=False)
    code = serializers.CharField(default='')
    is_active = serializers.BooleanField(default=True)
    currency_id = serializers.IntegerField(default=None, allow_null=True)
    countries = serializers.ListField(allow_null=True, default=[])
    delivery_price = serializers.IntegerField(allow_null=True, default=0)
    delivery_percent = serializers.IntegerField(allow_null=True, default=0)
    delivery_day = serializers.IntegerField(allow_null=True, default=0)


class DepartmentCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=True,
                                 default='', allow_blank=True)
    code = serializers.CharField(default='', allow_null=True)
    is_active = serializers.BooleanField(default=True)
    position = serializers.IntegerField(default=0, allow_null=True)
    languages = serializers.ListField(allow_null=True, default=[])
    department_id = serializers.IntegerField(allow_null=True, default=0)


class CategoryCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=True,
                                 default='', allow_blank=True)
    code = serializers.CharField(default='', allow_null=True)
    is_active = serializers.BooleanField(default=True)
    position = serializers.IntegerField(default=0, allow_null=True)
    languages = serializers.ListField(allow_null=True, default=[])
    category_id = serializers.IntegerField(allow_null=True, default=0)
    parent_id = serializers.IntegerField(allow_null=True, default=0)
    average_weight = serializers.FloatField(allow_null=True, default=0)


class IziColourCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=True,
                                 default='', allow_blank=True)
    code = serializers.CharField(default='', allow_null=True)
    hex = serializers.CharField(allow_null=True, required=False,
                                allow_blank=True, default=True)
    is_active = serializers.BooleanField(default=True)
    languages = serializers.ListField(allow_null=True, default=[])


class ParentCategoryCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=True,
                                 default='', allow_blank=True)
    code = serializers.CharField(default='', allow_null=True)
    is_active = serializers.BooleanField(default=True)
    position = serializers.IntegerField(default=0, allow_null=True)
    languages = serializers.ListField(allow_null=True, default=[])
    department_id = serializers.IntegerField(allow_null=True, default=0)


class IziSizeCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=True,
                                 default='', allow_blank=True)
    code = serializers.CharField(default='', allow_null=True)
    is_active = serializers.BooleanField(default=True)
    languages = serializers.ListField(allow_null=True, default=[])
    size_id = serializers.IntegerField(allow_null=True, default=0)


class IziContentCreateSerializer(serializers.Serializer):
    name = serializers.CharField(allow_null=True,
                                 default='', allow_blank=True)
    code = serializers.CharField(default='', allow_null=True)
    is_active = serializers.BooleanField(default=True)
    languages = serializers.ListField(allow_null=True, default=[])
