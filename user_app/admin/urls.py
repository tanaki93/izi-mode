from django.conf.urls import url
from django.urls import path
from user_app.admin import views

urlpatterns = [
    path('document/brands/', views.DocumentBrandListAPIView.as_view()),
    path('document/users/', views.AdminUserListCreateAPIView.as_view()),
    path('document/users/<int:user_id>/', views.AdminUserInfoAPIView.as_view()),
    path('document/list/', views.AdminDocumentListAPIView.as_view()),
    path('documents/<int:document_id>/process/', views.AdminDocumentGetProcessAPIView.as_view()),
    path('documents/<int:document_id>/process/products/', views.AdminDocumentProductsAPIView.as_view()),
    path('document/<int:document_id>/', views.AdminDocumentPatchAPIView.as_view()),
    path('countries/', views.AdminCountriesAPIView.as_view()),
    path('countries/<int:country_id>/', views.AdminCountryDetailAPIView.as_view()),
    path('currencies/', views.AdminCurrenciesAPIView.as_view()),
    path('currencies/<int:currency_id>/', views.AdminCurrencyDetailAPIView.as_view()),
    path('cities/', views.AdminCitiesAPIView.as_view()),
    path('cities/<int:city_id>/', views.AdminCityDetailAPIView.as_view()),
    path('points/', views.AdminPointsAPIView.as_view()),
    path('points/<int:point_id>/', views.AdminPointDetailAPIView.as_view()),
    path('languages/', views.AdminLanguagesAPIView.as_view()),
    path('languages/<int:language_id>/', views.AdminLanguageDetailAPIView.as_view()),
    path('exchanges/', views.AdminExchangesAPIView.as_view()),
    path('exchanges/<int:exchange_id>/', views.AdminExchangeDetailAPIView.as_view()),
    path('profiles/', views.AdminProfilesAPIView.as_view()),
    path('refunds/', views.AdminRefundsAPIView.as_view()),
    path('refunds/<int:refund_id>/', views.AdminRefundDetailAPIView.as_view()),

]
