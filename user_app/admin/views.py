from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListCreateAPIView, UpdateAPIView, \
    GenericAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from izi_mode.pagination import CustomPagination
from product_app.models import Brand, Document
from product_app.services import BrandService, DocumentService, ProductService, CountryService, CurrencyService, \
    CityService, PointService, LanguageService, ExchangeService
from projects_app.models import RefundOrder, RefundOrderItem
from user_app.admin.serializers import DocumentsSerializer, ProductSerializer, CountrySerializer, \
    CountryCreateSerializer, CurrencySerializer, CurrencyCreateSerializer, CitySerializer, CityCreateSerializer, \
    DeliveryPointSerializer, PointCreateSerializer, LanguageSerializer, LanguageCreateSerializer, \
    ExchangeRateSerializer, ExchangeCreateSerializer, RefundOrderListSerializer, BrandAdminDetailedSerializer
from user_app.models import UNDEFINED_TYPE, CLIENT, User
from user_app.permissions import IsAdmin
from user_app.serializers import UserSerializer
from user_app.services import UserService


class DocumentBrandListAPIView(APIView):
    """
    List API to show documents with infographics
    """
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = BrandAdminDetailedSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(Brand.objects.all(), many=True).data,
                        status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        response = BrandService.create_document(Brand.objects.get(id=request.data.get('brand_id')))
        return response


class AdminUserListCreateAPIView(APIView):
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        user_type = int(request.query_params.get('role', UNDEFINED_TYPE))
        if user_type == UNDEFINED_TYPE:
            data = UserService.get_all_users()
        else:
            data = UserService.get_users(user_type=user_type)
        return Response(self.serializer_class(data, many=True).data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        user = UserService.create(username=request.data['username'],
                                  email=request.data.get('email', ''),
                                  password=request.data['password'],
                                  phone=request.data.get('phone'),
                                  user_type=int(request.data.get('user_type', CLIENT))
                                  )
        user.save()
        return Response(
            self.serializer_class(user, many=False).data, status=status.HTTP_201_CREATED
        )


class AdminUserInfoAPIView(APIView):
    """
    API returns user details
    """
    serializer_class = UserSerializer
    permission_classes = ()

    def get(self, request, user_id):
        user = UserService.get(pk=user_id)

        return Response(self.serializer_class(user).data)

    def put(self, request, user_id):
        user = User.objects.get(id=user_id)
        user.email = request.data.get('email', '')
        user.user_type = int(request.data.get('user_type', 1))
        user.phone = request.data.get('phone', '')
        user.save()
        return Response(data=self.serializer_class(user).data, status=status.HTTP_200_OK)

    def delete(self, request, user_id):
        user = UserService.get(id=user_id)
        user.delete()
        return Response(status=status.HTTP_200_OK)


class AdminDocumentListAPIView(APIView):
    """
    List API to show documents with filters
    """
    permission_classes = ()
    authentication_classes = ()
    serializer_class = DocumentsSerializer

    def get(self, request, *args, **kwargs):
        return Response(data=self.serializer_class(DocumentService.get_admin_documents(request.query_params),
                                                   many=True).data, status=status.HTTP_200_OK)


class AdminDocumentPatchAPIView(UpdateAPIView):
    """
    Update API to change activity field
    """
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = DocumentsSerializer

    def put(self, request, *args, **kwargs):
        document = DocumentService.get(pk=kwargs['document_id'])
        document.is_new = request.data.get('is_new', False)
        document.save()
        return Response(self.serializer_class(document).data, status=status.HTTP_200_OK)


class AdminDocumentGetProcessAPIView(APIView):
    """
    GET, PUT, POST API -  processes with document
    """
    permission_classes = ()
    authentication_classes = ()
    serializer_class = DocumentsSerializer

    def get(self, request, *args, **kwargs):
        data = DocumentService.get_document_data(pk=kwargs['document_id'])
        return Response(data, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        data = DocumentService.set_comment(pk=kwargs['document_id'], comment=request.data.get('comment', ''))
        return Response(data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = Document.objects.get(pk=kwargs['document_id'])
        data.set_to_last_step()
        return Response(self.serializer_class(data).data, status=status.HTTP_200_OK)


class AdminDocumentProductsAPIView(GenericAPIView):
    """
    GET, PUT, POST API -  processes with document
    """
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = ProductSerializer
    pagination_class = CustomPagination

    def get(self, request, *args, **kwargs):
        document = DocumentService.get(id=kwargs['document_id'])
        queryset = self.filter_queryset(ProductService.get_admin_products(request.query_params, document))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            result = self.get_paginated_response(serializer.data)
            data = result.data  # pagination data
        else:
            serializer = self.get_serializer(queryset, many=True)
            data = serializer.data
        cont = {
            'step': document.step,
            'count': data['count'],
            'pages': data['pages'],
            'products': data['results'], }
        return Response(data=cont, status=status.HTTP_200_OK)


class AdminCountriesAPIView(ListCreateAPIView):
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = CountrySerializer

    def get_queryset(self):
        return CountryService.get_all_countries()

    def create(self, request, *args, **kwargs):
        serializer = CountryCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)

        country = CountryService.create(**serializer.validated_data)

        return Response(self.serializer_class(country, many=False).data, status=status.HTTP_201_CREATED)


class AdminCountryDetailAPIView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAdmin,)
    authentication_classes = (TokenAuthentication,)
    serializer_class = CountrySerializer

    def get(self, request, *args, **kwargs):
        return Response(
            self.serializer_class(CountryService.get(id=kwargs['country_id'])).data,
            status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = CountryCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        print(serializer.validated_data)
        serializer.validated_data['country_id'] = kwargs['country_id']
        country = CountryService.update(**serializer.validated_data)

        return Response(self.serializer_class(country, many=False).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        country = CountryService.get(id=kwargs['country_id'])
        country.delete()
        return Response(status=status.HTTP_200_OK)


class AdminCurrenciesAPIView(ListCreateAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = CurrencySerializer

    def get_queryset(self):
        return CurrencyService.get_all_currencies()

    def create(self, request, *args, **kwargs):
        serializer = CurrencyCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)

        currency = CurrencyService.create(**serializer.validated_data)

        return Response(self.serializer_class(currency, many=False).data, status=status.HTTP_201_CREATED)


class AdminCurrencyDetailAPIView(RetrieveUpdateDestroyAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = CurrencySerializer

    def get(self, request, *args, **kwargs):
        print(kwargs)
        return Response(
            self.serializer_class(CurrencyService.get(id=kwargs['currency_id'])).data,
            status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = CurrencyCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['currency_id'] = kwargs['currency_id']
        country = CurrencyService.update(**serializer.validated_data)

        return Response(self.serializer_class(country, many=False).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        currency = CurrencyService.get(id=kwargs['currency_id'])
        currency.delete()
        return Response(status=status.HTTP_200_OK)


class AdminCitiesAPIView(ListCreateAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = CitySerializer

    def get_queryset(self):
        return CityService.get_all_cities()

    def create(self, request, *args, **kwargs):
        serializer = CityCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)

        city = CityService.create(**serializer.validated_data)

        return Response(self.serializer_class(city, many=False).data, status=status.HTTP_201_CREATED)


class AdminCityDetailAPIView(RetrieveUpdateDestroyAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = CitySerializer

    def get(self, request, *args, **kwargs):
        return Response(
            self.serializer_class(CityService.get(id=kwargs['city_id'])).data,
            status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = CityCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['city_id'] = kwargs['city_id']
        city = CityService.update(**serializer.validated_data)
        return Response(self.serializer_class(city, many=False).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        city = CityService.get(id=kwargs['city_id'])
        city.delete()
        return Response(status=status.HTTP_200_OK)


class AdminPointsAPIView(ListCreateAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = DeliveryPointSerializer

    def get_queryset(self):
        return PointService.get_all_points()

    def create(self, request, *args, **kwargs):
        serializer = PointCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)

        point = PointService.create(**serializer.validated_data)

        return Response(self.serializer_class(point, many=False).data, status=status.HTTP_201_CREATED)


class AdminPointDetailAPIView(RetrieveUpdateDestroyAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = DeliveryPointSerializer

    def get(self, request, *args, **kwargs):
        return Response(
            self.serializer_class(PointService.get(id=kwargs['point_id'])).data,
            status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = PointCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['point_id'] = kwargs['point_id']
        point = PointService.update(**serializer.validated_data)
        return Response(self.serializer_class(point, many=False).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        point = PointService.get(id=kwargs['point_id'])
        point.delete()
        return Response(status=status.HTTP_200_OK)


class AdminLanguagesAPIView(ListCreateAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = LanguageSerializer

    def get_queryset(self):
        return LanguageService.get_all_languages()

    def create(self, request, *args, **kwargs):
        serializer = LanguageCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)

        language = LanguageService.create(**serializer.validated_data)

        return Response(self.serializer_class(language, many=False).data, status=status.HTTP_201_CREATED)


class AdminLanguageDetailAPIView(RetrieveUpdateDestroyAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = LanguageSerializer

    def get(self, request, *args, **kwargs):
        return Response(
            self.serializer_class(LanguageService.get(id=kwargs['language_id'])).data,
            status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = LanguageCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['language_id'] = kwargs['language_id']
        language = LanguageService.update(**serializer.validated_data)

        return Response(self.serializer_class(language, many=False).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        language = LanguageService.get(id=kwargs['language_id'])
        language.delete()
        return Response(status=status.HTTP_200_OK)


class AdminExchangesAPIView(ListCreateAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = ExchangeRateSerializer

    def get_queryset(self):
        return ExchangeService.get_all_exchanges()

    def create(self, request, *args, **kwargs):
        serializer = ExchangeCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)

        exchange = ExchangeService.create(**serializer.validated_data)

        return Response(self.serializer_class(exchange, many=False).data, status=status.HTTP_201_CREATED)


class AdminExchangeDetailAPIView(RetrieveUpdateDestroyAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = ExchangeRateSerializer

    def get(self, request, *args, **kwargs):
        return Response(
            self.serializer_class(ExchangeService.get(id=kwargs['exchange_id'])).data,
            status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = ExchangeCreateSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(data={
                'message': 'Invalid input',
                'errors': serializer.errors
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        serializer.validated_data['exchange_id'] = kwargs['exchange_id']
        exchange = ExchangeService.update(**serializer.validated_data)

        return Response(self.serializer_class(exchange, many=False).data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        exchange = LanguageService.get(id=kwargs['exchange_id'])
        exchange.delete()
        return Response(status=status.HTTP_200_OK)


class AdminProfilesAPIView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def get(self, request, *args, **kwargs):
        users = User.objects.filter(user_type=3)
        data = []
        for i in users:
            context = {
                'user_id': i.id,
                'profile': UserSerializer(i).data,
            }
            data.append(context)
        return Response(status=status.HTTP_200_OK, data=data)


class AdminRefundsAPIView(APIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = RefundOrderListSerializer

    def get(self, request, *args, **kwargs):
        refunds = RefundOrder.objects.all()
        return Response(status=status.HTTP_200_OK, data=self.serializer_class(refunds, many=True).data)

    def post(self, request, *args, **kwargs):
        order_id = int(request.data.get('order_id'))
        refund = RefundOrder.objects.create(order_id=order_id, price=float(request.data.get('price')), refund_type=1)
        refund.save()
        for i in request.data.get('order_items'):
            try:
                item = RefundOrderItem.objects.create(order_item_id=int(i), refund=refund)
                item.save()
            except:
                pass
        return Response(status=status.HTTP_200_OK)


class AdminRefundDetailAPIView(APIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = RefundOrderListSerializer

    def get(self, request, *args, **kwargs):
        refunds = RefundOrder.objects.get(id=kwargs['refund_id'])
        return Response(status=status.HTTP_200_OK, data=self.serializer_class(refunds, many=False).data)

    def post(self, request, *args, **kwargs):
        item_id = None
        try:
            item_id = int(request.data.get('item_id', ''))
        except:
            pass
        if item_id is not None:
            refund_item = RefundOrderItem.objects.get(id=item_id, refund_id=kwargs['refund_id'])
            comment = request.data.get('comment', '')
            status_data = int(request.data.get('status'))
            refund_item.status = status_data
            if comment != '':
                refund_item.comment = comment
            refund_item.save()
        return Response(status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        refund = RefundOrder.objects.get(id=kwargs['refund_id'])
        refund.status = int(request.data.get('status'))
        refund.save()
        return Response(status=status.HTTP_200_OK)
