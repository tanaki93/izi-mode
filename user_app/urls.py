from django.conf.urls import url
from django.urls import path, include

from .views import *

urlpatterns = [
    path('login/', LoginView.as_view(), name='login_view'),
    path('register/', RegisterView.as_view(), name='register_view'),
    path('confirm/', ConfirmView.as_view(), name='confirm_view'),
    path('reauth/', ReAuthView.as_view(), name='reauth_view'),
    path('resend/code/', ResendCodeView.as_view(), name='resend_code_view'),
    path('email/', ResetPasswordView.as_view(), name='reset_password_view'),
    path('reset/password/', ChangePasswordView.as_view(), name='change_password_view'),
    path('add_profile/', AddUserView.as_view(), name='add_user_view'),
]
