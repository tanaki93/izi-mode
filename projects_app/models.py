from django.db import models
from django.db.models import SET_NULL, CASCADE

from product_app.models import VendSize, OriginalProduct, Size, DeliveryPoint, City
from user_app.models import User

PAYMENT_STATUSES = (
    (-1, 'WAIT'),
    (1, 'PAID'),
    (0, 'NOT PAID'),
)

PAYMENT_TYPE = (
    (1, 'DEMIRBANK'),
    (2, 'PAYBOX'),
    (3, 'PAY24'),
)

ORDER_PROCESS_STATUSES = (
    (0, 'WAITING'),
    (1, 'IN PROCESS'),
    (2, 'DONE'),
    (-1, 'CANCELED'),
)

PROCESS_STATUSES = (
    (1, 'WAITING'),
    (2, 'IN PROCESS'),
    (3, 'DONE'),
    (4, 'REFUND'),
    (-1, 'CANCELED'),
)

RECEIVING_STATUSES = (
    (-1, 'NOT RECEIVED'),
    (1, 'RECEIVED'),
)

CHECKING_STATUSES = (
    (1, 'GOOD'),
    (0, 'WAIT'),
    (-1, 'DEFECT'),
    (-2, 'ERROR'),
)

DELIVERY_STATUSES = (
    (1, 'WAITING'),
    (2, 'SEND'),
    (-1, 'SENT BACK'),
)

SHIPPING_STATUSES = (
    (1, 'WAITING'),
    (2, 'SENT'),
    (3, 'RECEIVED'),
)

PACKAGE_STATUSES = (
    (1, 'ORDERED'),
    (2, 'RECEIVED'),
)

PACKET_STATUSES = (
    (1, 'WAITING'),
    (2, 'RECEIVED'),
)


class Order(models.Model):
    class Meta:
        ordering = '-payment_status date'.split()
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    PAYMENT_CHOICES = (
        (1, 'DEMIRBANK'),
        (2, 'PAYBOX'),
    )
    date = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    order_hash = models.CharField(max_length=1000, null=True, unique=True)
    payment_status = models.IntegerField(choices=PAYMENT_STATUSES, default=-1, null=True, blank=True)
    process_status = models.IntegerField(choices=PROCESS_STATUSES, default=1, null=True, blank=True)
    email = models.EmailField(null=True, blank=True, max_length=100)
    phone = models.CharField(max_length=100, null=True, blank=True)
    deliver_city = models.ForeignKey(City, null=True, blank=True, on_delete=models.SET_NULL)
    point = models.ForeignKey(DeliveryPoint, null=True, blank=True, on_delete=models.SET_NULL)
    delivery_type = models.IntegerField(default=1, null=True, blank=True)
    payment_type = models.IntegerField(default=2, null=True, blank=True, choices=PAYMENT_CHOICES)
    property_type = models.IntegerField(default=1, null=True, blank=True)
    address = models.CharField(max_length=1000, null=True, blank=True)
    address_name = models.CharField(max_length=100, null=True, blank=True, default='')
    products_price = models.FloatField(null=True)
    total_price = models.FloatField(null=True)
    shipping_price = models.FloatField(null=True)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    client = models.ForeignKey(User, null=True, related_name='client', blank=True, on_delete=models.SET_NULL)


class OrderPackage(models.Model):
    number = models.CharField(max_length=100)
    status = models.IntegerField(choices=PACKAGE_STATUSES, default=1)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)


class OrderItem(models.Model):
    class Meta:
        verbose_name_plural = 'Товары'
        verbose_name = 'Товар'

    order = models.ForeignKey(Order, null=True, on_delete=models.SET_NULL)
    size = models.ForeignKey(Size, null=True, on_delete=models.SET_NULL)
    product = models.ForeignKey(OriginalProduct, null=True, on_delete=models.SET_NULL)
    price = models.FloatField(null=True)
    shipping_price = models.FloatField(null=True)
    stage = models.CharField(null=True, max_length=255, blank=True)
    package = models.ForeignKey(OrderPackage, null=True, blank=True, on_delete=models.SET_NULL)
    amount = models.IntegerField(default=1)
    product_status = models.IntegerField(default=1, choices=PROCESS_STATUSES, null=True)
    checking_status = models.IntegerField(default=0, choices=CHECKING_STATUSES, null=True)
    delivery_status = models.IntegerField(default=1, choices=DELIVERY_STATUSES, null=True)
    receive_date = models.DateTimeField(null=True, blank=True)
    send_date = models.DateTimeField(null=True, blank=True)
    delivery_date = models.DateTimeField(null=True, blank=True)
    logistic_receive_status = models.IntegerField(default=1, choices=PACKET_STATUSES, null=True)
    logistic_deliver_status = models.IntegerField(default=1, choices=SHIPPING_STATUSES, null=True)
    shipping_service = models.TextField(null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)


class OrderItemComment(models.Model):
    order_item = models.ForeignKey(OrderItem, null=True, on_delete=models.SET_NULL)
    comment = models.TextField(null=True)


def file_upload_to(instance, filename):
    return "%s" % filename


class CommentImage(models.Model):
    image = models.ImageField(upload_to=file_upload_to, null=True)
    comment = models.ForeignKey(OrderItemComment, null=True, on_delete=models.SET_NULL)


class Flight(models.Model):
    number = models.CharField(max_length=100, null=True)


class OrderPacket(models.Model):
    class Meta:
        ordering = 'updated'.split()

    weight = models.IntegerField(null=True)
    money = models.IntegerField(null=True, default=0)
    flight = models.ForeignKey(Flight, null=True, on_delete=SET_NULL)
    status = models.IntegerField(null=True, choices=PACKET_STATUSES, default=1)
    received_status = models.IntegerField(null=True, choices=RECEIVING_STATUSES, default=-1)
    updated = models.DateTimeField(auto_now=True, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)


class PacketProduct(models.Model):
    order_item = models.ForeignKey(OrderItem, null=True, on_delete=SET_NULL)
    order_packet = models.ForeignKey(OrderPacket, null=True, on_delete=CASCADE)


class Transaction(models.Model):
    class Meta:
        verbose_name = 'Транзакцию'
        verbose_name_plural = 'Транзакции'

    pan = models.CharField(max_length=100, null=True, default='', blank=True)
    pg_payment_system = models.CharField(max_length=100, null=True, default='', blank=True)
    order = models.ForeignKey(Order, null=True, related_name='transactions', on_delete=models.SET_NULL)
    amount = models.FloatField(null=True)
    status = models.IntegerField(default=0, null=True)
    trans_id = models.CharField(max_length=1000, null=True)
    auth_code = models.CharField(max_length=1000, null=True, blank=True)
    card_brand = models.CharField(max_length=1000, null=True, default='', blank=True)
    updated = models.DateTimeField(auto_now=True, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return str(self.id)


class TelegramChat(models.Model):
    chat = models.IntegerField(null=True)


class Favourite(models.Model):
    product = models.ForeignKey(OriginalProduct, null=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    updated = models.DateTimeField(auto_now=True, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)


class RefundOrder(models.Model):
    order = models.OneToOneField(Order, on_delete=models.SET_NULL, null=True, blank=True)
    updated = models.DateTimeField(auto_now=True, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    price = models.FloatField(null=True)
    status = models.IntegerField(default=0)
    refund_type = models.IntegerField(default=1)


class RefundOrderItem(models.Model):
    refund = models.ForeignKey(RefundOrder, related_name='items', on_delete=models.SET_NULL, null=True, blank=True)
    order_item = models.ForeignKey(OrderItem, on_delete=models.SET_NULL, null=True, blank=True)
    status = models.IntegerField(default=0)
    details = models.TextField(null=True)
    comment = models.TextField(null=True, default='')


class RefundOrderItemImage(models.Model):
    image = models.ImageField(upload_to=file_upload_to, null=True)
    refund_order_item = models.ForeignKey(RefundOrderItem, related_name='item_images', null=True,
                                          on_delete=models.SET_NULL)


class ImageUploader(models.Model):
    image = models.ImageField(upload_to=file_upload_to, null=True)
