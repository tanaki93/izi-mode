#!/usr/bin/env python
# -*- coding: utf-8
import json
import random
import time
from datetime import datetime
from multiprocessing.dummy import Pool
from pprint import pprint
from random import choice

import requests
from bs4 import BeautifulSoup

from projects_app.update_parser.constants import USERAGENTS


def get_html(url):
    try:
        print(url)
        useragent = {'User-Agent': choice(USERAGENTS)}
        r = requests.get(url, headers=useragent, timeout=3)
        return r.text, r.status_code
    except:
        return '', 403


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_zara_data(context):
    product = {

    }
    try:
        html, status = get_html(context['url'])
        soup = BeautifulSoup(html, 'lxml')

        main_data = soup.find('div', id='page-container').find('section', id='main').find('section', id='product')
        script = main_data.find('script').contents[0]
        script = json.loads(script, strict=False)
        images_data = main_data.find('div', class_='product-images-section _product-images-section') \
            .find('div', class_='big-image-container _big-image-container').find('div', id='main-images').find_all(
            'div',
            class_='media-wrap')
        images = []
        form = main_data.find('form')
        sizes = []
        colour = main_data.find('div', class_='info-section').find('div',
                                                                   class_='product-info-wrapper _product-info').find(
            'p').find('span', class_='_colorName').text
        for i in (form.find('div', class_='size-select _size-select').find_all('label', class_='product-size')):
            input = i.find('div', class_='aria-size-input').find('input')
            size = {
                'value': input['value']
            }
            disabled = None
            try:
                disabled = input['disabled']
            except:
                pass
            if disabled is None:
                size['stock'] = True
            else:
                size['stock'] = False
            sizes.append(size)
        info = script[0]
        for i in images_data:
            images.append(i.find('a', class_='_seoImg main-image')['href'])
        product = {'images': images, 'selling_price': float(info['offers']['price']),
                   'discount_price': float(info['offers']['price']), 'original_price': float(info['offers']['price']),
                   'name': info['name'], 'description': info['description'], 'sizes': sizes,
                   'colour': colour.lower().capitalize()}

    except:
        pass
    cont = {
        'id': context['id'],
        'product': product
    }
    print(cont)
    return cont
