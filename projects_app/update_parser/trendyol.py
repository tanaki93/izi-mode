import json
from random import choice

import requests
from bs4 import BeautifulSoup

from projects_app.update_parser.constants import USERAGENTS


def get_html(url):
    try:
        useragent = {'User-Agent': choice(USERAGENTS)}
        # print(url)
        r = requests.get(url, headers=useragent, timeout=3)
        return r.text, r.status_code
    except:
        return '', 404


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_trendyol_data(context):
    product = None
    cont = {
        'id': context['id'],
        'product': product,
    }
    text = context['url']
    print(text)
    html, status = get_html(text)
    soup = BeautifulSoup(html, 'lxml')
    container = soup.find('div', id='container')
    for i in range(1, 6):
        data = None
        try:
            select = container.select('script')[i]
            href = select.text.split(
                'window.__PRODUCT_DETAIL_APP_INITIAL_STATE__ = ')[
                -1].strip()
            data = (json.loads(href[:-1]))
        except:
            pass
        if data is not None:
            product = data['product']
            break
    if status == 404:
        product = {}
    cont['status'] = status
    cont['product'] = product
    return cont
