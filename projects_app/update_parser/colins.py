#!/usr/bin/env python
# -*- coding: utf-8
import json
import random
import time
from datetime import datetime
from multiprocessing.dummy import Pool
from pprint import pprint
from random import choice

import requests
from bs4 import BeautifulSoup

from projects_app.update_parser.constants import USERAGENTS


def get_html(url):
    try:
        print(url)
        useragent = {'User-Agent': choice(USERAGENTS)}
        r = requests.get(url, headers=useragent, timeout=3)
        return r.text, r.status_code
    except:
        return '', 403


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_colins_data(context):
    cont = {
        'id': context['id']
    }
    product = None
    try:
        html, status = get_html(context['url'])
        product = {}
        soup = BeautifulSoup(html, 'lxml')
        container = soup.find('div', class_='main-site-container')
        base = container.find('div', class_='main-site-body-container') \
            .find('div', class_='prduct-detail-container container mb-4 catalog catalog-product') \
            .find('form')
        images_soup = base.find('div', class_='row product-details-page') \
            .find_all('div', class_='col-12 col-lg-6')[0] \
            .find('div', class_='product-detail-left') \
            .find('div', class_='slider-container') \
            .find('div', class_='row') \
            .find('div', class_='col-lg-12') \
            .find('div', class_='row slider-row') \
            .find('div', class_='col-lg-12') \
            .find('div', class_='product-slider-navbar mt-2') \
            .find_all('div')
        description = ''
        images = []
        for i in images_soup:
            images.append(i.find('img')['src'])
        right = base.find('div', class_='row product-details-page') \
            .find_all('div', class_='col-12 col-lg-6')[1].find('div', class_='product-detail-right')
        name_soup = right.find('h1')
        name = name_soup.text.strip()
        others = right.find('div', class_='product-variants-container')
        product['stock'] = False
        color = others.find('div',
                            class_='product-variant-line-container')['data-variant-name'].lower().capitalize()
        price = others.find('div',
                            class_='product-detail-product-prices-container mb-3') \
            .find('div', class_='row') \
            .find('div', class_='col-12 col-md-12 d-flex d-lg-block justify-content-center') \
            .find('div', class_='product-detail-price float-left mr-md-3') \
            .text.strip().split()[0]
        variants = others.find('div', class_='product-detail-size-container') \
            .find('div', class_='row') \
            .find('div', class_='col-12 col-lg-4 product-detail-size-select mb-2') \
            .find('select') \
            .find_all('option')
        sizes = []
        for variant in variants:
            if variant.text.strip() != 'seçiniz':
                size = {'value': variant.text.strip()}
                try:
                    x = (variant['disabled'])
                    size['stock'] = False
                except:
                    size['stock'] = True
                    product['stock'] = True
                sizes.append(size)

        product['description'] = description
        product['name'] = name
        product['colour'] = color
        product['price'] = float(price.replace(',', '.'))
        product['sizes'] = sizes
        product['images'] = images
    except:
        pass
    cont['product'] = product
    print(cont)
    return cont
