#!/usr/bin/env python
# -*- coding: utf-8
import json
import random
import time
from datetime import datetime
from multiprocessing.dummy import Pool
from pprint import pprint
from random import choice

import requests
from bs4 import BeautifulSoup


def get_html(url):
    try:
        print(url)
        r = requests.get(url, timeout=2)
        return r.text, r.status_code
    except:
        pass
    return '', 403


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_strad_data(context):
    status_code = 200
    product_dict = {
        'id': context['id']
    }
    cont = {}
    html = '{}'
    json_data = None
    error_text = ''
    try:
        url = context['url']
        category_id = url.split('category_id=')[1].split('&')[0]
        colour_id = url.split('colorId=')[1].split('&category_id=')[0]
        product_id = url.split('%s' % category_id)[1].split('.html')[0][1:]
        product_json_url = 'https://www.stradivarius.com/itxrest/2/catalog/store/54009571/50331081/category/0/product/%s/detail?languageId=-43&appId=2' % product_id
        html, status_code = get_html(product_json_url)
        json_data = json.loads(html)
    except:
        pass
    try:
        soup = BeautifulSoup(html, 'lxml')
        error_text = soup.find('h1').text
    except:
        pass
    try:
        cont['name'] = json_data['name']
        cont['stock'] = True
        cont['product_id'] = product_id
        cont['product_code'] = product_id
        cont['description'] = json_data['detail']['longDescription']
        images = []
        try:
            static = 'https://static.e-stradivarius.net/5/photos3'
            medias = json_data['detail']['xmedia']

            for media in medias:
                if int(media['colorCode']) == int(colour_id):
                    path = static + media['path'] + '/'
                    for xmedia in media['xmediaLocations'][0]['locations'][1]['mediaLocations']:
                        image = path + xmedia + '1.jpg'
                        images.append(image)
                    break
        except:
            pass
        image_text = ''
        for image in images:
            image_text = image_text + image + ' '
        cont['images'] = image_text
        product_sizes = []
        for color in json_data['detail']['colors']:
            if color['id'] == str(colour_id):
                cont['colour'] = color['name']
                product_sizes = color['sizes']
                break
        stock_url = 'https://www.stradivarius.com/itxrest/2/catalog/store/54009571/50331081/product/%s/stock?languageId=-43&appId=2' % product_id
        stock_json = get_html(stock_url)
        stock_data = json.loads(stock_json)
        sizes = []
        price = 0
        for stock in stock_data['stocks']:
            if stock['productId'] == int(product_id):
                for product_stock in stock['stocks']:
                    for product_size in product_sizes:
                        if product_size['sku'] == product_stock['id']:
                            stock_bool = False
                            if product_stock['availability'] == 'in_stock':
                                stock_bool = True
                            else:
                                continue
                            size = {
                                "value": product_size['name'],
                                'stock': stock_bool
                            }
                            sizes.append(size)
                            if int(product_size['price']) / 100 > price:
                                price = int(product_size['price']) / 100
            break
        data_size = []
        for i in sizes:
            if i not in data_size:
                data_size.append(i)
        cont['sizes'] = data_size
        if len(data_size) == 0:
            cont['stock'] = False
        cont['selling_price'] = price
        cont['discount_price'] = price
        cont['original_price'] = price
        product_dict['product'] = cont
    except:
        pass
    if error_text == 'Operation not allowed.':
        product_dict['id'] = 0
        status_code = 429
    if status_code == 403:
        product_dict['id'] = 0
    product_dict['product'] = cont
    product_dict['status_code'] = status_code
    pprint(product_dict)
    return product_dict


PAGES = 20
headers = {'Content-type': 'application/json', 'Accept': 'application/json'}


def main():
    url = 'https://magicbox.izishop.kg/api/v1/project/update/links/?brand=STRD&active=1'
    links = get_categories_from_db(url)
    length = (len(links))
    print(length, datetime.now())
    for i in links:
        product = get_strad_data(i)
        if product['status_code'] == 429:
            time.sleep(300)
        else:
            r = requests.post(url,
                              data=json.dumps([product]), headers=headers)
            print(r.status_code)


if __name__ == '__main__':
    main()
