from projects_app.update_parser.bershka import get_bershka_data
from projects_app.update_parser.colins import get_colins_data
from projects_app.update_parser.handm import get_handm_data
from projects_app.update_parser.massimo import get_massimo_data
from projects_app.update_parser.pull import get_pull_data
from projects_app.update_parser.stradivarius import get_strad_data
from projects_app.update_parser.trendyol import get_trendyol_data
from projects_app.update_parser.zara import get_zara_data
from projects_app.views import update_trendyol_original_product, update_massimo_product, update_collins_product, \
    update_zara_product

trendyol_codes = ['AYYLD', 'BMBI', 'DLVN', 'EKA', 'PRCRDN',
                  'EKOL', 'IPKL', 'IRONI', 'JJ', 'KOM', 'KTN', 'MRJN',
                  'MIORRE', 'MVLC', 'NSRDN', 'PNT', 'STN',
                  'SETRE', 'SLZNG', 'TAS', 'USPL']


def parse_products(product):
    code = product.brand.code
    if code == 'HANDM':
        data = get_handm_data({'id': product.link.id, 'url': product.link.url})
        update_massimo_product(product, data['product'])
    elif code == 'COLNS':
        data = get_colins_data({'id': product.link.id, 'url': product.link.url})
        update_collins_product(product, data['product'])
    elif code == 'BRSHK':
        data = get_bershka_data({'id': product.link.id, 'url': product.link.url})
        update_massimo_product(product, data['product'])
    elif code == 'MDT':
        data = get_massimo_data({'id': product.link.id, 'url': product.link.url})
        update_massimo_product(product, data['product'])
    elif code == 'PLLBR':
        data = get_pull_data({'id': product.link.id, 'url': product.link.url})
        update_massimo_product(product, data['product'])
    elif code == 'STRD':
        data = get_strad_data({'id': product.link.id, 'url': product.link.url})
        update_massimo_product(product, data['product'])
    elif code == 'ZARA':
        data = get_zara_data({'id': product.link.id, 'url': product.link.url})
        update_zara_product(product, data['product'])
    elif code in trendyol_codes:
        data = get_trendyol_data({'id': product.link.id, 'url': product.link.url})
        if data['product'] is not None:
            update_trendyol_original_product(product, data['product'])
    return product