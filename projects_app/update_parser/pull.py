#!/usr/bin/env python
# -*- coding: utf-8
import json
import random
import time
from datetime import datetime
from multiprocessing.dummy import Pool
from pprint import pprint
from random import choice

import requests
from bs4 import BeautifulSoup

from projects_app.update_parser.constants import USERAGENTS


def get_html(url):
    try:
        print(url)
        useragent = {'User-Agent': choice(USERAGENTS)}
        r = requests.get(url, headers=useragent, timeout=2)
        return r.text, r.status_code
    except:
        pass
    return '', 403


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_pull_data(context):
    status_code = 200
    product_dict = {
        'id': context['id']
    }
    cont = {}
    html = '{}'
    json_data = None
    error_text = ''
    try:
        url = context['url']
        product_id = url.split('-c')[-1].split('p')[-1].split('.html')[0]
        colour_id = url.split('.html?cS=')[-1]
        category_id = url.split('-c')[-1].split('p')[0]
        product_json_url = 'https://www.pullandbear.com/itxrest/2/catalog/store/25009521/20309411/category/0/product/%s/detail?languageId=-43&appId=1' % product_id
        html, status_code = get_html(product_json_url)
        json_data = json.loads(html)
    except:
        pass
    try:
        soup = BeautifulSoup(html, 'lxml')
        error_text = soup.find('h1').text
    except:
        pass

    cont['stock'] = False
    static = 'https://static.pullandbear.net/2/photos'
    medias = []
    try:
        medias = json_data['bundleProductSummaries'][0]['detail']['xmedia']
    except:
        pass
    images = []
    for media in medias:
        if media['colorCode'] == str(colour_id):
            path = static + media['path'] + '/'
            m = media['path'].split('/')[-3:]
            t = ''
            for n in m:
                t = t + n
            for xmedia in media['xmediaItems'][0]['medias']:
                split = xmedia['idMedia'].split('_')
                z = t
                for s in split[1:]:
                    z = z + '_' + s
                image = path + z + '8.jpg?t=' + str(
                    xmedia['timestamp'])
                images.append(image)
                z = ''
            break

    image_text = ''
    for image in images:
        image_text = image_text + image + ' '
    cont['images'] = image_text
    product_sizes = []
    bundle_colors = []
    try:
        bundle_colors = json_data['bundleProductSummaries'][0]['detail']['colors']
    except:
        pass
    for color in bundle_colors:
        if color['id'] == str(colour_id):
            cont['colour'] = color['name']
            product_sizes = color['sizes']
            break
    stock_url = 'https://www.pullandbear.com/itxrest/2/catalog/store/25009521/20309411/product/%s/stock?languageId=-43&appId=1' % product_id
    stock_json, status_code = get_html(stock_url)
    stock_data = {'stocks': []}
    try:
        stock_data = json.loads(stock_json)
    except:
        pass
    try:
        soup = BeautifulSoup(html, 'lxml')
        error_text = soup.find('h1').text
    except:
        pass
    sizes = []
    price = 0
    for stock in stock_data['stocks']:
        if stock['productId'] == int(product_id):
            for product_stock in stock['stocks']:
                for product_size in product_sizes:
                    if product_size['sku'] == product_stock['id']:
                        stock_bool = False
                        if product_stock['availability'] == 'in_stock':
                            stock_bool = True
                        size = {
                            "value": product_size['name'],
                            'stock': stock_bool
                        }
                        sizes.append(size)
                        if int(product_size['price']) / 100 > price:
                            price = int(product_size['price']) / 100
            break
    cont['sizes'] = sizes
    cont['selling_price'] = price
    cont['discount_price'] = price
    cont['original_price'] = price
    if error_text == 'Operation not allowed.':
        product_dict['id'] = 0
        status_code = 429
    if status_code == 403:
        product_dict['id'] = 0
    product_dict['product'] = cont
    product_dict['status_code'] = status_code
    pprint(product_dict)
    return product_dict
