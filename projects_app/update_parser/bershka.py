#!/usr/bin/env python
# -*- coding: utf-8
import json
import random
import time
from datetime import datetime
from multiprocessing.dummy import Pool
from pprint import pprint

from random import choice

import requests
from bs4 import BeautifulSoup

from projects_app.update_parser.constants import USERAGENTS


def get_html(url):
    try:
        print(url)
        useragent = {'User-Agent': choice(USERAGENTS)}
        r = requests.get(url, headers=useragent, timeout=2)
        return r.text, r.status_code
    except:
        pass
    return '', 403


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_bershka_data(context):
    product_dict = {
        'id': context['id'],
        'product': {}
    }
    cont = {}
    url = context['url']
    category_id = url.split('category_id=')[1].split('&')[0]
    colour_id = url.split('colorId=')[1].split('&category_id=')[0]
    product_id = url.split('%s' % category_id)[1].split('.html')[0][1:]
    product_json_url = 'https://www.bershka.com/itxrest/2/catalog/store/44109521/40259537/category/0/product/%s' \
                       '/detail?languageId=-43&appId=2' % product_id
    html, status = get_html(product_json_url)
    try:
        json_data = json.loads(html)
    except:
        product_dict['product'] = None
        return product_dict
    try:
        cont['name'] = json_data['name']
    except:
        if json_data['key'] == '_ERR_PRODUCT_NOT_FOUND':
            return product_dict
    cont['stock'] = True
    cont['product_id'] = product_id
    cont['product_code'] = product_id
    cont['description'] = json_data['detail']['longDescription']
    static = 'https://static.bershka.net/4/photos2'
    medias = json_data['detail']['xmedia']
    images = []
    for media in medias:
        if int(media['colorCode']) == int(colour_id):
            path = static + media['path'] + '/'
            for xmedia in media['xmediaLocations'][0]['locations'][0]['mediaLocations']:
                image = path + xmedia + '0.jpg'
                images.append(image)
            break
    image_text = ''
    for image in images:
        image_text = image_text + image + ' '
    cont['images'] = image_text
    product_sizes = []
    for color in json_data['detail']['colors']:
        if color['id'] == str(colour_id):
            cont['colour'] = color['name']
            product_sizes = color['sizes']
            break
    stock_url = 'https://www.bershka.com/itxrest/2/catalog/store/44109521/40259537/product/%s/stock?languageId=-43&appId=2' % product_id
    stock_json, status = get_html(stock_url)
    stock_data = json.loads(stock_json)
    sizes = []
    price = 0
    for stock in stock_data['stocks']:
        if stock['productId'] == int(product_id):
            for product_stock in stock['stocks']:
                for product_size in product_sizes:
                    if product_size['sku'] == product_stock['id']:
                        stock_bool = False
                        if product_stock['availability'] == 'in_stock':
                            stock_bool = True
                        else:
                            continue
                        size = {
                            "value": product_size['name'],
                            'stock': stock_bool
                        }
                        sizes.append(size)
                        if int(product_size['price']) / 100 > price:
                            price = int(product_size['price']) / 100
        break
    data_size = []
    for i in sizes:
        if i not in data_size:
            data_size.append(i)
    cont['sizes'] = data_size
    if len(data_size) == 0:
        cont['stock'] = False
    cont['selling_price'] = price
    cont['discount_price'] = price
    cont['original_price'] = price
    product_dict['product'] = cont
    print(product_dict)
    return product_dict
