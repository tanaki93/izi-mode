#!/usr/bin/env python
# -*- coding: utf-8
import json
import time
from datetime import datetime
from multiprocessing.dummy import Pool
from pprint import pprint
from random import choice

import requests
from bs4 import BeautifulSoup

from projects_app.update_parser.constants import USERAGENTS


def get_html(url):
    try:
        print(url)
        useragent = {'User-Agent': choice(USERAGENTS)}
        r = requests.get(url, headers=useragent, timeout=2)
        return r.text, r.status_code
    except:
        pass
    return '{}', 403


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_handm_data(context):
    cont = None
    product = {
        'id': context['id']
    }
    cont = {}
    url = context['url']
    html, status = get_html(url)
    if status == 200:
        soup = BeautifulSoup(html, 'lxml')
        scripts = json.loads(soup.find('script', type='application/ld+json').text)
        cont['colour'] = scripts['color']
        cont['description'] = scripts['description']
        cont['name'] = scripts['name']
        cont['product_code'] = scripts['sku']
        cont['selling_price'] = float(scripts['offers'][0]['price'])
        cont['original_price'] = float(scripts['offers'][0]['price'])
        cont['images'] = scripts['image']
        if scripts['offers'][0]['availability'] == 'http://schema.org/InStock':
            stock = True
        else:
            stock = False
        cont['stock'] = stock
        sizes = []
        li = soup.find('main').find_all('div')[1]
        scripts = li.find_all('script')[0]
        data = (scripts.text.replace('\r', '').replace('\n', '').replace('\t', '').replace(' ', '').strip().split(
            '\'sizes\''))

        data = data[1:]
        for i in data[:-1]:
            text = i[1:]
            index = text.find(']')
            try:
                js = json.loads(text[0:index + 1].replace('\'', '\"'))
                for j in js:
                    if cont['product_code'] + j['size'] == j['sizeCode']:
                        size = {'value': j['name'], 'stock': False, 'code': j['sizeCode']}
                        sizes.append(size)
            except:
                pass
        service = 'https://www2.hm.com/hmwebservices/service/product/tr/availability/%s.json' % cont['product_code'][
                                                                                                0:-3]
        h, status = get_html(service)
        try:
            n = BeautifulSoup(h, 'lxml')
            size_data = (json.loads(n.text))
            new_sizes = []
            for i in size_data['availability']:
                for j in sizes:
                    if j['code'] == i:
                        new_sizes.append(i)
                        break
            available = []
            count = len(new_sizes)
            for i in sizes:
                flag = 0
                for j in new_sizes:
                    if i['code'] == j:
                        continue
                    else:
                        flag += 1
                if flag != count and i not in available:
                    available.append({'value': i['value'], 'stock': True, 'code': i['code']})
                else:
                    available.append({'value': i['value'], 'stock': False, 'code': i['code']})
            cont['sizes'] = available
            product['product'] = cont
            product['status_code'] = 200
        except:
            product['product'] = None
            product['status_code'] = 403
    elif status == 403:
        product['product'] = None
        product['status_code'] = 403
    elif status == 404:
        product['product'] = {}
        product['status_code'] = 404
    return product
