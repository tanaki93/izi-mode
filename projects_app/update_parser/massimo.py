#!/usr/bin/env python
# -*- coding: utf-8
import json
import random
import time
from datetime import datetime
from multiprocessing.dummy import Pool
from pprint import pprint
from random import choice

import requests
from bs4 import BeautifulSoup

from projects_app.update_parser.constants import USERAGENTS


def get_html(url):
    try:
        useragent = {'User-Agent': choice(USERAGENTS)}
        r = requests.get(url, headers=useragent, timeout=2)
        return r.text, r.status_code
    except:
        pass
    return '{}', 403


def get_html1(url):
    r = requests.get(url)
    return r.text


def get_categories_from_db(url):
    html = get_html1(url)
    return json.loads(html)


def get_massimo_data(context):
    status_code = 200
    product_dict = {
        'id': context['id']
    }
    cont = {}
    html = '{}'
    json_data = None
    error_text = ''
    try:
        url = context['url']
        category_id = url.split('category_id=')[1].split('&')[0]
        colour_id = url.split('colorId=')[1].split('&category_id=')[0]
        product_id = url.split('%s' % category_id)[1].split('.html')[0][1:]
        product_json_url = 'https://www.massimodutti.com/itxrest/2/catalog/store/34009471/30359503/category/0/product/%s/detail?languageId=-43&appId=2' % product_id
        html, status_code = get_html(product_json_url)
        json_data = json.loads(html)
    except:
        pass
    try:
        soup = BeautifulSoup(html, 'lxml')
        error_text = soup.find('h1').text
    except:
        pass
    try:
        cont['name'] = json_data['name']
        cont['stock'] = True
        cont['product_id'] = product_id
        cont['product_code'] = product_id
        cont['description'] = json_data['detail']['longDescription']
        static = 'https://static.massimodutti.net/3/photos'
        medias = json_data['detail']['xmedia']
        images = []
        for media in medias:
            if media['colorCode'] == str(colour_id):
                path = static + media['path'] + '/'
                for xmedia in media['xmediaItems'][0]['medias']:
                    image = path + xmedia['idMedia'] + '16.jpg?t=' + str(
                        xmedia['timestamp']) + '&impolicy=massimodutti-itxmedium&imwidth=700'
                    images.append(image)
                break
        image_text = ''
        for image in images:
            image_text = image_text + image + ' '
        cont['images'] = image_text
        product_sizes = []
        for color in json_data['detail']['colors']:
            if color['id'] == str(colour_id):
                cont['colour'] = color['name']
                product_sizes = color['sizes']
                break
        stock_url = 'https://www.massimodutti.com/itxrest/2/catalog/store/34009471/30359503/product/%s/stock?languageId=-43&appId=2' % product_id
        stock_json = get_html(stock_url)
        stock_data = json.loads(stock_json)
        sizes = []
        price = 0
        for stock in stock_data['stocks']:
            if stock['productId'] == int(product_id):
                for product_stock in stock['stocks']:
                    for product_size in product_sizes:
                        if product_size['sku'] == product_stock['id']:
                            stock_bool = False
                            if product_stock['availability'] == 'in_stock':
                                stock_bool = True
                            size = {
                                "value": product_size['name'],
                                'stock': stock_bool
                            }
                            sizes.append(size)
                            if int(product_size['price']) / 100 > price:
                                price = int(product_size['price']) / 100
            break
        cont['sizes'] = sizes
        cont['selling_price'] = price
        cont['discount_price'] = price
        cont['original_price'] = price
    except:
        pass
    if error_text == 'Operation not allowed.':
        product_dict['id'] = 0
        status_code = 429
    if status_code == 403:
        product_dict['id'] = 0
    product_dict['product'] = cont
    product_dict['status_code'] = status_code
    pprint(product_dict)
    return product_dict

