from rest_framework import serializers

from product_app.models import VendCategory


class NewVendCategoryDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendCategory
        fields = ('id', 'name', 'link', 'is_active')
