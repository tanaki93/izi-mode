from django.conf.urls import url

from projects_app.parsing import views

urlpatterns = [
    url(r'^brands/$', views.brands_list_view),
    url(r'^categories/$', views.categories_list_view),
    url(r'^links/$', views.links_brand_list_view),
    url(r'^update/links/$', views.links_update_list_view),
    url(r'^update/by_categories/$', views.links_update_categories_view),
]
