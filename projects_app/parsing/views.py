from django.db import transaction
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from product_app.models import Brand, VendDepartment, VendCategory, Link, OriginalProduct, VendColour, Variant, VendSize
from product_app.services import ProductService
from projects_app.parsing.serializers import NewVendCategoryDetailSerializer
from user_app.admin.serializers import LinkSerializer, LinkWithBrandSerializer
from user_app.main.serializers import BrandSerializer


@api_view(['GET', 'POST', 'PUT'])
@permission_classes([AllowAny])
def brands_list_view(request):
    if request.method == 'GET':
        brand = request.GET.get('brand', '')
        if brand == '':
            brands = Brand.objects.filter(is_active=True)
            return Response(data=BrandSerializer(brands, many=True).data,
                            status=status.HTTP_200_OK)
        else:
            brands = None
            try:
                brands = Brand.objects.get(name=brand)
            except Brand.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
            return Response(data=BrandSerializer(brands).data, status=status.HTTP_200_OK)
    if request.method == 'PUT':
        new_brand = None
        brand = request.GET.get('brand', '')
        if brand != '':
            try:
                new_brand = Brand.objects.get(name=brand)
            except:
                pass
        if new_brand is not None:
            for i in request.data:
                department_name = i['department']
                department = None
                try:
                    department = VendDepartment.objects.get(brand=new_brand, name=department_name)
                except:
                    pass
                if department is None:
                    department = VendDepartment.objects.create(brand=new_brand, name=department_name)
                    department.save()
                    for j in i['categories']:
                        category_name = j['category']
                        category_link = j['link']
                        category = None
                        try:
                            category = VendCategory.objects.get(department=department, name=category_name,
                                                                link=category_link)
                        except:
                            pass
                        if category is None:
                            category = VendCategory.objects.create(department=department, name=category_name,
                                                                   link=category_link)
                            category.save()
        return Response(status=status.HTTP_200_OK)


@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def categories_list_view(request):
    if request.method == 'GET':
        brand = request.GET.get('brand', '')
        categories = []
        if brand != '':
            categories = VendCategory.objects.filter(is_active=True, department__brand__is_active=True,
                                                     department__is_active=True,
                                                     department__brand__code=brand)
        return Response(data=NewVendCategoryDetailSerializer(categories, many=True).data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        with transaction.atomic():
            for i in request.data:
                category = VendCategory.objects.get(id=int(i['category_id']))
                for j in i['links']:
                    link = None
                    try:
                        link = Link.objects.filter(url=j)
                    except:
                        pass
                    if link.count() == 0:
                        link = Link.objects.create(url=j, tr_category=category)
                        link.save()
        return Response(status=status.HTTP_200_OK)


def create_zara_product(link, param):
    original_product = OriginalProduct()
    original_product.link = link
    original_product.product_code = None
    original_product.title = param['name']
    original_product.product_id = link.url
    original_product.discount_price = param['selling_price']
    original_product.selling_price = param['selling_price']
    original_product.original_price = param['selling_price']
    colour = param['colour']
    original_product.colour_code = colour
    images = ''
    for image in param['images']:
        images += (image + ' ')
    original_product.images = images.strip()
    original_product.description = param['description']

    vend_colour = None
    try:
        vend_colour = VendColour.objects.get(name=colour)
    except:
        pass
    if vend_colour is None:
        vend_colour = VendColour.objects.create(name=colour)
        vend_colour.save()
    original_product.colour = vend_colour
    original_product.save()
    for variant in param['sizes']:
        variant_item = Variant()
        tr_size = None
        try:
            tr_size = VendSize.objects.get(name=variant['value'].upper())
        except:
            pass
        if tr_size is None:
            tr_size = VendSize.objects.create(name=variant['value'].upper())
            tr_size.save()
        # save_size(tr_size)
        variant_item.tr_size = tr_size
        variant_item.original_product = original_product
        variant_item.stock = variant['stock']
        variant_item.save()
    try:
        original_product.brand_id = original_product.link.tr_category.department.brand_id
    except:
        pass
    try:
        original_product.department_id = original_product.link.tr_category.department_id
    except:
        pass
    try:
        original_product.category_id = original_product.link.tr_category_id
    except:
        pass
    try:
        original_product.izi_department_id = original_product.link.tr_category.department.department_id
    except:
        pass
    try:
        original_product.izi_category_id = original_product.link.tr_category.category_id
    except:
        pass
    try:
        original_product.izi_parent_category_id = original_product.link.tr_category.category.parent_id
    except:
        pass
    try:
        original_product.izi_colour_id = original_product.colour.izi_colour.id
    except:
        pass
    original_product.save()


def create_collins_product(link, param):
    try:
        original_product = OriginalProduct()
        original_product.link = link
        original_product.product_code = None
        original_product.title = param['name']
        original_product.product_id = link.url
        original_product.discount_price = param['price']
        original_product.selling_price = param['price']
        original_product.original_price = param['price']
        colour = param['colour']
        original_product.colour_code = colour
        original_product.stock = param['stock']
        images = ''
        for image in param['images']:
            images += (image + ' ')
        original_product.images = images.strip()
        original_product.description = param['description']

        vend_colour = None
        try:
            vend_colour = VendColour.objects.get(name=colour)
        except:
            pass
        if vend_colour is None:
            vend_colour = VendColour.objects.create(name=colour)
            vend_colour.save()
        original_product.colour = vend_colour
        original_product.save()
        for variant in param['sizes']:
            variant_item = Variant()
            tr_size = None
            try:
                tr_size = VendSize.objects.get(name=variant['value'].upper())
            except:
                pass
            if tr_size is None:
                tr_size = VendSize.objects.create(name=variant['value'].upper())
                tr_size.save()
            # save_size(tr_size)
            variant_item.tr_size = tr_size
            variant_item.original_product = original_product
            variant_item.stock = variant['stock']
            variant_item.save()
        try:
            original_product.brand_id = original_product.link.tr_category.department.brand_id
        except:
            pass
        try:
            original_product.department_id = original_product.link.tr_category.department_id
        except:
            pass
        try:
            original_product.category_id = original_product.link.tr_category_id
        except:
            pass
        try:
            original_product.izi_department_id = original_product.link.tr_category.department.department_id
        except:
            pass
        try:
            original_product.izi_category_id = original_product.link.tr_category.category_id
        except:
            pass
        try:
            original_product.izi_parent_category_id = original_product.link.tr_category.category.parent_id
        except:
            pass
        try:
            original_product.izi_colour_id = original_product.colour.izi_colour.id
        except:
            pass
        original_product.save()

    except:
        pass


def create_handm_product(link, param):
    try:
        original_product = OriginalProduct()
        original_product.link = link
        original_product.title = param['name']
        original_product.stock = param['stock']
        original_product.product_id = param['product_code']
        original_product.product_code = param['product_code']
        original_product.discount_price = param['selling_price']
        original_product.selling_price = param['selling_price']
        original_product.original_price = param['original_price']
        colour = param['colour']
        original_product.colour_code = colour
        original_product.images = param['images']
        original_product.description = param['description']

        vend_colour = None
        try:
            vend_colour = VendColour.objects.get(name=colour)
        except:
            pass
        if vend_colour is None:
            vend_colour = VendColour.objects.create(name=colour)
            vend_colour.save()
        original_product.colour = vend_colour
        original_product.save()
        for variant in param['sizes']:
            variant_item = Variant()
            tr_size = None
            try:
                tr_size = VendSize.objects.get(name=variant['value'].upper())
            except:
                pass
            if tr_size is None:
                tr_size = VendSize.objects.create(name=variant['value'].upper())
                tr_size.save()
            # save_size(tr_size)
            variant_item.tr_size = tr_size
            variant_item.original_product = original_product
            variant_item.stock = variant['stock']
            variant_item.save()
        variant_items = Variant.objects.filter(original_product=original_product)
        flag = False
        for i in variant_items:
            if i.stock:
                flag = True
                break
        original_product.stock = flag
        original_product.save()
        try:
            original_product.brand_id = original_product.link.tr_category.department.brand_id
        except:
            pass
        try:
            original_product.department_id = original_product.link.tr_category.department_id
        except:
            pass
        try:
            original_product.category_id = original_product.link.tr_category_id
        except:
            pass
        try:
            original_product.izi_department_id = original_product.link.tr_category.department.department_id
        except:
            pass
        try:
            original_product.izi_category_id = original_product.link.tr_category.category_id
        except:
            pass
        try:
            original_product.izi_parent_category_id = original_product.link.tr_category.category.parent_id
        except:
            pass
        try:
            original_product.izi_colour_id = original_product.colour.izi_colour.id
        except:
            pass
        original_product.save()
    except:
        pass


def create_original_product(link, param):
    original_product = OriginalProduct()
    original_product.link = link
    original_product.product_code = param['productCode']
    original_product.title = param['name']
    original_product.product_id = param['id']
    colour = param['color'].split('/')[0]
    vend = VendColour.objects.filter(name=colour)
    if len(vend) > 0:
        original_product.colour = vend.first()
    original_product.discount_price = param['price']['discountedPrice']['value']
    original_product.original_price = param['price']['originalPrice']['value']
    original_product.selling_price = param['price']['sellingPrice']['value']
    try:
        original_product.is_rush_delivery = param['deliveryInformation']['isRushDelivery']
    except:
        pass
    try:
        original_product.delivery_date = param['deliveryInformation']['deliveryDate']
    except:
        pass
    original_product.is_free_argo = param['isFreeCargo']
    images = ''
    for image in param['images']:
        images += ('https://trendyol.com' + image + ' ')
    original_product.images = images.strip()
    if param['hasStock']:
        original_product.stock = True
    else:
        original_product.stock = False
    promotions = ''
    for promotion in param['promotions']:
        promotions += (promotion['text'] + '|\n')
    original_product.promotions = promotions
    original_product.description = param['description']
    original_product.save()
    for variant in param['variants']:
        variant_item = Variant()
        tr_size = None
        try:
            tr_size = VendSize.objects.filter(name=variant['attributeValue'].upper())[0]
        except:
            pass
        if tr_size is None:
            tr_size = VendSize.objects.create(name=variant['attributeValue'].upper())
            tr_size.save()
        # save_size(tr_size)
        variant_item.tr_size = tr_size
        variant_item.original_product = original_product
        if variant['stock'] != 0:
            variant_item.stock = True
        # else:
        #     variant_item.stock = False
        variant_item.save()
    try:
        original_product.brand_id = original_product.link.tr_category.department.brand_id
    except:
        pass
    try:
        original_product.department_id = original_product.link.tr_category.department_id
    except:
        pass
    try:
        original_product.category_id = original_product.link.tr_category_id
    except:
        pass
    try:
        original_product.izi_department_id = original_product.link.tr_category.department.department_id
    except:
        pass
    try:
        original_product.izi_category_id = original_product.link.tr_category.category_id
    except:
        pass
    try:
        original_product.izi_parent_category_id = original_product.link.tr_category.category.parent_id
    except:
        pass
    try:
        original_product.izi_colour_id = original_product.colour.izi_colour.id
    except:
        pass
    original_product.save()


@api_view(['GET', 'POST', 'PUT'])
@permission_classes([AllowAny])
def links_brand_list_view(request):
    if request.method == 'GET':
        brand = request.GET.get('brand', '')
        links = []
        if brand != '':
            links = Link.objects.filter(tr_category__isnull=False, tr_category__is_active=True,
                                        tr_category__department__is_active=True,
                                        originalproduct__isnull=True,
                                        tr_category__department__brand__code=brand)
        return Response(data=LinkSerializer(links, many=True).data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        with transaction.atomic():
            brand = request.GET.get('brand', '')
            for i in request.data:
                link = None
                try:
                    link = Link.objects.get(id=int(i['id']))
                except:
                    pass
                if link is None:
                    continue
                original_product = None
                try:
                    original_product = link.originalproduct
                except:
                    pass
                if original_product is None and brand == 'ZARA':
                    create_zara_product(link, i['product'])
                elif original_product is None and brand == 'COLNS':
                    create_collins_product(link, i['product'])
                elif original_product is None and brand == 'HANDM':
                    create_handm_product(link, i['product'])
                elif original_product is None and brand == 'MDT':
                    create_handm_product(link, i['product'])
                elif original_product is None and brand == 'PLLBR':
                    create_handm_product(link, i['product'])
                elif original_product is None and brand == 'BRSHK':
                    create_handm_product(link, i['product'])
                elif original_product is None and brand == 'STRD':
                    create_handm_product(link, i['product'])
                else:
                    if original_product is None:
                        try:
                            create_original_product(link, i['product'])
                        except:
                            pass
        return Response(status=status.HTTP_200_OK)


def update_trendyol_original_product(original_product, param):
    if param == {}:
        original_product.stock = False
        original_product.save()
        return
    if param['hasStock']:
        original_product.stock = True
    else:
        original_product.stock = False
    original_product.discount_price = param['price']['discountedPrice']['value']
    original_product.original_price = param['price']['originalPrice']['value']
    try:
        original_product.selling_price = param['price']['sellingPrice']['value']
    except:
        pass
    variants = Variant.objects.filter(original_product=original_product)
    variants.delete()
    for variant in param['variants']:
        variant_item = Variant()
        tr_size = None
        try:
            tr_size = VendSize.objects.filter(name=variant['attributeValue'].upper())[0]
        except:
            pass
        if tr_size is None:
            tr_size = VendSize.objects.create(name=variant['attributeValue'].upper())
            tr_size.save()
        # save_size(tr_size)
        variant_item.tr_size = tr_size
        variant_item.original_product = original_product
        if variant['stock'] != 0:
            variant_item.stock = True
        else:
            variant_item.stock = False
        variant_item.save()
    original_product.save()


def update_massimo_product(original_product, param):
    if param == {}:
        original_product.stock = False
        original_product.save()
        return
    elif param is not None:
        original_product.discount_price = param['selling_price']
        original_product.selling_price = param['selling_price']
        original_product.original_price = param['original_price']
        try:
            original_product.images = param['images']
        except:
            pass
        original_product.save()
        try:
            variants = Variant.objects.filter(original_product=original_product)
            variants.delete()
            for variant in param['sizes']:
                variant_items = Variant.objects.filter(original_product=original_product,
                                                       tr_size__name=variant['value'].upper())

                if variant_items.count() == 0:
                    variant_item = Variant()
                    tr_size = None
                    try:
                        tr_size = VendSize.objects.filter(name=variant['value'].upper())[0]
                    except:
                        pass
                    if tr_size is None:
                        tr_size = VendSize.objects.create(name=variant['value'].upper())
                        tr_size.save()
                    # save_size(tr_size)
                    variant_item.tr_size = tr_size
                    variant_item.original_product = original_product
                    variant_item.stock = variant['stock']
                    variant_item.save()
                elif variant_items.count() > 0:
                    variant_item = variant_items[0]
                    variant_item.stock = variant['stock']
                    variant_item.save()
            variant_items = Variant.objects.filter(original_product=original_product)
            flag = False
            for i in variant_items:
                if i.stock:
                    flag = True
                    break
            original_product.stock = flag
            original_product.save()
        except:
            pass


def update_collins_product(link, param):
    if param is not None:
        original_product = link
        try:
            original_product.discount_price = param['price']
            original_product.selling_price = param['price']
            original_product.original_price = param['price']
        except:
            pass
        try:
            original_product.stock = param['stock']
        except:
            pass
        original_product.save()
        try:
            variants = Variant.objects.filter(original_product=original_product)
            variants.delete()
            for variant in param['sizes']:
                variant_item = Variant()
                tr_size = None
                try:
                    tr_size = VendSize.objects.get(name=variant['value'].upper())
                except:
                    pass
                if tr_size is None:
                    tr_size = VendSize.objects.create(name=variant['value'].upper())
                    tr_size.save()
                # save_size(tr_size)
                variant_item.tr_size = tr_size
                variant_item.original_product = original_product
                if variant['stock'] != 0:
                    variant_item.stock = True
                else:
                    variant_item.stock = False
                variant_item.save()
            variant_items = Variant.objects.filter(original_product=original_product)
            flag = False
            for i in variant_items:
                if i.stock:
                    flag = True
                    break
            original_product.stock = flag
            original_product.save()
        except:
            pass


def update_zara_product(original_product, param):
    if param == {}:
        original_product.stock = False
        original_product.save()
        return
    else:
        original_product.discount_price = param['selling_price']
        original_product.selling_price = param['selling_price']
        original_product.original_price = param['original_price']
        original_product.save()
        images = ''
        for image in param['images']:
            images += (image + ' ')
        original_product.images = images.strip()
        variants = Variant.objects.filter(original_product=original_product)
        variants.delete()
        for variant in param['sizes']:
            variant_item = Variant()
            tr_size = None
            try:
                tr_size = VendSize.objects.get(name=variant['value'].upper())
            except:
                pass
            if tr_size is None:
                tr_size = VendSize.objects.create(name=variant['value'].upper())
                tr_size.save()
            # save_size(tr_size)
            variant_item.tr_size = tr_size
            variant_item.original_product = original_product
            variant_item.stock = variant['stock']
            variant_item.save()
        variant_items = Variant.objects.filter(original_product=original_product)
        flag = False
        for i in variant_items:
            if i.stock:
                flag = True
                break
        original_product.stock = flag
        original_product.save()
    # else:
    #     original_product.stock = False
    #     original_product.save()
    # print(original_product.updated_at, original_product.images)


@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def links_update_list_view(request):
    if request.method == 'POST':
        brand = request.GET.get('brand', '')
        project = request.GET.get('project', '')

        for i in request.data:
            original_product = None
            try:
                link = Link.objects.get(id=int(i['id']))
                link.save()
                original_product = link.originalproduct
            except:
                continue
            if project == 'trendyol' and original_product is not None:
                update_trendyol_original_product(original_product, i['product'])
                continue
            if brand == 'HANDM' or brand == 'BRSHK' or brand == 'STRD' or brand == 'MDT' or brand == 'PLLBR':
                if original_product is not None:
                    update_massimo_product(original_product, i['product'])
            elif original_product is not None and brand == 'COLNS':
                update_collins_product(original_product, i['product'])
            elif original_product is not None and brand == 'ZARA':
                update_zara_product(original_product, i['product'])

        return Response(status=status.HTTP_200_OK)

    elif request.method == 'GET':
        brand = request.GET.get('brand', '')
        project = request.GET.get('project', '')
        active = request.GET.get('active', '')
        links = []
        if brand != '':
            links = Link.objects.filter(tr_category__isnull=False, tr_category__is_active=True,
                                        tr_category__department__is_active=True,
                                        originalproduct__isnull=False,
                                        tr_category__department__brand__code=brand)
        if project == 'trendyol':
            links = Link.objects.filter(tr_category__isnull=False, tr_category__is_active=True,
                                        tr_category__department__is_active=True,
                                        originalproduct__isnull=False,
                                        originalproduct__brand__is_trend_yol=True)
        if active == '1':
            links = links.filter(originalproduct__stock=True)
        elif active == '2':
            links = links.filter(originalproduct__stock=False)
        return Response(data=LinkSerializer(links, many=True).data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([AllowAny])
def links_update_categories_view(request):
    if request.method == 'GET':
        data = ProductService.get_parsing_links(request.query_params)
        return Response(data=LinkWithBrandSerializer(data, many=True).data, status=status.HTTP_200_OK)
