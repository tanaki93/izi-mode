from product_app.models import Variant, VendSize


def update_trendyol_original_product(original_product, param):
    if param == {}:
        original_product.stock = False
        original_product.save()
        return
    if param['hasStock']:
        original_product.stock = True
    else:
        original_product.stock = False
    original_product.discount_price = param['price']['discountedPrice']['value']
    original_product.original_price = param['price']['originalPrice']['value']
    try:
        original_product.selling_price = param['price']['sellingPrice']['value']
    except:
        pass
    variants = Variant.objects.filter(original_product=original_product)
    variants.delete()
    for variant in param['variants']:
        variant_item = Variant()
        tr_size = None
        try:
            tr_size = VendSize.objects.filter(name=variant['attributeValue'].upper())[0]
        except:
            pass
        if tr_size is None:
            tr_size = VendSize.objects.create(name=variant['attributeValue'].upper())
            tr_size.save()
        # save_size(tr_size)
        variant_item.tr_size = tr_size
        variant_item.original_product = original_product
        if variant['stock'] != 0:
            variant_item.stock = True
        else:
            variant_item.stock = False
        variant_item.save()
    original_product.save()


def update_massimo_product(original_product, param):
    if param == {}:
        original_product.stock = False
        original_product.save()
        return
    elif param is not None:
        original_product.discount_price = param['selling_price']
        original_product.selling_price = param['selling_price']
        original_product.original_price = param['original_price']
        try:
            original_product.images = param['images']
        except:
            pass
        original_product.save()
        try:
            variants = Variant.objects.filter(original_product=original_product)
            variants.delete()
            for variant in param['sizes']:
                variant_items = Variant.objects.filter(original_product=original_product,
                                                       tr_size__name=variant['value'].upper())

                if variant_items.count() == 0:
                    variant_item = Variant()
                    tr_size = None
                    try:
                        tr_size = VendSize.objects.filter(name=variant['value'].upper())[0]
                    except:
                        pass
                    if tr_size is None:
                        tr_size = VendSize.objects.create(name=variant['value'].upper())
                        tr_size.save()
                    # save_size(tr_size)
                    variant_item.tr_size = tr_size
                    variant_item.original_product = original_product
                    variant_item.stock = variant['stock']
                    variant_item.save()
                elif variant_items.count() > 0:
                    variant_item = variant_items[0]
                    variant_item.stock = variant['stock']
                    variant_item.save()
            variant_items = Variant.objects.filter(original_product=original_product)
            flag = False
            for i in variant_items:
                if i.stock:
                    flag = True
                    break
            original_product.stock = flag
            original_product.save()
        except:
            pass


def update_collins_product(link, param):
    if param is not None:
        original_product = link
        try:
            original_product.discount_price = param['price']
            original_product.selling_price = param['price']
            original_product.original_price = param['price']
        except:
            pass
        try:
            original_product.stock = param['stock']
        except:
            pass
        original_product.save()
        try:
            variants = Variant.objects.filter(original_product=original_product)
            variants.delete()
            for variant in param['sizes']:
                variant_item = Variant()
                tr_size = None
                try:
                    tr_size = VendSize.objects.get(name=variant['value'].upper())
                except:
                    pass
                if tr_size is None:
                    tr_size = VendSize.objects.create(name=variant['value'].upper())
                    tr_size.save()
                # save_size(tr_size)
                variant_item.tr_size = tr_size
                variant_item.original_product = original_product
                if variant['stock'] != 0:
                    variant_item.stock = True
                else:
                    variant_item.stock = False
                variant_item.save()
            variant_items = Variant.objects.filter(original_product=original_product)
            flag = False
            for i in variant_items:
                if i.stock:
                    flag = True
                    break
            original_product.stock = flag
            original_product.save()
        except:
            pass


def update_zara_product(original_product, param):
    if param == {}:
        original_product.stock = False
        original_product.save()
        return
    else:
        original_product.discount_price = param['selling_price']
        original_product.selling_price = param['selling_price']
        original_product.original_price = param['original_price']
        original_product.save()
        images = ''
        for image in param['images']:
            images += (image + ' ')
        original_product.images = images.strip()
        variants = Variant.objects.filter(original_product=original_product)
        variants.delete()
        for variant in param['sizes']:
            variant_item = Variant()
            tr_size = None
            try:
                tr_size = VendSize.objects.filter(name=variant['value'].upper()).first()
            except:
                pass
            if tr_size is None:
                tr_size = VendSize.objects.create(name=variant['value'].upper())
                tr_size.save()
            # save_size(tr_size)
            variant_item.tr_size = tr_size
            variant_item.original_product = original_product
            variant_item.stock = variant['stock']
            variant_item.save()
        variant_items = Variant.objects.filter(original_product=original_product)
        flag = False
        for i in variant_items:
            if i.stock:
                flag = True
                break
        original_product.stock = flag
        original_product.save()